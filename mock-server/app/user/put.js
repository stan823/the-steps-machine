const token = require('../Responses').VALID_TOKEN
const guid = require('../Responses').VALID_GUID
const NAME_IN_USE_NAME = "nameinuse"
const FREE_NAME_NAME = "freename"


module.exports.put = (req, res) => {

    const uid = req.body.uId;
    const uName = req.body.uName;

    if (typeof uid == 'string' && typeof uName == 'string') {


        if (uName === NAME_IN_USE_NAME) {
            res.status(200);
            res.send({
                token: token,
                name: `${uName}_1`,
                guid: guid
            });
        } else {
            usedId = uid
            usedName = uName;
            res.send({
                token: token,
                name: uName,
                guid: guid
            });
        }

    } else {
        res.status(400);
        res.send({})
    }
}

