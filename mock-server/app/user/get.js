const valid_uid = require('../Paramters').VALID_UID;
const user_not_found_uid = require('../Paramters').USER_NOT_FOUND_UID;
const token = require('../Responses').VALID_TOKEN
const guid = require('../Responses').VALID_GUID

module.exports.get = (req, res) =>{
    const uid = req.query.uid;
    console.log(`uid ${uid}`);

    if (uid === user_not_found_uid){
        console.log("sending error response")
        res.statusCode = 412;
        res.send({
            error: 2102
        })
    }else if (uid === valid_uid){
        console.log("sending success response")
        res.send({
            token: token,
            guid: guid,
            name: "User Name"
        })
    }
}

