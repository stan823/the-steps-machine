module.exports.patch = (req, res) => {

    const NAME_IN_USE = "nameInUse"
    const name = req.body.name;
    
    if (name == null || name == ""){
        res.status(400);
        res.send({error:1001});
    } else if (name == NAME_IN_USE) {
        res.status(401);
        res.send({error:2101});
    } else {
        res.status(200);
        res.send({});

    }
}