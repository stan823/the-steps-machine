const express = require('express');
const put_user = require('./user/put.js')
const get_user = require('./user/get.js')
const change_name = require('./user/name/patch.js')
const delete_user = require('./user/delete.js')
const bodyParser = require("body-parser");

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.get('/', (req, res) => {
    res.send("Hello 9");
});

app.put("/user", (req, res) => {
    put_user.put(req, res)
})

app.patch("/user/name", (req, res) => {
    change_name.patch(req, res)
})
app.delete("/user", (req, res) => {
    delete_user.delete(req, res)
})

app.get("/user", (req, res)=>{
    get_user.get(req, res);
});


app.listen('5000', () => {
    console.log("Listening on port 5000");
})