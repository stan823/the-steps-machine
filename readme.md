# TheStepsMachine

[![version](https://img.shields.io/badge/version-0.0.1-red.svg)](https://semver.org)


TheStepsMachine is Android App - *currently work in progress* - that lets user track his daily steps 
share his effort with social network.

## Flavours

App uses three flavours:
1.  **Prod** - used for connecting with production server,
2.  **Stage** - used for connecting with staging server,
3.  **Mock** - used for running UITests,

## Running Instrumented Tests

In order to run entire AndroidTest suite you must
*  switch build variant to mockDebug
*  start mock server `./gradlew composeUp`
*  after tests mock server can be stopped by executing `./gradlew composeDown` 
