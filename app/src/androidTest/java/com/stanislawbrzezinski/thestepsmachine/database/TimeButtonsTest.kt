package com.stanislawbrzezinski.thestepsmachine.database

import androidx.test.espresso.action.ViewActions.click
import androidx.test.rule.ActivityTestRule
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dsl.areNotSelected
import com.stanislawbrzezinski.thestepsmachine.dsl.click
import com.stanislawbrzezinski.thestepsmachine.dsl.isSeleced
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 01/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TimeButtonsTest {

    @get:Rule
    public val rule = ActivityTestRule<MainActivity>(MainActivity::class.java)

    val dayButton = R.id.dayButton
    val weekButton = R.id.weekButton
    val monthButton = R.id.monthButton

    @Test
    fun shouldOnlyOneTimeButtonBeSelected() {

        dayButton.isSeleced()
        arrayOf(weekButton, monthButton).areNotSelected()

        weekButton.click()
        weekButton.isSeleced()
        arrayOf(dayButton, monthButton).areNotSelected()

        monthButton.click()
        monthButton.isSeleced()
        arrayOf(dayButton, weekButton).areNotSelected()

        dayButton.click()
        dayButton.isSeleced()
        arrayOf(weekButton, monthButton).areNotSelected()
    }
}