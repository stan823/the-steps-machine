package com.stanislawbrzezinski.thestepsmachine.database

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.platform.app.InstrumentationRegistry
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.StepEntity
import junit.framework.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsDaoTest {

    private lateinit var dao: StepsDao

    @get:Rule
    var rule = InstantTaskExecutorRule()

    @Before
    fun setUp() {
        val context = InstrumentationRegistry.getInstrumentation().targetContext
        val db = AppDatabase.getInstance(context)
        dao = db.getStepsDao()
    }

    @Test
    fun testInsert1() {

        var updatedCounter = 0
        val steps = dao.getStepsLiveData(0L)
        steps.observeForever {
            if (updatedCounter == 1) {
                assertEquals(1, steps.value)
            }
            if (updatedCounter == 2) {
                assertEquals(2, steps.value)
            }
        }

        updatedCounter++
        dao.insert(StepEntity(created = 4L))

        updatedCounter++
        dao.insert(StepEntity(created = 43L))
    }
}