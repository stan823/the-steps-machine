package com.stanislawbrzezinski.thestepsmachine.database

import androidx.test.rule.ActivityTestRule
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dsl.click
import com.stanislawbrzezinski.thestepsmachine.dsl.isDisplayed
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetsPageTest {

    @get:Rule
    val activityRule = ActivityTestRule<MainActivity>(
            MainActivity::class.java,
            true,
            true
    )

    @Test
    fun shouldPressingTargetButtonShowTargetsScreen() {
        R.id.targetsButton.click()
        R.id.fragmentTargets.isDisplayed()
    }
}