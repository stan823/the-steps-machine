package com.stanislawbrzezinski.thestepsmachine.ui

import androidx.test.InstrumentationRegistry
import androidx.test.rule.ActivityTestRule
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dsl.*
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-08-22.
 * Copyrights Stanisław Brzeziński
 */
class AccountScreenTest {

    @get:Rule
    val rule = ActivityTestRule(MainActivity::class.java, true, false)

    @Test
    fun testOpennigngAccountPageWhenLoggedIn() {

        AppDatabase.getInstance(InstrumentationRegistry.getTargetContext())
                .getAccountsDao()
                .insertAccountSync(AccountEntity("token", "guid", "name"))
        rule.launchActivity(null)

        R.id.profileButton.click()
        R.id.fragmentAccount.isDisplayed()

        R.id.nameHolder.hasText("name")
    }

    @Test
    fun testDeletingAccountDialog() {

        AppDatabase.getInstance(InstrumentationRegistry.getTargetContext())
                .getAccountsDao()
                .insertAccountSync(AccountEntity("token", "guid", "name"))
        rule.launchActivity(null)

        R.id.profileButton.click()
        R.id.fragmentAccount.isDisplayed()

        R.id.nameHolder.hasText("name")
        R.id.deleteButton.click()
        R.string.alert_confirm.textIsDisplayed()
    }

    @Test
    fun testDismissingDeletingAccountDialog() {

        AppDatabase.getInstance(InstrumentationRegistry.getTargetContext())
                .getAccountsDao()
                .insertAccountSync(AccountEntity("token", "guid", "name"))
        rule.launchActivity(null)

        R.id.profileButton.click()
        R.id.fragmentAccount.isDisplayed()

        R.id.nameHolder.hasText("name")
        R.id.deleteButton.click()
        R.string.alert_confirm.textIsDisplayed()

        android.R.string.cancel.textClick()
        R.string.alert_confirm.textDoesntExit()
    }

    @Test
    fun testDeletingAccount() {

        AppDatabase.getInstance(InstrumentationRegistry.getTargetContext())
                .getAccountsDao()
                .insertAccountSync(AccountEntity("token", "guid", "name"))
        rule.launchActivity(null)

        R.id.profileButton.click()
        R.id.fragmentAccount.isDisplayed()

        R.id.nameHolder.hasText("name")
        R.id.deleteButton.click()

        android.R.string.ok.textClick()
        R.string.alert_confirm.textDoesntExit()

        R.id.fragmentMain.isDisplayed()
        R.id.profileButton.click()
        R.id.fragmentLogin.isDisplayed()
    }

    @Test
    fun testOpennigngAccountPageWhenNotLoggedIn() {

        rule.launchActivity(null)

        R.id.profileButton.click()
        R.id.fragmentLogin.isDisplayed()
    }
}