package com.stanislawbrzezinski.thestepsmachine.ui

import androidx.test.rule.ActivityTestRule
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dsl.*
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManagerImpl
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-07-31.
 * Copyrights Stanisław Brzeziński
 */
class RegistrationTest {

    @get:Rule
    val rule = ActivityTestRule(MainActivity::class.java)

    @Test
    fun shouldEnableRegisterButtonForRightCredentials() {
        R.id.profileButton.click()
        R.id.fragmentLogin.isDisplayed()

        R.id.registerButton.isDisabled()

        R.id.emailField typeText "abc"
        R.id.passwordField typeText "abc"

        R.id.registerButton.isDisabled()
        R.id.emailField
                .clearText()
                .typeText("abc@cde.efg")
        R.id.registerButton.isEnabled()
    }

    @Test
    fun shouldResetStateAfterGoingBack() {
        R.id.profileButton.click()
        R.id.fragmentLogin.isDisplayed()

        R.id.emailField typeText "abc@abc.abc"
        R.id.passwordField typeText "abc"

        R.id.backButton.click()

        R.id.fragmentMain.isDisplayed()
        R.id.profileButton.click()

        R.id.emailField.isEmpty()
        R.id.passwordField.isEmpty()
        R.id.registerButton.isDisabled()
    }

    @Test
    fun shouldShowPasswordTooWeekError() {
        R.id.profileButton.click()

        R.id.emailField.typeText("abc@abc.abc")
        R.id.passwordField.typeText("12345")
        R.id.registerButton.click()

        R.id.error.hasText(R.string.error_password_too_week)
        R.id.loader.isHidden()
    }

    @Test
    fun shouldShowUserExistsError() {
        R.id.profileButton.click()

        R.id.emailField.typeText(FirebaseAuthManagerImpl.EMAIL_IN_USE)
        R.id.passwordField.typeText("123456")
        R.id.registerButton.click()

        R.id.error.hasText(R.string.error_user_already_exists)
        R.id.loader.isHidden()
    }

    @Test
    fun shouldNavigateToSuccessScreenWithProvidedName() {
        R.id.profileButton.click()

        val email = "${FirebaseAuthManagerImpl.FREE_NAME}@email.com"
        R.id.emailField.typeText(email)
        R.id.passwordField.typeText("123456")
        R.id.registerButton.click()

        R.id.fragmentRegistrationSuccessful.isDisplayed()
        R.id.nameHolder.hasText(FirebaseAuthManagerImpl.FREE_NAME)
    }

    @Test
    fun shouldNavigateToSuccessScreenWithProvidedNameInUser() {
        R.id.profileButton.click()

        val email = "${FirebaseAuthManagerImpl.NAME_IN_USE_NAME}@email.com"
        R.id.emailField.typeText(email)
        R.id.passwordField.typeText("123456")
        R.id.registerButton.click()

        R.id.fragmentRegistrationSuccessful.isDisplayed()
        R.id.nameHolder.hasText(FirebaseAuthManagerImpl.NAME_IN_USE_NAME + "_1")
    }

    @Test
    fun shouldNavigateBackToMainScreenFromRegistrationSuccessful() {
        R.id.profileButton.click()

        val email = "${FirebaseAuthManagerImpl.FREE_NAME}@email.com"
        R.id.emailField.typeText(email)
        R.id.passwordField.typeText("123456")
        R.id.registerButton.click()

        R.id.fragmentRegistrationSuccessful.isDisplayed()
        R.id.okButton.click()

        R.id.fragmentMain.isDisplayed()
    }
}