package com.stanislawbrzezinski.thestepsmachine.utils

import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
class ValidationUtilsImplTest {

    private val validator = ValidationUtilsImpl()

    @Test
    fun shouldValidateEmailReturnFalseWhenEmpty() {
        assertFalse(validator.isValidEmail(""))
    }

    @Test
    fun shouldValidateEmailReturnFalseWhenInvalid() {
        assertFalse(validator.isValidEmail("abcd"))
    }

    @Test
    fun shouldValidateEmailReturnTrueWhenValid() {
        assertTrue(validator.isValidEmail("abc@abc.abc"))
    }
}