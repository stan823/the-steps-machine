package com.stanislawbrzezinski.thestepsmachine.dsl

import androidx.annotation.StringRes
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.assertion.ViewAssertions.doesNotExist
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import org.hamcrest.Matchers.not

/**
 * Created by Stanisław Brzeziński on 01/05/2019.
 * Copyrights Stanisław Brzeziński
 */

fun Int.click() {
    onView(ViewMatchers.withId(this))
            .perform(ViewActions.click())
}

fun Int.textClick() {
    onView(ViewMatchers.withText(this))
            .perform(ViewActions.click())
}

fun Int.isSeleced() {
    onView(withId(this))
            .check(matches(isSelected()))
}

fun Int.isNotSelected() {
    onView(withId(this))
            .check(matches(not(isSelected())))
}

fun Int.isEnabled() {
    onView(withId(this))
            .check(matches(ViewMatchers.isEnabled()))
}

fun Int.isDisabled() {
    onView(withId(this))
            .check(matches(not(ViewMatchers.isEnabled())))
}

fun Array<Int>.areNotSelected() {
    this.forEach {
        onView(withId(it))
                .check(matches(not(isSelected())))
    }
}

fun Int.isDisplayed() {
    onView(withId(this))
            .check(matches(ViewMatchers.isDisplayed()))
}

fun Int.isHidden() {
    onView(withId(this))
            .check(matches(not(ViewMatchers.isDisplayed())))
}

infix fun Int.typeText(text: String): Int {
    onView(withId(this))
            .perform(ViewActions.typeText(text))
    return this
}

fun Int.clearText(): Int {
    onView(withId(this))
            .perform(ViewActions.clearText())
    return this
}

fun Int.isEmpty(): Int {
    onView(withId(this))
            .check(matches(ViewMatchers.withText("")))
    return this
}

fun pressBack() {
    onView(isRoot()).perform(ViewActions.pressBack())
}

fun Int.hasText(@StringRes stringRes: Int): Int {
    onView(withId(this))
            .check(matches(withText(stringRes)))
    return this
}

fun Int.hasText(text: String): Int {
    onView(withId(this))
            .check(matches(withText(text)))
    return this
}

fun Int.textIsDisplayed(): Int {
    onView(withText(this))
            .check(matches(ViewMatchers.isDisplayed()))
    return this
}

fun Int.textIsNotDisplayed(): Int {
    onView(withText(this))
            .check(matches(not(ViewMatchers.isDisplayed())))
    return this
}

fun Int.textDoesntExit(): Int {
    onView(withText(this))
            .check(doesNotExist())
    return this
}