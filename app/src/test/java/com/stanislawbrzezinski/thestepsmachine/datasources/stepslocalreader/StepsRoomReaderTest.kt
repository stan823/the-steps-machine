package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import org.junit.Rule

/**
 * Created by Stanisław Brzeziński on 04/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsRoomReaderTest {

    @get:Rule
    val instantRule = InstantTaskExecutorRule()
}