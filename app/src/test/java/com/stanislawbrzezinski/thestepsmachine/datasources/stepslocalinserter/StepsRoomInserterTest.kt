package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.room.daos.DaysDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.MonthsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.WeeksDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.StepEntity
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsRoomInserterTest {

    @RelaxedMockK
    private lateinit var stepsDao: StepsDao

    @RelaxedMockK
    private lateinit var daysDao: DaysDao

    @RelaxedMockK
    private lateinit var weeksDao: WeeksDao

    @RelaxedMockK
    private lateinit var monthsDao: MonthsDao

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    private val inserter: StepsLocalInserter by lazy {
        StepsRoomInserter(stepsDao, daysDao, weeksDao, monthsDao, systemTimeProvider)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testInsertingStep() {

        val insertedTime = 100L

        val slot = slot<StepEntity>()
        every {
            systemTimeProvider.getCurrentTimeInMilliseconds()
        } returns insertedTime

        every {
            stepsDao.insert(step = capture(slot))
        } answers {
            assertEquals(slot.captured.created, insertedTime)
        }

        inserter.insertStep()
    }
}