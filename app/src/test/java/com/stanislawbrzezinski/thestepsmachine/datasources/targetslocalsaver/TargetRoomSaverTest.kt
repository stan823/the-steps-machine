package com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.room.daos.TargetsDao
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-05-09.
 * Copyrights Stanisław Brzeziński
 */
class TargetRoomSaverTest {

    @RelaxedMockK
    private lateinit var timeProvider: SystemTimeProvider

    @RelaxedMockK
    private lateinit var targetsDao: TargetsDao

    private val saver: TargetRoomSaver by lazy {
        TargetRoomSaver(targetsDao, timeProvider)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should insert into db entity`() {

        every {
            timeProvider.getCurrentTimeInMilliseconds()
        } returns 100L

        saver.saveTargets(1, 1, 1)
        verify {
            targetsDao.insert(any())
        }
    }
}