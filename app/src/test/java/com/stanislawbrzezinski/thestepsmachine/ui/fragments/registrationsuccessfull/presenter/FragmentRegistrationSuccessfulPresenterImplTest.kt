package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter

import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveData
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
class FragmentRegistrationSuccessfulPresenterImplTest {

    @RelaxedMockK
    private lateinit var accountsLiveData: AccountLiveData

    private val presenter: FragmentRegistrationSuccessfulPresenterImpl by lazy {
        FragmentRegistrationSuccessfulPresenterImpl(accountsLiveData)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should mapper return name`() {
        val name = "someName"
        val account = AccountModel(name = name)

        val result = presenter.mapper(account)

        assertEquals(name, result)
    }
}