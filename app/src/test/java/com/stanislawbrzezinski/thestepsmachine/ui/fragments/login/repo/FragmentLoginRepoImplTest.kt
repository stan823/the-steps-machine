package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo

import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.AccountRetrofitService
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.LoginStateLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.RegistrationState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.RegistrationStateLiveDataInterface
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Single
import kotlinx.coroutines.*
import org.junit.Before
import org.junit.Test
import org.junit.jupiter.api.BeforeEach

/**
 * Created by Stanisław Brzeziński on 2019-07-29.
 * Copyrights Stanisław Brzeziński
 */

class FragmentLoginRepoImplTest {


    @RelaxedMockK
    private lateinit var firebaseManager: FirebaseAuthManager

    @RelaxedMockK
    private lateinit var registrationLiveData: RegistrationStateLiveDataInterface

    @RelaxedMockK
    private lateinit var loginStateLiveData: LoginStateLiveDataInterface

    @RelaxedMockK
    private lateinit var accountRetrofitService: AccountRetrofitService

    @RelaxedMockK
    private lateinit var errorService: ErrorService

    @RelaxedMockK
    private lateinit var accountLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var accountsDao: AccountsDao

    private val repo: FragmentLoginRepoImpl by lazy {
        FragmentLoginRepoImpl(
                firebaseManager,
                registrationLiveData,
                loginStateLiveData,
                accountRetrofitService,
                errorService,
                accountLiveData,
                accountsDao
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should call registerWithEmail on firebase when creating user`() {

        val email = "email"
        val password = "password"


        repo.createUser(email, password)

        verify {
            firebaseManager.registerWithEmail(email, password)
        }
    }

    @Test
    fun `should show password too week error when PasswordException`() {
        val e = mockk<Exception>(relaxed = true)

        val passwordException  = mockk<FirebaseAuthWeakPasswordException>()

        repo.onFirebaseRegisterError(passwordException)

        verify {
            registrationLiveData.postValue(RegistrationState.PASSWORD_TOO_WEEK)
        }
    }

    @Test
    fun `should show user already exists when UsersCollisions exception`() {
        val e = mockk<Exception>(relaxed = true)

        val causeException  = mockk<FirebaseAuthUserCollisionException>()

        repo.onFirebaseRegisterError(causeException)

        verify {
            registrationLiveData.postValue(RegistrationState.USER_ALREADY_EXISTS)
        }
    }
}