package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.enums.ToastMessages
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo.FragmentLoginRepo
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.*
import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtils
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLoginViewModelImplTest {

    companion object {
        const val VALID_EMAIL = "aaa@aaa.aa"
        const val INVALID_EMAIL = "aaa"
        const val PASSWORD = "abc"
    }

    @RelaxedMockK
    private lateinit var validationUtils: ValidationUtils

    @RelaxedMockK
    private lateinit var toastsService: ToastsService

    @RelaxedMockK
    private lateinit var repo: FragmentLoginRepo

    @RelaxedMockK
    private lateinit var registationStateLiveData: RegistrationStateLiveDataInterface

    @RelaxedMockK
    private lateinit var stateLiveData: FragmentLoginStateLiveDataInterface

    @RelaxedMockK
    private lateinit var navigation: MainNavigationLiveDataInterface

    @RelaxedMockK
    private lateinit var accountLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var loginStateLiveData: LoginStateLiveDataInterface

    private val viewModel: FragmentLoginViewModelImpl by lazy {
        FragmentLoginViewModelImpl(
                validationUtils,
                repo,
                navigation,
                registationStateLiveData,
                loginStateLiveData,
                accountLiveData,
                stateLiveData,
                toastsService
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            validationUtils.isValidEmail(VALID_EMAIL)
        } returns true

        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()
    }

    @Test
    fun shouldEnableButtonsWhenValidInput() {
        viewModel.email = VALID_EMAIL
        viewModel.password = PASSWORD

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.updateFields()

        verify {
            stateLiveData.postValue(any())
        }
        assertTrue(slot.captured.isLoginEnabled)
        assertTrue(slot.captured.isRegistrationEnabled)
    }

    @Test
    fun shouldDisableButtonsWhenInvalidEmail() {
        viewModel.email = INVALID_EMAIL
        viewModel.password = PASSWORD

        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.updateFields()

        verify {
            stateLiveData.postValue(any())
        }
        assertFalse(slot.captured.isLoginEnabled)
        assertFalse(slot.captured.isRegistrationEnabled)
    }

    @Test
    fun shouldDisableButtonsWhenInvalidPassword() {
        viewModel.email = VALID_EMAIL
        viewModel.password = ""
        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.updateFields()

        verify {
            stateLiveData.postValue(any())
        }
        assertFalse(slot.captured.isLoginEnabled)
        assertFalse(slot.captured.isRegistrationEnabled)
    }

    @Test
    fun shouldDisableButtonsWhenEmptyCredentials() {
        viewModel.email = VALID_EMAIL
        viewModel.password = ""
        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.updateFields()

        verify {
            stateLiveData.postValue(any())
        }
        assertFalse(slot.captured.isLoginEnabled)
        assertFalse(slot.captured.isRegistrationEnabled)
    }

    @Test
    fun shouldCallRepoOnLogin() {
        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()

        viewModel.login()

        coVerify {
            repo.login(viewModel.email, viewModel.password)
        }
    }

    @Test
    fun shouldShowLoadingScreenOnLogin() {
        every {
            stateLiveData.getValue()
        } returns FragmentLoginState()

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.login()

        assertTrue(slot.captured.isLoading)
    }

    @Test
    fun `should show invalid password when INVALID_PASSWORD`() {

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.loginStateObserver.onChanged(LoginState.INVALID_PASSWORD)

        assertEquals(ErrorMessages.INVALID_PASSWORD, slot.captured.errorMessage)
    }

    @Test
    fun `should show user not found when USER_NOT_FOUND`() {

        val slot = slot<FragmentLoginState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {}

        viewModel.loginStateObserver.onChanged(LoginState.INVALID_PASSWORD)

        assertEquals(ErrorMessages.INVALID_PASSWORD, slot.captured.errorMessage)
        assertFalse(slot.captured.isLoading)
    }

    @Test
    fun `should show error when login network error`() {
        viewModel.loginStateObserver.onChanged(LoginState.NETWORK_ERROR)
        verify {
            toastsService.showMessage(ToastMessages.NETWORK_PROBLEM)
        }
    }
}