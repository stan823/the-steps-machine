package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main

import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
class FragmentStepCounterInteractorImplTest {

    @RelaxedMockK
    private lateinit var accountLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var navigationLiveData: MainNavigationLiveDataInterface

    private val interactor: FragmentStepCounterInteractorImpl by lazy {
        FragmentStepCounterInteractorImpl(accountLiveData, navigationLiveData)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun name() {

        val slot = slot<NavigationAction>()
        slot.clear()

        every {
            navigationLiveData.postValue(value = capture(slot))
        } answers {}
      //  interactor.onProfilePressed()
        slot.clear()
    }

    //    @Test
//    fun `should navigate to login when account empty`() {
//        c
//
//        interactor.onProfilePressed()
//
//        val navigationAction = slot.captured
//
//        if (navigationAction is Push) {
//            assertEquals(R.id.action_fragmentMain_to_fragmentLogin, navigationAction.action)
//        } else {
//            fail()
//        }
//    }
//
//    @Test
//    fun `should navigate to accounts when account not empty`() {
//        val slot = slot<NavigationAction>()
//        val account = AccountModel("aa","bb","cc")
//
//        every {
//            accountLiveData.getValue()
//        } returns account
//
//        every {
//            navigationLiveData.postValue(capture(slot))
//        } answers {}
//
//        interactor.onProfilePressed()
//
//        val navigationAction = slot.captured
//
//        if (navigationAction is Push) {
//            assertEquals(R.id.action_fragmentMain_to_fragmentAccount, navigationAction.action)
//        } else {
//            fail()
//        }
//    }
}