package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-06-05.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterProgressCirclePresenterImplTest {

    @RelaxedMockK
    private lateinit var targetLoader: TargetLocalLoader

    @RelaxedMockK
    private lateinit var stepsLoader: StepsLocalLoader

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    @RelaxedMockK
    private lateinit var timeUnitLiveData: TimeUnitLiveData

    private val presenter: StepCounterProgressCirclePresenterImpl by lazy {
        StepCounterProgressCirclePresenterImpl(
                targetLoader,
                stepsLoader,
                systemTimeProvider,
                timeUnitLiveData
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testCalculatePercentageFor50() {
        val result = presenter.calculateTargetPercentage(100, 200)

        assertEquals(result, 50f)
    }

    @Test
    fun testCalculatePercentageFor150() {
        val result = presenter.calculateTargetPercentage(300, 200)

        assertEquals(result, 100f)
    }
}