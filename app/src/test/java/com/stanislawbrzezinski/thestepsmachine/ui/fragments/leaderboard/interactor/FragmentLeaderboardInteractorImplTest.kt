package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor

import com.stanislawbrzezinski.thestepsmachine.NetworkUnavailableException
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.updateData
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LocaleService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.LeaderboardEntryModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardStateLiveDataInterface
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import rules.InitLiveDataIntrefaceRule
import java.lang.Exception
import java.net.SocketTimeoutException
import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-09-29.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLeaderboardInteractorImplTest {

    @get:Rule
    val liveDataRule = InitLiveDataIntrefaceRule()

    @RelaxedMockK
    private lateinit var accountLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var networkService: NetworkService

    @RelaxedMockK
    private lateinit var stateLiveData: FragmentLeaderboardStateLiveDataInterface

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    @RelaxedMockK
    private lateinit var stepsLocalLoader: StepsLocalLoader

    @RelaxedMockK
    private lateinit var loadingScreenLiveData: LoadingScreenLiveDataInterface

    @RelaxedMockK
    private lateinit var toastsService: ToastsService

    @RelaxedMockK
    private lateinit var localeService: LocaleService

    private var initialState: FragmentLeaderboardState = FragmentLeaderboardState()

    private val interactor: FragmentLeaderboardInteractorImpl by lazy {
        FragmentLeaderboardInteractorImpl(
                accountLiveData,
                networkService,
                stateLiveData,
                stepsLocalLoader,
                systemTimeProvider,
                localeService,
                loadingScreenLiveData,
                toastsService
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        every {
            stateLiveData.getValue()
        } returns initialState

        every {
            loadingScreenLiveData.getValue()
        } returns true
    }

    @Test
    fun `should call unfollow service`() {
        val guid = "guid"
        interactor.unfollowUser(guid)

        verify {
            networkService.unfollow(guid)
        }
    }

    @Test
    fun `should show something went wrong error when unfollow error`() {
        val exception = mockk<Exception>(relaxed = true)
        every {
            networkService.unfollow(any())
        } returns Single.error(exception)

        interactor.unfollowUser("guid")

        verify {
            toastsService.somethingWentWrong()
        }
    }

    @Test
    fun `should hide messages when fetches leaderboard`() {

        var callCounter = 0
        val dataSlot = slot<FragmentLeaderboardState>()

        every {
            accountLiveData.getValue()
        } returns null

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 0) {
                assertFalse(value.showNoInternetMessage)
                assertFalse(value.showSomethingWentWrongMessage)
                assertFalse(value.showConnectionsMessage)
                callCounter++
            }
        }

        every {
            networkService.getConnections
        } returns Single.just(listOf<LeaderboardEntryModel>())

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should display result when fetched leaderboard`() {

        var callCounter = 0
        val leaderboard = listOf<LeaderboardEntryModel>()
        val dataSlot = slot<FragmentLeaderboardState>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertEquals(value.leaderboardEntries, leaderboard)
            }
            callCounter++
        }

        every {
            networkService.getConnections
        } returns Single.just(leaderboard)

        every {
            accountLiveData.getValue()
        } returns null

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should hide LoadingSpinner when fetched leaderboard`() {

        var callCounter = 0
        val leaderboard = listOf<LeaderboardEntryModel>()
        val dataSlot = slot<FragmentLeaderboardState>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertFalse(value.showLoadingSpinner)
            }
            callCounter++
        }

        every {
            networkService.getConnections
        } returns Single.just(leaderboard)

        every {
            accountLiveData.getValue()
        } returns null

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should hide pull to refresh when fetched leaderboard`() {

        var callCounter = 0
        val leaderboard = listOf<LeaderboardEntryModel>()
        val dataSlot = slot<FragmentLeaderboardState>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertFalse(value.showPullToRefreshSpinner)
            }
            callCounter++
        }

        every {
            accountLiveData.getValue()
        } returns null

        every {
            networkService.getConnections
        } returns Single.just(leaderboard)

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should show noInternet message when NetworkUnavailable exception`() {

        var callCounter = 0
        val dataSlot = slot<FragmentLeaderboardState>()
        val exception = mockk<NetworkUnavailableException>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertTrue(value.showNoInternetMessage)
            }
            callCounter++
        }

        every {
            networkService.getConnections
        } returns Single.error(exception)

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should show noInternet message when SocketTimeout exception`() {

        var callCounter = 0
        val dataSlot = slot<FragmentLeaderboardState>()
        val exception = mockk<SocketTimeoutException>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertTrue(value.showNoInternetMessage)
            }
            callCounter++
        }

        every {
            networkService.getConnections
        } returns Single.error(exception)

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should show somethignWentWrong message when HttpException`() {

        var callCounter = 0
        val dataSlot = slot<FragmentLeaderboardState>()
        val exception = mockk<HttpException>()

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 1) {
                assertTrue(value.showSomethingWentWrongMessage)
            }
            callCounter++
        }

        every {
            networkService.getConnections
        } returns Single.error(exception)

        interactor.fetchLeaderboard()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }

    @Test
    fun `should display right date on updateDate`() {

        var callCounter = 0
        val dataSlot = slot<FragmentLeaderboardState>()

        every {
            localeService.locale
        } returns Locale.UK

        every {
            stateLiveData.postValue(capture(dataSlot))
        } answers {
            val value = dataSlot.captured
            if (callCounter == 0) {
                assertEquals("September 2019", value.currentMonth)
            }
            callCounter++
        }

        every {
            systemTimeProvider.getCurrentTimeInMilliseconds()
        } returns 1569825471628L

        interactor.updateDate()

        verify(atLeast = 1) {
            stateLiveData.updateData(any())
        }
    }
}
