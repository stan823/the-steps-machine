package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor

import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.slot
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
class FragmentRegistrationSuccessfulInteractorImplTest {

    @RelaxedMockK
    private lateinit var navigation: MainNavigationLiveDataInterface

    private val interactor: FragmentRegistrationSuccessfulInteractor by lazy {
        FragmentRegistrationSuccessfulInteractorImpl(navigation)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should pop backstack when on clicked`() {

        val slot = slot<NavigationAction>()

        every {
            navigation.postValue(capture(slot))
        } answers {}

        interactor.okClicked()

        val action = slot.captured
        assertTrue(action is Pop)
    }
}