package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor

import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LogService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-09-27.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSplashInteractorImplTest {

    @RelaxedMockK
    private lateinit var logService: LogService

    @RelaxedMockK
    private lateinit var accountsDao: AccountsDao

    @RelaxedMockK
    private lateinit var navigation: MainNavigationLiveDataInterface

    @RelaxedMockK
    private lateinit var accountsLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var firebaseRemoteConfigManager: FirebaseRemoteConfigManager

    @RelaxedMockK
    private lateinit var threadsService: ThreadsService

    private val interactor: FragmentSplashInteractorImpl by lazy {
        FragmentSplashInteractorImpl(
                accountsDao,
                navigation,
                accountsLiveData,
                firebaseRemoteConfigManager,
                threadsService,
                logService
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should navigate to main`() {

        interactor.runDbThread()

        verify(exactly = 1) {
            navigation.push(R.id.action_fragmentSplash_to_fragmentMain)
        }
    }

    @Test
    fun `should access db on separate thread`() {

        interactor.loadData()

        verify {
            threadsService.provideThread(interactor::runDbThread)
        }
    }

    @Test
    fun `should execute db thread`() {
        val thread = mockk<Thread>(relaxed = true)

        every {
            threadsService.provideThread(any())
        } returns thread

        interactor.loadData()

        verify {
            thread.start()
        }
    }

    @Test
    fun `should post account`() {

        val accountName = "name"
        val accountGuid = "guid"
        val accountToken = "token"
        val account = AccountEntity(
                name = accountName,
                token = accountToken,
                guid = accountGuid
        )

        every {
            accountsDao.getAccount()
        } returns account

        val slot = slot<AccountModel>()

        every {
            accountsLiveData.postValue(capture(slot))
        } answers {
            assertEquals(slot.captured.name, accountName)
            assertEquals(slot.captured.token, accountToken)
            assertEquals(slot.captured.guid, accountGuid)
        }

        interactor.runDbThread()

        verify {
            accountsLiveData.postValue(any())
        }
    }

    @Test
    fun `should post null when account null`() {

        every {
            accountsDao.getAccount()
        } returns null

        val slot = slot<AccountModel>()

        every {
            accountsLiveData.postValue(capture(slot))
        } answers {
            assertNull(slot.captured)
        }

        interactor.runDbThread()

        verify {
            accountsLiveData.postValue(any())
        }
    }

    @Test
    fun `should init firebaseConfig`() {
        interactor.loadData()

        verify {
            firebaseRemoteConfigManager.fetchSettings()
        }
    }
}