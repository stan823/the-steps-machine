package com.stanislawbrzezinski.thestepsmachine.ui.fragments.repo

import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ResultResponse
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.room.daos.*
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo.FragmentAccountRepoImpl
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Single
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-08-21.
 * Copyrights Stanisław Brzeziński
 */
class FragmentAccountRepoImplTest {

    @RelaxedMockK
    private lateinit var accountsDao: AccountsDao

    @RelaxedMockK
    private lateinit var networkService: NetworkService

    @RelaxedMockK
    private lateinit var accountsLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var firebaseAuthManager: FirebaseAuthManager

    @RelaxedMockK
    private lateinit var stepsLocalInserter: StepsLocalInserter

    private val repo: FragmentAccountRepoImpl by lazy {
        FragmentAccountRepoImpl(
                accountsDao,
                networkService,
                accountsLiveData,
                firebaseAuthManager,
                stepsLocalInserter
        )
    }

    private val oldName = "oldName"
    private val token = "token"
    private val guid = "guid"

    @Before
    fun setUp() {
        MockKAnnotations.init(this)

        every {
            accountsLiveData.getValue()
        } returns AccountModel(
                name = oldName,
                token = token,
                guid = guid
        )
    }

    @Test
    fun `should update db when name changed`() {

        val newName = "newName"

        val resultResponse = mockk<ResultResponse>()

        every {
            networkService.changeName(newName)
        } returns Single.just(resultResponse)

        val entiyDeleteSlot = slot<AccountEntity>()
        every {
            accountsDao.deleteAccount(capture(entiyDeleteSlot))
        } answers {
            assertEquals(entiyDeleteSlot.captured.name, oldName)
            assertEquals(entiyDeleteSlot.captured.token, token)
            assertEquals(entiyDeleteSlot.captured.guid, guid)
        }

        val entityInsertSlot = slot<AccountEntity>()
        every {
            accountsDao.insertAccount(capture(entityInsertSlot))
        } answers {
            assertEquals(entityInsertSlot.captured.name, newName)
            assertEquals(entityInsertSlot.captured.token, token)
            assertEquals(entityInsertSlot.captured.guid, guid)
        }

        repo.changeName(newName, {}, {})

        verifyOrder {
            accountsDao.deleteAccount(any())
            accountsDao.insertAccount(any())
        }
    }
}