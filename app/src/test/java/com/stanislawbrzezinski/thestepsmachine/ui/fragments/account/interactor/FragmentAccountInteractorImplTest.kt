package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.NetworkErrors
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo.FragmentAccountRepo
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountStateLiveDataInterface
import io.mockk.*
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import retrofit2.HttpException
import rules.InitLiveDataIntrefaceRule

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
class FragmentAccountInteractorImplTest {

    @get:Rule
    val rule = InitLiveDataIntrefaceRule()

    @RelaxedMockK
    private lateinit var accountsDao: AccountsDao

    @RelaxedMockK
    private lateinit var firebaseAuthManager: FirebaseAuthManager

    @RelaxedMockK
    private lateinit var accountLiveData: AccountLiveDataInterface

    @RelaxedMockK
    private lateinit var navigationLiveData: MainNavigationLiveDataInterface

    @RelaxedMockK
    private lateinit var repo: FragmentAccountRepo

    @RelaxedMockK
    private lateinit var stateLiveData: FragmentAccountStateLiveDataInterface

    @RelaxedMockK
    private lateinit var errorService: ErrorService

    @RelaxedMockK
    private lateinit var threadsService: ThreadsService

    private val interactor: FragmentAccountInteractorImpl by lazy {
        FragmentAccountInteractorImpl(
                accountsDao,
                firebaseAuthManager,
                accountLiveData,
                navigationLiveData,
                repo,
                stateLiveData,
                errorService,
                threadsService
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should post null as account when logged out`() {
        interactor.onLogOut()

        verify {
            accountLiveData.postValue(null)
        }
    }

    @Test
    fun `should pop back when logged out`() {
        val slot = slot<NavigationAction>()

        every {
            navigationLiveData.postValue(capture(slot))
        } answers {
            assertTrue(slot.captured is Pop)
        }

        interactor.onLogOut()

        verify {
            navigationLiveData.postValue(any())
        }
    }

    @Test
    fun `should start delete thread when logged out`() {
        interactor.onLogOut()

        verify {
            threadsService.provideThread(interactor::loggOutRunner)
        }
    }

    @Test
    fun `should start delete account thread when logged out`() {
        val threadMock = mockk<Thread>(relaxed = true)

        every {
            threadsService.provideThread(any())
        } returns threadMock

        interactor.onLogOut()

        verify {
            threadMock.start()
        }
    }

    @Test
    fun `should delete account from database when log out thread started`() {

        val accountModel = AccountModel(
                token = "token",
                guid = "guid",
                name = "name"
        )

        every {
            accountLiveData.getValue()
        } returns accountModel

        val deleteSlot = slot<AccountEntity>()

        every {
            accountsDao.deleteAccount(capture(deleteSlot))
        } answers {
            val value = deleteSlot.captured
            assertEquals(value.name, accountModel.name)
            assertEquals(value.guid, accountModel.guid)
            assertEquals(value.token, accountModel.token)
        }

        interactor.loggOutRunner()

        verify {
            accountsDao.deleteAccount(any())
        }
    }

    @Test
    fun `should log out from firebase when log out thread runned`() {

        val account = mockk<AccountModel>(relaxed = true)

        every {
            accountLiveData.getValue()
        } returns account

        interactor.loggOutRunner()
        verify {
            firebaseAuthManager.logOut()
        }
    }

    @Test
    fun `should pop back when back pressed`() {

        val navigationSlot = slot<NavigationAction>()

        every {
            navigationLiveData.postValue(capture(navigationSlot))
        } answers {
            assertTrue(navigationSlot.captured is Pop)
        }

        interactor.onBack()

        verify {
            navigationLiveData.postValue(any())
        }
    }

    @Test
    fun `should show loading screen on name change`() {
        val newName = "newName"

        val stateSlot = slot<FragmentAccountState>()

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(isLoading = false)

        every {
            stateLiveData.postValue(capture(stateSlot))
        } answers {
            assertTrue(stateSlot.captured.isLoading)
        }

        interactor.onChange(newName)

        verify {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should call change on repo when name change`() {
        val name = "some name"

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(isLoading = false)

        interactor.onChange(name)

        verify {
            repo.changeName(
                    name,
                    interactor::onChangeNameSuccess,
                    interactor::onChangeNameError
            )
        }
    }

    @Test
    fun `should show success message when change name succes`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(showSuccessMessage = false)

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertTrue(slot.captured.showSuccessMessage)
        }

        interactor.onChangeNameSuccess()
    }

    @Test
    fun `should hide error message when change name success`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(errorMessage = ErrorMessages.NAME_TOO_SHORT)

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertNull(slot.captured.errorMessage)
        }

        interactor.onChangeNameSuccess()
    }

    @Test
    fun `should hide loading spinner when change name success`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(isLoading = true)

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertFalse(slot.captured.isLoading)
        }

        interactor.onChangeNameSuccess()
    }

    @Test
    fun `should hide loading message when change name error`() {

        var statesCount = 0
        val throwable = mockk<Throwable>()

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(showSuccessMessage = true)

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            if (statesCount == 0) {
                assertFalse(slot.captured.showSuccessMessage)
            }
            statesCount++
        }

        interactor.onChangeNameError(throwable)

        verify {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should show not authorised error when change name token not found response`() {

        var callsCount = 0

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(errorMessage = null)

        val error = mockk<HttpException>()

        every {
            errorService.parseException(error)
        } returns NetworkErrors.TOKEN_NOT_FOUND

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            if (callsCount == 1) {
                assertEquals(
                        slot.captured.errorMessage,
                        ErrorMessages.NOT_AUTHORISED
                )
            }
            callsCount++
        }

        interactor.onChangeNameError(error)

        verify(exactly = 2) {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should show name in use error when change name and user exists`() {

        var callsCount = 0

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(errorMessage = null)

        val error = mockk<HttpException>()

        every {
            errorService.parseException(error)
        } returns NetworkErrors.USER_ALREADY_EXISTS

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            if (callsCount == 1) {
                assertEquals(
                        slot.captured.errorMessage,
                        ErrorMessages.NAME_IN_USE
                )
            }
            callsCount++
        }

        interactor.onChangeNameError(error)

        verify(exactly = 2) {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should show something worn error when change name and unsupported http error`() {

        var callsCount = 0

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(errorMessage = null)

        val error = mockk<HttpException>()

        every {
            errorService.parseException(error)
        } returns NetworkErrors.UNKNOWN

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            if (callsCount == 1) {
                assertEquals(
                        slot.captured.errorMessage,
                        ErrorMessages.SOMETHING_WENT_WRONG
                )
            }
            callsCount++
        }

        interactor.onChangeNameError(error)

        verify(exactly = 2) {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should show something worn error when change name and unsupported error`() {

        var callsCount = 0

        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(errorMessage = null)

        val error = mockk<Exception>()

        val slot = slot<FragmentAccountState>()

        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            if (callsCount == 1) {
                assertEquals(
                        slot.captured.errorMessage,
                        ErrorMessages.SOMETHING_WENT_WRONG
                )
            }
            callsCount++
        }

        interactor.onChangeNameError(error)

        verify(exactly = 2) {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should call repo when delete called`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState()

        interactor.deleteAccount()

        verify {
            repo.deleteAccount(
                    interactor::deleteAccountSuccessCallback,
                    interactor::deleteAccountErrorCallback
            )
        }
    }

    @Test
    fun `should show something wrong message when account not deleted`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState()

        val slot = slot<FragmentAccountState>()
        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertEquals(
                    slot.captured.errorMessage,
                    ErrorMessages.SOMETHING_WENT_WRONG
            )
        }

        interactor.deleteAccountErrorCallback()

        verify {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should navigate back when account deleted`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState()

        val slot = slot<NavigationAction>()

        every {
            navigationLiveData.postValue(capture(slot))
        } answers {
            assertTrue(slot.captured is Pop)
        }

        interactor.deleteAccountSuccessCallback()

        verify {
            navigationLiveData.postValue(any())
        }
    }

    @Test
    fun `should hide loading when  account not  deleted`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(isLoading = false)

        val slot = slot<FragmentAccountState>()
        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertFalse(slot.captured.isLoading)
        }

        interactor.deleteAccountSuccessCallback()

        verify {
            stateLiveData.postValue(any())
        }
    }

    @Test
    fun `should hide loading when  account deleted`() {
        every {
            stateLiveData.getValue()
        } returns FragmentAccountState(isLoading = false)

        val slot = slot<FragmentAccountState>()
        every {
            stateLiveData.postValue(capture(slot))
        } answers {
            assertFalse(slot.captured.isLoading)
        }

        interactor.deleteAccountSuccessCallback()

        verify {
            stateLiveData.postValue(any())
        }
    }
}