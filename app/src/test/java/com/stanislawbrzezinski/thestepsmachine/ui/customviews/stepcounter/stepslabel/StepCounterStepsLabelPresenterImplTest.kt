package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Assert.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-06-05.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterStepsLabelPresenterImplTest {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var stepsLoader: StepsLocalLoader

    @RelaxedMockK
    private lateinit var timeUnitLiveData: TimeUnitLiveData

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    private val presenter: StepCounterStepsLabelPresenterImpl by lazy {
        StepCounterStepsLabelPresenterImpl(
                stepsLoader,
                timeUnitLiveData,
                systemTimeProvider
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testFormattedMapper() {
        val result = presenter.formattedStepsMapper(10)

        assertEquals("10", result)
    }
}