package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import io.mockk.MockKAnnotations
import io.mockk.impl.annotations.RelaxedMockK
import org.junit.Before
import org.junit.Rule
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 2019-06-06.
 * Copyrights Stanisław Brzeziński
 */
class StepsChartPresenterImplTest {

    @get:Rule
    val instantExecutorRule = InstantTaskExecutorRule()

    @RelaxedMockK
    private lateinit var timeUnitLiveData: TimeUnitLiveData

    @RelaxedMockK
    private lateinit var stepsLoader: StepsLocalLoader

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    private val presenter: StepsChartPresenterImpl by lazy {
        StepsChartPresenterImpl(
                timeUnitLiveData,
                stepsLoader
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun testPairToChartEntriesMapper() {
        // TODO: Needs test
    }
}
