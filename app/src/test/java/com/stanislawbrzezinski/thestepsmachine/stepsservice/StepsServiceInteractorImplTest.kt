package com.stanislawbrzezinski.thestepsmachine.stepsservice

import io.mockk.MockKAnnotations
import io.mockk.confirmVerified
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsServiceInteractorImplTest {

    @RelaxedMockK
    private lateinit var repository: StepsServiceRepository

    private val interactor: StepsServiceInteractor by lazy {
        StepsServiceInteractorImpl(repository)
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should repository record step when step recored`() {
        interactor.recordStep()

        verify {
            repository.recordStep()
        }

        confirmVerified(repository)
    }
}