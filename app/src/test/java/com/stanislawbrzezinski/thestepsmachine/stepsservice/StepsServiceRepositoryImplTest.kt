package com.stanislawbrzezinski.thestepsmachine.stepsservice

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.retrofit.StepsRetrofitService
import io.mockk.MockKAnnotations
import io.mockk.confirmVerified
import io.mockk.impl.annotations.RelaxedMockK
import io.mockk.verify
import org.junit.Before
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsServiceRepositoryImplTest {

    @RelaxedMockK
    private lateinit var stepsInserter: StepsLocalInserter

    @RelaxedMockK
    private lateinit var stepsLoader: StepsLocalLoader

    @RelaxedMockK
    private lateinit var systemTimeProvider: SystemTimeProvider

    @RelaxedMockK
    private lateinit var stepsRetrofitService: StepsRetrofitService

    @RelaxedMockK
    private lateinit var firebaseRemoteConfigManager: FirebaseRemoteConfigManager

    private val repo: StepsServiceRepositoryImpl by lazy {
        StepsServiceRepositoryImpl(
                stepsInserter,
                stepsLoader,
                systemTimeProvider,
                stepsRetrofitService,
                firebaseRemoteConfigManager
        )
    }

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun `should insert step when step recorded`() {
        repo.recordStep()

        verify(exactly = 1) {
            stepsInserter.insertStep()
        }

        confirmVerified(stepsInserter)
    }
}