package com.stanislawbrzezinski.thestepsmachine.services.errorservice

import com.google.gson.Gson
import io.mockk.every
import io.mockk.mockk
import org.junit.Assert.*
import org.junit.Test
import retrofit2.HttpException

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
class ErrorServiceImplTest {

    private val errorService = ErrorServiceImpl(Gson())

    @Test
    fun `should be error 2101 for supported code`() {
        val exception = createException(
                412, """{"error": 2101}"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.USER_ALREADY_EXISTS, result)
    }

    @Test
    fun `should be RESOURCES_NOT_FOUND for error 2102 for supported code`() {
        val exception = createException(
                412, """{"error": 2102}"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.RESOURCES_NOT_FOUND, result)
    }

    @Test
    fun `should be unknown error for unsupported code`() {
        val exception = createException(
                415, """{"error": 2101}"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.UNKNOWN, result)
    }

    @Test
    fun `should be unknown error for missformated message`() {
        val exception = createException(
                412, """{"error": 2101"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.UNKNOWN, result)
    }

    @Test
    fun `should be unknown error for unknonw error code`() {
        val exception = createException(
                412, """{"error": 5000}"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.UNKNOWN, result)
    }

    @Test
    fun `should be unknown error for wrong json formate`() {
        val exception = createException(
                412, """{"error": 5000}"""
        )
        val result = errorService.parseException(exception)
        assertEquals(NetworkErrors.UNKNOWN, result)
    }

    private fun createException(code: Int, message: String): HttpException {
        val exception = mockk<HttpException>()
        every {
            exception.code()
        } returns code

        every {
            exception.message()
        } returns message

        return exception
    }
}