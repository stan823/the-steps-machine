package com.stanislawbrzezinski.thestepsmachine.extensions

import org.junit.Assert
import org.junit.Assert.*
import org.junit.Test

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class FloatExtensionsKtTest {

    @Test
    fun testPercentageToDegreeFor0() {
        val result = 0f.percentateToDegree()
        Assert.assertEquals(0f, result)
    }

    @Test
    fun testPercentageToDegreeFor50() {
        val result = 50f.percentateToDegree()
        Assert.assertEquals(180f, result)
    }

    @Test
    fun testPercentageToDegreeFor75() {
        val result = 75f.percentateToDegree()
        Assert.assertEquals(270f, result)
    }

    @Test
    fun testPercentageToDegreeFor100() {
        val result = 100f.percentateToDegree()
        Assert.assertEquals(360f, result)
    }

    @Test
    fun testPercentageToDegreeForNegative() {
        val result = (-50f).percentateToDegree()
        Assert.assertEquals(0f, result)
    }

    @Test
    fun testPercentageToDegreeOver100() {
        val result = (101f).percentateToDegree()
        Assert.assertEquals(360f, result)
    }

    @Test
    fun testDegreesToPercentageForNegative() {
        val result = (-1f).degreeToPercentage()
        assertEquals(0f, result)
    }

    @Test
    fun testDegreesToPercentageFor0() {
        val result = (0f).degreeToPercentage()
        assertEquals(0f, result)
    }

    @Test
    fun testDegreesToPercentageFor90() {
        val result = (90f).degreeToPercentage()
        assertEquals(25f, result)
    }

    @Test
    fun testDegreesToPercentageFor360() {
        val result = (360f).degreeToPercentage()
        assertEquals(100f, result)
    }

    @Test
    fun testDegreesToPercentageForOver360() {
        val result = (400f).degreeToPercentage()
        assertEquals(100f, result)
    }
}