package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import org.junit.Test

import org.junit.Assert.*
import java.util.*

/**
 * Created by Stanisław Brzeziński on 04/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class SystemTimeProviderImplTest {

    val systemTimeProvider = SystemTimeProviderImpl()

    @Test
    fun `should reset to first second of the month`() {
        val calendar = Calendar.getInstance()
        val result = systemTimeProvider.getTimeOfFirstDayOfCurrentMonth(calendar)
        val resCalendar = Calendar.getInstance()
        resCalendar.timeInMillis = result
        assertEquals(calendar.get(Calendar.MONTH), resCalendar.get(Calendar.MONTH))
        assertEquals(1, resCalendar.get(Calendar.DAY_OF_MONTH))
        assertEquals(0, resCalendar.get(Calendar.HOUR_OF_DAY))
        assertEquals(0, resCalendar.get(Calendar.MINUTE))
        assertEquals(0, resCalendar.get(Calendar.SECOND))
    }
}