package rules

import io.mockk.mockkStatic
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

/**
 * Created by Stanisław Brzeziński on 2019-09-29.
 * Copyrights Stanisław Brzeziński
 */
class InitLiveDataIntrefaceRule : TestRule {

    override fun apply(base: Statement?, description: Description?): Statement {
        return object : Statement() {
            override fun evaluate() {
                mockkStatic("com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterfaceKt")
                base?.evaluate()
            }
        }
    }
}