package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import java.util.*

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
interface SystemTimeProvider {
    fun getCurrentTimeInMilliseconds(): Long
    fun getTimeOfFirstDayOfCurrentMonth(calendar: Calendar): Long
    fun getTimeOfFirstDayOfCurrentDay(calendar: Calendar): Long
    fun getTimeOfFirstDayOfCurrentWeek(calendar: Calendar): Long
}