package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtils
import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtilsImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppUtilsModule {

    @Provides
    @Singleton
    fun provideValidationUtils(): ValidationUtils {
        return ValidationUtilsImpl()
    }
}