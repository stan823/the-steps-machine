package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

/**
 * Created by Stanisław Brzeziński on 2019-10-15.
 * Copyrights Stanisław Brzeziński
 */
interface LogService {
    fun log(tag:String, message: String)
}