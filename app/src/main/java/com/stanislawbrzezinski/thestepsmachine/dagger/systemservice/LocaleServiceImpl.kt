package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
class LocaleServiceImpl : LocaleService {

    override val locale: Locale
        get() = Locale.getDefault()
}