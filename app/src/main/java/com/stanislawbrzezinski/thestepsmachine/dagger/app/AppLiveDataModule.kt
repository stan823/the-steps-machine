package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.stanislawbrzezinski.thestepsmachine.livedata.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 17/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppLiveDataModule {

    @Singleton
    @Provides
    fun loadingScreenLiveData(): LoadingScreenLiveDataInterface{
        return LoadingScreenLiveData()
    }

    @Singleton
    @Provides
    fun accountLiveData(): AccountLiveDataInterface {
        return AccountLiveData()
    }

    @Singleton
    @Provides
    fun timeUnitLiveData(): TimeUnitLiveData {
        return TimeUnitLiveData()
    }

    @Singleton
    @Provides
    fun mainNavigationLiveData(): MainNavigationLiveDataInterface {
        return MainNavigationLiveData()
    }

    @Singleton
    @Provides
    fun currentUserLiveData(): CurrentUserLiveData {
        return CurrentUserLiveData()
    }
}