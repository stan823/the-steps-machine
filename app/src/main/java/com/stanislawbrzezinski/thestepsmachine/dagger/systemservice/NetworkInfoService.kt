package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

/**
 * Created by Stanisław Brzeziński on 2019-08-05.
 * Copyrights Stanisław Brzeziński
 */
interface NetworkInfoService {
    fun isConnected(): Boolean
}