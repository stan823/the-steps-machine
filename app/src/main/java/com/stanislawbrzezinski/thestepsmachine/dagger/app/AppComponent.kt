package com.stanislawbrzezinski.thestepsmachine.dagger.app

import android.content.Context
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.*
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver.TargetLocalSaver
import com.stanislawbrzezinski.thestepsmachine.enums.AppFlavour
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.livedata.*
import com.stanislawbrzezinski.thestepsmachine.retrofit.AccountRetrofitService
import com.stanislawbrzezinski.thestepsmachine.retrofit.ConnectionsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.retrofit.StepsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtils
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 17/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Component(modules = [
    AppLiveDataModule::class,
    AppFlavourModule::class,
    AppDatabaseModule::class,
    AppContextModule::class,
    AppSystemServiceModule::class,
    AppDataSourcesModule::class,
    AppUtilsModule::class,
    AppRetrofitModule::class,
    AppServicesModule::class,
    AppFirebaseModule::class
])
@Singleton
interface AppComponent {

    fun mainNavigationLiveData(): MainNavigationLiveDataInterface
    fun timeUnitLiveData(): TimeUnitLiveData
    fun currentUserLiveData(): CurrentUserLiveData
    fun accountLiveData(): AccountLiveDataInterface
    fun loadingScreenLiveData(): LoadingScreenLiveDataInterface
    fun provideStepsDao(): StepsDao

    fun systemTimeService(): SystemTimeProvider
    fun errorService(): ErrorService
    fun toastsService(): ToastsService
    fun networkInfoService(): NetworkInfoService
    fun threadsService(): ThreadsService
    fun localeService(): LocaleService
    fun logService(): LogService
    fun firebaseManager(): FirebaseAuthManager
    fun firebaseRemoteConfigManager(): FirebaseRemoteConfigManager

    // utils
    fun validationUtils(): ValidationUtils

    // retrofit
    fun networkService(): NetworkService
    fun accountService(): AccountRetrofitService
    fun connectionsService(): ConnectionsRetrofitService
    fun stepsService(): StepsRetrofitService

    fun appFlavour(): AppFlavour
    fun database(): AppDatabase
    fun context(): Context

    fun stepsLocalInserter(): StepsLocalInserter
    fun stepsLocalReader(): StepsLocalLoader
    fun targetsLocalSaver(): TargetLocalSaver
    fun targetsLocalLoader(): TargetLocalLoader
}