package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.stanislawbrzezinski.thestepsmachine.firebase.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppFirebaseModule {

    @Provides
    @Singleton
    fun providesFirebaseAuthManager(): FirebaseAuthManager {
        return FirebaseAuthManagerImpl()
    }

    @Provides
    @Singleton
    fun providesFirebaseRemoteConfigManager(): FirebaseRemoteConfigManager {
        return FirebaseRemoteConfigManagerImpl()
    }
}