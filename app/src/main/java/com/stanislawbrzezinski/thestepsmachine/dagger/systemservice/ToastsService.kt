package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import com.stanislawbrzezinski.thestepsmachine.enums.ToastMessages

/**
 * Created by Stanisław Brzeziński on 2019-05-13.
 * Copyrights Stanisław Brzeziński
 */
interface ToastsService {
    fun showString(text: String)
    fun showMessage(networkProblem: ToastMessages)
    fun somethingWentWrong()
}