package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
interface LocaleService {
    val locale: Locale
}