package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import android.util.Log

/**
 * Created by Stanisław Brzeziński on 2019-10-15.
 * Copyrights Stanisław Brzeziński
 */
class LogServiceImpl :LogService{

    override fun log(tag: String, message: String) {
        Log.d(tag, message)
    }
}