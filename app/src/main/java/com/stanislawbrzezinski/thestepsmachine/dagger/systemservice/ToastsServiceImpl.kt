package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import android.content.Context
import android.widget.Toast
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.enums.ToastMessages

/**
 * Created by Stanisław Brzeziński on 2019-05-13.
 * Copyrights Stanisław Brzeziński
 */
class ToastsServiceImpl(
    private val context: Context
) : ToastsService {

    override fun showString(text: String) {
        Toast.makeText(context, text, Toast.LENGTH_SHORT).show()
    }

    override fun showMessage(message: ToastMessages) {
        Toast.makeText(context, message.strResource, Toast.LENGTH_SHORT).show()
    }

    override fun somethingWentWrong() {
        Toast.makeText(context, R.string.error_something_went_wrong, Toast.LENGTH_SHORT).show()
    }
}