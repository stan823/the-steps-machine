package com.stanislawbrzezinski.thestepsmachine.dagger.app

import android.content.Context
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppContextModule(
    private val context: Context
) {

    @Provides
    @Singleton
    fun provideContext() = context
}