package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.enums.AppFlavour
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppFlavourModule {

    @Provides
    @Singleton
    fun provideAppFlavour(): AppFlavour {
        return AppFlavour.getFlavourByName(BuildConfig.FLAVOR)
    }
}