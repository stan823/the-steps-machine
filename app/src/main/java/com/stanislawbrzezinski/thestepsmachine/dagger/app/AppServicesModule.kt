package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.google.gson.Gson
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorServiceImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppServicesModule {

    @Singleton
    @Provides
    fun provideErrorsService(gson: Gson): ErrorService {
        return ErrorServiceImpl(gson)
    }
}