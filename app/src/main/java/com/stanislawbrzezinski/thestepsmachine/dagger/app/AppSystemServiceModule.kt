package com.stanislawbrzezinski.thestepsmachine.dagger.app

import android.content.Context
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.*
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppSystemServiceModule(
    private val context: Context
) {


    @Provides
    @Singleton
    fun provideLogService():LogService{
        return LogServiceImpl()
    }

    @Provides
    @Singleton
    fun provideThreadsService(): ThreadsService {
        return ThreadsServiceImpl()
    }

    @Provides
    @Singleton
    fun provideNetworkService(): NetworkInfoService {
        return NetworkInfoServiceImpl(context)
    }


    @Provides
    @Singleton
    fun provideSystemTimeService(): SystemTimeProvider {
        return SystemTimeProviderImpl()
    }

    @Provides
    @Singleton
    fun provideToastsService(): ToastsService {
        return ToastsServiceImpl(context)
    }

    @Provides
    @Singleton
    fun provideLocaleService(): LocaleService {
        return LocaleServiceImpl()
    }
}