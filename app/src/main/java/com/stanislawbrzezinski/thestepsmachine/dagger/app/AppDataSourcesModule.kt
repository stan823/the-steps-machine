package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsRoomInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsRoomLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetRoomLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver.TargetLocalSaver
import com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver.TargetRoomSaver
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppDataSourcesModule {

    @Provides
    @Singleton
    fun provideTargetLoader(
        appDatabase: AppDatabase
    ): TargetLocalLoader {
        return TargetRoomLoader(
                appDatabase.getTargetsDao()
        )
    }

    @Provides
    @Singleton
    fun provideStepsLocalInserter(
        appDatabase: AppDatabase,
        timeProvider: SystemTimeProvider
    ): StepsLocalInserter {
        return StepsRoomInserter(
                appDatabase.getStepsDao(),
                appDatabase.getDaysDao(),
                appDatabase.getWeeksDao(),
                appDatabase.getMonthsDao(),
                timeProvider)
    }

    @Provides
    @Singleton
    fun provideStepsLocalReader(appDatabase: AppDatabase): StepsLocalLoader {
        return StepsRoomLoader(
                appDatabase.getStepsDao(),
                appDatabase.getDaysDao(),
                appDatabase.getWeeksDao(),
                appDatabase.getMonthsDao()
        )
    }

    @Provides
    @Singleton
    fun provideTargetsSaver(
        appDatabase: AppDatabase,
        timeProvider: SystemTimeProvider
    ): TargetLocalSaver {
        return TargetRoomSaver(
                appDatabase.getTargetsDao(),
                timeProvider
        )
    }
}