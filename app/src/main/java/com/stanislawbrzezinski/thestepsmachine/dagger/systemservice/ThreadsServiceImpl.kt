package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

/**
 * Created by Stanisław Brzeziński on 2019-09-27.
 * Copyrights Stanisław Brzeziński
 */
class ThreadsServiceImpl : ThreadsService {

    override fun provideThread(action: () -> Unit): Thread {
        return Thread(action)
    }
}