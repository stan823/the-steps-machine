package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import java.util.Calendar

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class SystemTimeProviderImpl
    : SystemTimeProvider {

    override fun getCurrentTimeInMilliseconds(): Long {
        return System.currentTimeMillis()
    }

    override fun getTimeOfFirstDayOfCurrentMonth(inputCalendar: Calendar): Long {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = inputCalendar.timeInMillis
        calendar.set(Calendar.DAY_OF_MONTH, 1)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        return calendar.timeInMillis
    }

    override fun getTimeOfFirstDayOfCurrentWeek(inputCalendar: Calendar): Long {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = inputCalendar.timeInMillis
        calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        val mils = calendar.timeInMillis
        return mils
    }

    override fun getTimeOfFirstDayOfCurrentDay(inputCalendar: Calendar): Long {
        val calendar = Calendar.getInstance()
        calendar.timeInMillis = inputCalendar.timeInMillis
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        return calendar.timeInMillis
    }
}