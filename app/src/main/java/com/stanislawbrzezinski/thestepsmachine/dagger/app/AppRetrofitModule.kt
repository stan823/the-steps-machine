package com.stanislawbrzezinski.thestepsmachine.dagger.app

import com.google.gson.Gson
import com.stanislawbrzezinski.thestepsmachine.Endpoints
import com.stanislawbrzezinski.thestepsmachine.NetworkUnavailableException
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.NetworkInfoService
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.isEmpty
import com.stanislawbrzezinski.thestepsmachine.retrofit.*
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 2019-07-23.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppRetrofitModule {

    @Provides
    @Singleton
    fun providesHttpClient(
        networkInfoService: NetworkInfoService,
        accountLiveData: AccountLiveDataInterface
    ): OkHttpClient {
        return OkHttpClient.Builder()
                .addInterceptor {
                    if (!accountLiveData.getValue().isEmpty()) {
                        val request = it.request().newBuilder()
                                .addHeader("token", accountLiveData.getValue()?.token ?: "")
                                .build()
                        it.proceed(request)
                    } else {
                        it.proceed(it.request())
                    }
                }
                .addInterceptor {
                    if (!networkInfoService.isConnected()) {
                        val request = it.request()
                        throw NetworkUnavailableException()
                    }
                    it.proceed(it.request())
                }.build()
    }

    @Provides
    @Singleton
    fun networkService(
        connetionsRetrofitService: ConnectionsRetrofitService,
        accountRetrofitService: AccountRetrofitService
    ): NetworkService {
        return NetworkServiceImpl(
                connetionsRetrofitService,
                accountRetrofitService
        )
    }

    @Provides
    @Singleton
    fun provideRetrofit(client: OkHttpClient): Retrofit {
        return Retrofit.Builder()
                .client(client)
                .baseUrl(Endpoints.baseUrl)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build()
    }

    @Provides
    @Singleton
    fun provideGson(): Gson {
        return Gson()
    }

    @Provides
    @Singleton
    fun provideAccountService(retrofit: Retrofit): AccountRetrofitService {
        return retrofit.create(AccountRetrofitService::class.java)
    }

    @Provides
    @Singleton
    fun provideConnectionsService(retrofit: Retrofit): ConnectionsRetrofitService {
        return retrofit.create(ConnectionsRetrofitService::class.java)
    }

    @Provides
    @Singleton
    fun provideStepsService(retrofit: Retrofit): StepsRetrofitService {
        return retrofit.create(StepsRetrofitService::class.java)
    }
}