package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

/**
 * Created by Stanisław Brzeziński on 2019-09-27.
 * Copyrights Stanisław Brzeziński
 */
interface ThreadsService {
    fun provideThread(action: () -> Unit): Thread
}