package com.stanislawbrzezinski.thestepsmachine.dagger.app

import android.content.Context
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class AppDatabaseModule {

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase {
        return AppDatabase.getInstance(context)
    }

    @Provides
    @Singleton
    fun provideStepsDao(appDatabase: AppDatabase): StepsDao {
        return appDatabase.getStepsDao()
    }
}