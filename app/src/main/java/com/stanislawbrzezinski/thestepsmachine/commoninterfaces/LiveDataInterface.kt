package com.stanislawbrzezinski.thestepsmachine.commoninterfaces

import androidx.lifecycle.*

/**
 * Created by Stanisław Brzeziński on 2019-07-23.
 * Copyrights Stanisław Brzeziński
 */
interface LiveDataInterface<T> {

    fun postValue(value: T?)
    fun observe(lifecycleOwner: LifecycleOwner, observer: Observer<in T>)
    fun observeForever(observer: Observer<in T>)
    fun getValue(): T?
}

fun <tInput, tResult> LiveDataInterface<tInput>.map(mapper: (tInput?) -> tResult): LiveData<tResult> {
    return Transformations.map(this as LiveData<tInput>, mapper) as LiveData<tResult>
}

fun <T> LiveDataInterface<T>.updateData(callback: (data: T) -> T) {
    getValue()?.let {
        val newValue = callback(it)
        postValue(newValue)
    }
}
