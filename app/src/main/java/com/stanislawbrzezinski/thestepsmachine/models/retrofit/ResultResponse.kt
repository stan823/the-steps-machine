package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-07-24.
 * Copyrights Stanisław Brzeziński
 */
data class ResultResponse(
    val result: Boolean? = true
)