package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-09-17.
 * Copyrights Stanisław Brzeziński
 */
data class StepsUploadRequest(
    val steps: Int
)