package com.stanislawbrzezinski.thestepsmachine.models.retrofit

import com.google.gson.annotations.SerializedName
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity

/**
 * Created by Stanisław Brzeziński on 2019-07-29.
 * Copyrights Stanisław Brzeziński
 */
data class AccountModel(
    @SerializedName(RetrofitParams.AccountModel.NAME)
    var name: String? = null,

    @SerializedName(RetrofitParams.AccountModel.GUID)
    var guid: String? = null,

    @SerializedName(RetrofitParams.AccountModel.TOKEN)
    var token: String? = null
)

fun AccountModel.toEntity() = AccountEntity(
        token = this.token ?: "",
        guid = this.guid ?: "",
        name = this.name ?: ""
)

fun AccountModel?.isEmpty(): Boolean {
    return this == null ||
            name.isNullOrEmpty() ||
            guid.isNullOrEmpty() ||
            token.isNullOrEmpty()
}