package com.stanislawbrzezinski.thestepsmachine.models.retrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by Stanisław Brzeziński on 2019-09-16.
 * Copyrights Stanisław Brzeziński
 */
data class ConnectionFollowRequest(

    @SerializedName("guid")
    val guid: String
)