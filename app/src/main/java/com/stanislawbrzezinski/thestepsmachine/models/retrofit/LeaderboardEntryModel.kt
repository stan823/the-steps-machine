package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
data class LeaderboardEntryModel(
    val guid: String? = null,
    val name: String? = null,
    val steps: Int? = null
)