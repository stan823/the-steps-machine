package com.stanislawbrzezinski.thestepsmachine.models

import com.stanislawbrzezinski.thestepsmachine.Constants.Targets.DEFAULT_DAILY_TARGET
import com.stanislawbrzezinski.thestepsmachine.Constants.Targets.DEFAULT_WEEKLY_TARGET
import com.stanislawbrzezinski.thestepsmachine.Constants.Targets.DEFAULT_MONTHLY_TARGET

/**
 * Created by Stanisław Brzeziński on 2019-05-09.
 * Copyrights Stanisław Brzeziński
 */
class TargetsModel(
    val daily: Int = DEFAULT_DAILY_TARGET,
    val weekly: Int = DEFAULT_WEEKLY_TARGET,
    val monthly: Int = DEFAULT_MONTHLY_TARGET
)