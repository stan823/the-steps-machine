package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
data class ChangeNameRequest(
    var name: String? = null
)