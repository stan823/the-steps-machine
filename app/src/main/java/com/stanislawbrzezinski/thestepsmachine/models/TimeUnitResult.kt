package com.stanislawbrzezinski.thestepsmachine.models

/**
 * Created by Stanisław Brzeziński on 10/12/2018.
 * Copyrights Stanisław Brzeziński
 */
data class TimeUnitResult(
    val currentValue: Int,
    val target: Int
)