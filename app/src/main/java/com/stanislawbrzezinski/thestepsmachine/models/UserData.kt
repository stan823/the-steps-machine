package com.stanislawbrzezinski.thestepsmachine.models

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
data class UserData(
    val userName: String? = null,
    val email: String? = null,
    val uuid: String? = null
)