package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-10-14.
 * Copyrights Stanisław Brzeziński
 */
data class UnfollowUserRequest(
        val guid:String
)