package com.stanislawbrzezinski.thestepsmachine.models.retrofit

import androidx.recyclerview.widget.DiffUtil
import com.google.gson.annotations.SerializedName
import com.stanislawbrzezinski.thestepsmachine.enums.ConnectionStatus

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
data class ConnectionSearchResultModel(
    val name: String? = null,
    val guid: String? = null,
    @SerializedName("status")
    var _status: String? = null,
    var hidden: Boolean = false
) {

    companion object {
        val CALLBACK = object : DiffUtil.ItemCallback<ConnectionSearchResultModel>() {
            override fun areItemsTheSame(oldItem: ConnectionSearchResultModel, newItem: ConnectionSearchResultModel): Boolean {
                return oldItem.guid == newItem.guid
            }

            override fun areContentsTheSame(oldItem: ConnectionSearchResultModel, newItem: ConnectionSearchResultModel): Boolean {
                return oldItem.guid == newItem.guid && oldItem.name == newItem.name
            }
        }
    }
}

val ConnectionSearchResultModel.status: ConnectionStatus
    get() = ConnectionStatus.getByDescription(_status)
