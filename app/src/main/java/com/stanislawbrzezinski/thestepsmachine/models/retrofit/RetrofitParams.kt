package com.stanislawbrzezinski.thestepsmachine.models.retrofit

/**
 * Created by Stanisław Brzeziński on 2019-07-29.
 * Copyrights Stanisław Brzeziński
 */
object RetrofitParams {

    private const val NAME = "name"
    private const val GUID = "guid"
    private const val TOKEN = "token"

    object AccountModel {
        const val NAME = RetrofitParams.NAME
        const val GUID = RetrofitParams.GUID
        const val TOKEN = RetrofitParams.TOKEN
    }
}