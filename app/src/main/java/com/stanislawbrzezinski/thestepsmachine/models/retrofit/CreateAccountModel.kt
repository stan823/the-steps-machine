package com.stanislawbrzezinski.thestepsmachine.models.retrofit

import com.google.gson.annotations.SerializedName

/**
 * Created by Stanisław Brzeziński on 2019-07-23.
 * Copyrights Stanisław Brzeziński
 */
data class CreateAccountModel(
    @SerializedName("uId") val userId: String,
    @SerializedName("uName") val userName: String
)