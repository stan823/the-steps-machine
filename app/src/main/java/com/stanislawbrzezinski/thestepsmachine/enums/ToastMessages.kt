package com.stanislawbrzezinski.thestepsmachine.enums

import androidx.annotation.StringRes
import com.stanislawbrzezinski.thestepsmachine.R

/**
 * Created by Stanisław Brzeziński on 2019-08-06.
 * Copyrights Stanisław Brzeziński
 */
enum class ToastMessages(@StringRes val strResource: Int) {
    NETWORK_PROBLEM(R.string.toasts_network_error)
}