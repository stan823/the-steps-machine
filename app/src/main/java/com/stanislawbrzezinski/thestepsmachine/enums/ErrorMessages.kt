package com.stanislawbrzezinski.thestepsmachine.enums

import androidx.annotation.StringRes
import com.stanislawbrzezinski.thestepsmachine.R

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
enum class ErrorMessages(@StringRes val messageRes: Int) {
    NOT_AUTHORISED(R.string.error_not_authorised),
    PASSWORD_TOO_WEAK(R.string.error_password_too_week),
    USER_ALREADY_EXISTS(R.string.error_user_already_exists),
    INVALID_PASSWORD(R.string.error_invalid_password),
    NAME_IN_USE(R.string.error_name_in_use),
    SOMETHING_WENT_WRONG(R.string.error_something_went_wrong),
    NAME_TOO_SHORT(R.string.errro_name_too_short),
    USER_NOT_FOUND(R.string.error_user_not_found);
}