package com.stanislawbrzezinski.thestepsmachine.enums

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
enum class AppFlavour(private val flavourName: String) {

    PRODUCTION("prod"),
    MOCK("mock"),
    STAGING("stage");

    companion object {
        fun getFlavourByName(name: String): AppFlavour {
            return values().find {
                it.flavourName == name
            } ?: PRODUCTION
        }
    }
}