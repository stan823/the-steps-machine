package com.stanislawbrzezinski.thestepsmachine.enums

/**
 * Created by Stanisław Brzeziński on 2019-08-29.
 * Copyrights Stanisław Brzeziński
 */
enum class ConnectionStatus(val id: Int, val description: String) {

    NONE(1000, "none"),
    PENDING(1, "pending"),
    ACCEPTED(2, "accepted"),
    BLOCKED(3, "blocked");

    companion object {
        fun getById(id: Int) = values().find { it.id == id } ?: NONE

        fun getByDescription(description: String?): ConnectionStatus {
            return values()?.find { description == it.description } ?: NONE
        }
    }
}