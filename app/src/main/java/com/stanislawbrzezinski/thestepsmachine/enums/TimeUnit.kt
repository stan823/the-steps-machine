package com.stanislawbrzezinski.thestepsmachine.enums

/**
 * Created by Stanisław Brzeziński on 10/12/2018.
 * Copyrights Stanisław Brzeziński
 */
enum class TimeUnit(val index: Int) {

    DAY(0),
    WEEK(1),
    MONTH(2);

    companion object {

        fun getByIndex(index: Int): TimeUnit {
            return values().find {
                it.index == index
            } ?: DAY
        }
    }
}