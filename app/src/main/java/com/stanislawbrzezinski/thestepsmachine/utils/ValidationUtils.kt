package com.stanislawbrzezinski.thestepsmachine.utils

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
interface ValidationUtils {
    fun isValidEmail(email: String): Boolean
}