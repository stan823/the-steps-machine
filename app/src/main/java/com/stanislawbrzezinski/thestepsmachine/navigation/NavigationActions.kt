package com.stanislawbrzezinski.thestepsmachine.navigation

import androidx.annotation.IdRes

/**
 * Created by Stanisław Brzeziński on 01/05/2019.
 * Copyrights Stanisław Brzeziński
 */

sealed class NavigationAction
    data class Push(@IdRes val action: Int) : NavigationAction()
    class Pop : NavigationAction()