package com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.models.TargetsModel
import com.stanislawbrzezinski.thestepsmachine.room.daos.TargetsDao

/**
 * Created by Stanisław Brzeziński on 2019-05-09.
 * Copyrights Stanisław Brzeziński
 */
class TargetRoomLoader(
    private val targetsDao: TargetsDao
) : TargetLocalLoader {

    val targesLivedata = Transformations.map(targetsDao.getTargetsLiveData()) {
        if (it != null) {
            TargetsModel(it.daily, it.weekly, it.monthly)
        } else {
            TargetsModel()
        }
    }

    override fun loadTargetsLiveData(): LiveData<TargetsModel> {
        return targesLivedata
    }
}
