package com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver

/**
 * Created by Stanisław Brzeziński on 2019-05-08.
 * Copyrights Stanisław Brzeziński
 */
interface TargetLocalSaver {

    fun saveTargets(
        daily: Int,
        weekly: Int,
        monthly: Int
    )
}