package com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.room.daos.TargetsDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.TargetsEntity
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

/**
 * Created by Stanisław Brzeziński on 2019-05-08.
 * Copyrights Stanisław Brzeziński
 */
class TargetRoomSaver(
    private val targetsDao: TargetsDao,
    private val systemTimeProvider: SystemTimeProvider
) : TargetLocalSaver {

    override fun saveTargets(daily: Int, weekly: Int, monthly: Int) {
        val currentTime = systemTimeProvider.getCurrentTimeInMilliseconds()

        val target = TargetsEntity(
                daily = daily,
                weekly = weekly,
                monthly = monthly,
                created = currentTime
        )
        GlobalScope.launch {
            targetsDao.insert(target)
        }
    }
}