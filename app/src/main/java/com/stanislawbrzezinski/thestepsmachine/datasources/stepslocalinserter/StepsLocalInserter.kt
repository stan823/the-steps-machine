package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
interface StepsLocalInserter {

    fun insertStep()
    fun clearAll()
}