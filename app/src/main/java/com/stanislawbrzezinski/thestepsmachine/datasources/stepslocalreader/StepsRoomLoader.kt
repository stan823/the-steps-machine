package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader

import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.room.daos.DaysDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.MonthsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.WeeksDao

/**
 * Created by Stanisław Brzeziński on 04/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsRoomLoader(
    private val stepsDao: StepsDao,
    private val daysDao: DaysDao,
    private val weeksDao: WeeksDao,
    private val monthsDao: MonthsDao
) : StepsLocalLoader {

    override fun getStepsLiveDataForPeriod(fromTimeInMilliseconds: Long): LiveData<Int> {
        return stepsDao.getStepsLiveData(fromTimeInMilliseconds)
    }

    override fun getStepsByDay(): LiveData<List<Pair<Long, Int>>> {
        return Transformations.map(daysDao.getStepsByDay()) {
            it.map { Pair(it.timestampOfMidnight, it.count) }
        }
    }

    override fun getStepsByWeek(): LiveData<List<Pair<Long, Int>>> {
        return Transformations.map(weeksDao.getStepsByWeek()) {
            it.map { Pair(it.timestampOfMonday, it.count) }
        }
    }

    override fun getStepsByMonth(): LiveData<List<Pair<Long, Int>>> {
        return Transformations.map(monthsDao.getStepsByMonth()) {
            it.map { Pair(it.timestampOfFirstOfMonth, it.count) }
        }
    }
}