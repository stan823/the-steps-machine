package com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader

import androidx.lifecycle.LiveData
import com.stanislawbrzezinski.thestepsmachine.models.TargetsModel

/**
 * Created by Stanisław Brzeziński on 25/02/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TargetLocalLoader {

    fun loadTargetsLiveData(): LiveData<TargetsModel>
}