package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.extensions.resetTimestampDay
import com.stanislawbrzezinski.thestepsmachine.extensions.resetTimestampMonth
import com.stanislawbrzezinski.thestepsmachine.extensions.resetTimestampWeek
import com.stanislawbrzezinski.thestepsmachine.room.daos.DaysDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.MonthsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.WeeksDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.DaysEntity
import com.stanislawbrzezinski.thestepsmachine.room.entities.MonthsEntity
import com.stanislawbrzezinski.thestepsmachine.room.entities.StepEntity
import com.stanislawbrzezinski.thestepsmachine.room.entities.WeeksEntity

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsRoomInserter(
    private val stepsDao: StepsDao,
    private val daysDao: DaysDao,
    private val weeksDao: WeeksDao,
    private val monthsDao: MonthsDao,
    private val systemTimeProvider: SystemTimeProvider
) : StepsLocalInserter {

    override fun clearAll() {
        stepsDao.deleteAll()
        weeksDao.deleteAll()
        daysDao.deleteAll()
        monthsDao.deleteAll()
    }


    override fun insertStep() {
        val created = systemTimeProvider.getCurrentTimeInMilliseconds()
        stepsDao.insert(StepEntity(created = created))
        upsertDay(created.resetTimestampDay())
        upsertWeek(created.resetTimestampWeek())
        upsertMonth(created.resetTimestampMonth())
    }

    fun upsertDay(timestamp: Long) {
        val id = daysDao.insert(DaysEntity(timestampOfMidnight = timestamp, count = 1))
        if (id == -1L) {
            daysDao.addStepToDay(timestamp)
        }
    }

    fun upsertWeek(timestamp: Long) {
        val id = weeksDao.insert(WeeksEntity(timestampOfMonday = timestamp, count = 1))
        if (id == -1L) {
            weeksDao.addStepToWeek(timestamp)
        }
    }

    fun upsertMonth(timestamp: Long) {
        val id = monthsDao.insert(MonthsEntity(timestampOfFirstOfMonth = timestamp, count = 1))
        if (id == -1L) {
            monthsDao.addStepToMonth(timestamp)
        }
    }
}