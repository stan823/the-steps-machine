package com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader

import androidx.lifecycle.LiveData

/**
 * Created by Stanisław Brzeziński on 04/02/2019.
 * Copyrights Stanisław Brzeziński
 */
interface StepsLocalLoader {

    fun getStepsLiveDataForPeriod(fromTimeInMilliseconds: Long): LiveData<Int>
    fun getStepsByDay(): LiveData<List<Pair<Long, Int>>>
    fun getStepsByWeek(): LiveData<List<Pair<Long, Int>>>
    fun getStepsByMonth(): LiveData<List<Pair<Long, Int>>>
}