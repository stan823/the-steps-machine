package com.stanislawbrzezinski.thestepsmachine.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsService

/**
 * Created by Stanisław Brzeziński on 2019-10-15.
 * Copyrights Stanisław Brzeziński
 */
class BootReceiver: BroadcastReceiver(){


    override fun onReceive(context: Context?, intent: Intent?) {

            context?.startForegroundService(Intent(context, StepsService::class.java))

    }
}