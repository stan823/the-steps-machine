package com.stanislawbrzezinski.thestepsmachine

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
object Constants {

    object Targets {
        const val DEFAULT_DAILY_TARGET = 5_000
        const val DEFAULT_WEEKLY_TARGET = 20_000
        const val DEFAULT_MONTHLY_TARGET = 100_000
    }

    object BaseUrl {
        const val BASE_URL = "https://hrx6wl3zf0.execute-api.eu-west-2.amazonaws.com/prod/"
        const val BASE_URL_STAGING = "https://hrx6wl3zf0.execute-api.eu-west-2.amazonaws.com/dev/"
        const val BASE_URL_MOCK = "http://10.0.2.2:5000/"
        const val TERMS_AND_CONDITIONS_URL = "https://the-steps-machine.s3.eu-west-2.amazonaws.com/privacy_policy.html"
    }

    object Connections {
        const val SEARCH_PAGE_SIZE = 30
        const val MIN_SEARCH_WORD_SIZE = 3
    }
}