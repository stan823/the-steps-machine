package com.stanislawbrzezinski.thestepsmachine

import java.io.IOException

/**
 * Created by Stanisław Brzeziński on 2019-08-06.
 * Copyrights Stanisław Brzeziński
 */
class NetworkUnavailableException : IOException()
