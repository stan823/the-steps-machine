package com.stanislawbrzezinski.thestepsmachine

import android.app.Application
import android.content.ComponentName
import android.content.Context
import android.content.pm.PackageManager
import com.stanislawbrzezinski.thestepsmachine.dagger.app.*
import com.stanislawbrzezinski.thestepsmachine.receivers.BootReceiver

/**
 * Created by Stanisław Brzeziński on 17/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class TheStepsMachineApp : Application() {


    companion object {
        var context: Context? = null
        lateinit var appComponent: AppComponent
    }


    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        TheStepsMachineApp.context = base
        val systemServiceModule = AppSystemServiceModule(baseContext)
        appComponent = DaggerAppComponent
                .builder()
                .appContextModule(AppContextModule(baseContext))
                .appSystemServiceModule(systemServiceModule)
                .build()
        base?.let {context->
            val receiver = ComponentName(context, BootReceiver::class.java)

            context.packageManager.setComponentEnabledSetting(
                    receiver,
                    PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
                    PackageManager.DONT_KILL_APP
            )
        }
    }
}