package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel

/**
 * Created by Stanisław Brzeziński on 2019-07-29.
 * Copyrights Stanisław Brzeziński
 */
interface AccountLiveDataInterface : LiveDataInterface<AccountModel>

class AccountLiveData : MutableLiveData<AccountModel>(), AccountLiveDataInterface