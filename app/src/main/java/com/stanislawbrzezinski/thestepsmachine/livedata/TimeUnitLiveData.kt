package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit

/**
 * Created by Stanisław Brzeziński on 10/12/2018.
 * Copyrights Stanisław Brzeziński
 */

class TimeUnitLiveData : MutableLiveData<TimeUnit>() {
    init {
        value = TimeUnit.DAY
    }
}