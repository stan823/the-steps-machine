package com.stanislawbrzezinski.thestepsmachine.livedata

/**
 * Created by Stanisław Brzeziński on 2019-09-18.
 * Copyrights Stanisław Brzeziński
 */
data class RemoteConfigModel(
    val stepsUploadInterval: Int
)