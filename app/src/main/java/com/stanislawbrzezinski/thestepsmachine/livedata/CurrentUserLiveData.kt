package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.models.UserData

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
class CurrentUserLiveData : MutableLiveData<UserData>()