package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.annotation.IdRes
import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.navigation.Push

/**
 * Created by Stanisław Brzeziński on 02/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface MainNavigationLiveDataInterface : LiveDataInterface<NavigationAction> {

    fun push(@IdRes actionRes: Int)
    fun pop()
}
class MainNavigationLiveData : MutableLiveData<NavigationAction>(), MainNavigationLiveDataInterface {

    override fun pop() {
        postValue(Pop())
    }

    override fun push(actionRes: Int) {
        postValue(Push(actionRes))
    }
}
