package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.models.TargetsModel

/**
 * Created by Stanisław Brzeziński on 25/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetLiveData : MutableLiveData<TargetsModel>() {
    init {
        value = TargetsModel()
    }
}