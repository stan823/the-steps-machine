package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-10-14.
 * Copyrights Stanisław Brzeziński
 */

interface LoadingScreenLiveDataInterface : LiveDataInterface<Boolean>
class LoadingScreenLiveData :MutableLiveData<Boolean>(), LoadingScreenLiveDataInterface{
    init {
        value = false
    }
}