package com.stanislawbrzezinski.thestepsmachine.livedata

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.enums.ToastMessages

/**
 * Created by Stanisław Brzeziński on 2019-08-06.
 * Copyrights Stanisław Brzeziński
 */
interface ToastsLiveDataInterface : LiveDataInterface<ToastMessages>

class ToastsLiveData : MutableLiveData<ToastMessages>(), ToastsLiveDataInterface