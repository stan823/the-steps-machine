package com.stanislawbrzezinski.thestepsmachine.retrofit

import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ResultResponse
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.StepsUploadRequest
import retrofit2.http.Body
import retrofit2.http.POST

/**
 * Created by Stanisław Brzeziński on 2019-09-17.
 * Copyrights Stanisław Brzeziński
 */
interface StepsRetrofitService {

    @POST("steps/upload/v1")
    fun uploadSteps(@Body body: StepsUploadRequest): ResultResponse
}