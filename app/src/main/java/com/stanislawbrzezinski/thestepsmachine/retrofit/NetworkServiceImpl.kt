package com.stanislawbrzezinski.thestepsmachine.retrofit

import com.stanislawbrzezinski.thestepsmachine.models.retrofit.*
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
class NetworkServiceImpl(
    private val connectionsRetrofitService: ConnectionsRetrofitService,
    private val accountRetrofitService: AccountRetrofitService
) : NetworkService {

    override val getConnections: Single<List<LeaderboardEntryModel>>
        get() = connectionsRetrofitService.get()
                .onNetworkThread()

    override fun changeName(newName: String): Single<ResultResponse> {
        val request = ChangeNameRequest(name = newName)
        return accountRetrofitService.changeName(request)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }

    override fun deleteAccount(): Single<ResultResponse> {
        return accountRetrofitService.deleteAccount()
                .subscribeOn(Schedulers.io())
                .subscribeOn(Schedulers.io())
    }

    override fun follow(guid: String): Single<ResultResponse> {
        return connectionsRetrofitService.follow(ConnectionFollowRequest(guid))
                .onNetworkThread()
    }

    override fun unfollow(guid: String): Single<ResultResponse> {
        return connectionsRetrofitService.unfollowUser(UnfollowUserRequest(guid))
                .onNetworkThread()
    }
}

private fun <T> Single<T>.onNetworkThread(): Single<T> {
    return this.subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}