package com.stanislawbrzezinski.thestepsmachine.retrofit

import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionFollowRequest
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.LeaderboardEntryModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ResultResponse
import io.reactivex.Single
import retrofit2.http.Body

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
interface NetworkService {

    val getConnections: Single<List<LeaderboardEntryModel>>

    fun changeName(newName: String): Single<ResultResponse>

    fun deleteAccount(): Single<ResultResponse>

    fun follow(guid:String): Single<ResultResponse>

    fun unfollow(guid: String): Single<ResultResponse>
}