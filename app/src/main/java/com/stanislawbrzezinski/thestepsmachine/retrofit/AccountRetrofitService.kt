package com.stanislawbrzezinski.thestepsmachine.retrofit

import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ChangeNameRequest
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.CreateAccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ResultResponse
import io.reactivex.Single
import retrofit2.http.*

/**
 * Created by Stanisław Brzeziński on 2019-07-19.
 * Copyrights Stanisław Brzeziński
 */
interface AccountRetrofitService {

    @PUT("user/")
    fun createAccount(@Body model: CreateAccountModel): Single<AccountModel>

    @GET("user/")
    fun login(@Query("uid") uid: String): Single<AccountModel>

    @PATCH("user/name/")
    fun changeName(@Body request: ChangeNameRequest): Single<ResultResponse>

    @DELETE("user/")
    fun deleteAccount(): Single<ResultResponse>
}