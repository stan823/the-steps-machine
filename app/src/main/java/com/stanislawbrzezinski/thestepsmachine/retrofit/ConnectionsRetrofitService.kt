package com.stanislawbrzezinski.thestepsmachine.retrofit

import com.stanislawbrzezinski.thestepsmachine.Constants
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.*
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
interface ConnectionsRetrofitService {

    @GET("connection/find/")
    suspend fun find(
            @Query(value = "name") name: String,
            @Query(value = "pageSize") pageSize: Int = Constants.Connections.SEARCH_PAGE_SIZE,
            @Query(value = "pageNumber") pageNumber: Int
    ): List<ConnectionSearchResultModel>

    @POST("connection/follow/")
    fun follow(
            @Body body: ConnectionFollowRequest
    ): Single<ResultResponse>

    @GET("connection/v1/")
    fun get(): Single<List<LeaderboardEntryModel>>

    @POST("connection/unfollow/v1/")
    fun unfollowUser(@Body body: UnfollowUserRequest): Single<ResultResponse>
}