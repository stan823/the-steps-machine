package com.stanislawbrzezinski.thestepsmachine.firebase

/**
 * Created by Stanisław Brzeziński on 2019-09-18.
 * Copyrights Stanisław Brzeziński
 */
interface FirebaseRemoteConfigManager {
    fun fetchSettings()
    val stepsUploadInterval: Int
}