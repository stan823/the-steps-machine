package com.stanislawbrzezinski.thestepsmachine.firebase

import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings
import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.R

/**
 * Created by Stanisław Brzeziński on 2019-09-18.
 * Copyrights Stanisław Brzeziński
 */
class FirebaseRemoteConfigManagerImpl : FirebaseRemoteConfigManager {

    private val remoteConfig: FirebaseRemoteConfig by lazy {
        val config = FirebaseRemoteConfig.getInstance()
        val configSettings = FirebaseRemoteConfigSettings.Builder()
                .setMinimumFetchIntervalInSeconds(3600)
                .setDeveloperModeEnabled(BuildConfig.DEBUG)
                .build()
        config.setConfigSettingsAsync(configSettings)
        config.setDefaultsAsync(R.xml.remote_config_default)
        config
    }

    override fun fetchSettings() {
        remoteConfig.fetchAndActivate()
    }

    override val stepsUploadInterval: Int
        get() = remoteConfig.getLong("steps_upload_interval").toInt()
}