package com.stanislawbrzezinski.thestepsmachine.firebase

import io.reactivex.Single

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
interface FirebaseAuthManager {

    fun registerWithEmail(email: String, password: String): Single<String>
    fun logOut()
    fun loginWithEmail(email: String, password: String): Single<String>
    fun deleteUser(): Single<Boolean>
}