package com.stanislawbrzezinski.thestepsmachine.stepsservice

import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor.TYPE_STEP_DETECTOR
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.util.Log
import androidx.core.app.NotificationCompat
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.stepsservice.dagger.stepsServiceComponent
import javax.inject.Inject
import android.app.PendingIntent
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import android.app.NotificationManager
import android.app.NotificationChannel

/**
 * Created by Stanisław Brzeziński on 2019-09-25.
 * Copyrights Stanisław Brzeziński
 */
class StepsService : Service(), SensorEventListener {

    companion object {
        const val CHANELL_ID = "1234"
    }
    override fun onBind(intent: Intent?) = null

    @Inject
    lateinit var interactor: StepsServiceInteractor

    lateinit var sensorManager: SensorManager

    override fun onStart(intent: Intent?, startId: Int) {
        super.onStart(intent, startId)
        Log.d("sser2","starting in start command")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.d("sser2","starting1")
        stepsServiceComponent.inject(this)
        Log.d("sser2","starting2")
        val notificationTitle = getString(R.string.app_name)
        val notificationChanell = getString(R.string.notification_chanell_name)
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        val sensor = sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR)
        sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL)

        val notificationIntent = Intent(this, MainActivity::class.java)
        val notificationChannel = NotificationChannel(CHANELL_ID, notificationChanell, NotificationManager.IMPORTANCE_DEFAULT)
        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        notificationManager.createNotificationChannel(notificationChannel)
        val pendingIntent = PendingIntent.getActivity(this, 0,
                notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)
        val notification = NotificationCompat.Builder(this, CHANELL_ID)
                .setSmallIcon(R.drawable.ic_notification_small_con)
                .setContentText(notificationTitle)
                .setContentIntent(pendingIntent)
                .build()
        startForeground(123, notification)
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        try {
            sensorManager.unregisterListener(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
    }

    override fun onSensorChanged(event: SensorEvent?) {
        val sensor = event?.sensor
        Log.d("sensr", "step")
        if (sensor != null && sensor.type == TYPE_STEP_DETECTOR) {
            interactor.recordStep()
        }
    }
}