package com.stanislawbrzezinski.thestepsmachine.stepsservice.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.retrofit.StepsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsServiceInteractor
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsServiceInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsServiceRepository
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsServiceRepositoryImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class StepsServiceModule {

    @Provides
    @StepsServiceScope
    fun provideRepository(
        stepsLocalInserter: StepsLocalInserter,
        stepsLocalLoader: StepsLocalLoader,
        systemTimeProvider: SystemTimeProvider,
        stepsRetrofitService: StepsRetrofitService,
        firebase: FirebaseRemoteConfigManager
    ): StepsServiceRepository {
        return StepsServiceRepositoryImpl(
                stepsLocalInserter,
                stepsLocalLoader,
                systemTimeProvider,
                stepsRetrofitService,
                firebase
        )
    }

    @Provides
    @StepsServiceScope
    fun provideInteractor(
        repository: StepsServiceRepository
    ): StepsServiceInteractor {
        return StepsServiceInteractorImpl(repository)
    }
}