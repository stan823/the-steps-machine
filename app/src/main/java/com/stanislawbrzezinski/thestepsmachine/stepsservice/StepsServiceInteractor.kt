package com.stanislawbrzezinski.thestepsmachine.stepsservice

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
interface StepsServiceInteractor {
    fun recordStep()
}