package com.stanislawbrzezinski.thestepsmachine.stepsservice

/**
 * Created by Stanisław Brzeziński on 28/01/2019.
 * Copyrights Stanisław Brzeziński
 */
interface StepsServiceRepository {
    fun recordStep()
}