package com.stanislawbrzezinski.thestepsmachine.stepsservice.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.stepsservice.FakeSystemServiceButton
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsService
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        modules = [StepsServiceModule::class],
        dependencies = [AppComponent::class]
)
@StepsServiceScope
interface StepsServiceComponent {

    fun inject(fakeButton: FakeSystemServiceButton)
    fun inject(stepService: StepsService)
}

val stepsServiceComponent: StepsServiceComponent by lazy {
    DaggerStepsServiceComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}