package com.stanislawbrzezinski.thestepsmachine.stepsservice

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsServiceInteractorImpl(
    private val repo: StepsServiceRepository
) : StepsServiceInteractor {

    override fun recordStep() {
        repo.recordStep()
    }
}