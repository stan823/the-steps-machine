package com.stanislawbrzezinski.thestepsmachine.stepsservice

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import com.stanislawbrzezinski.thestepsmachine.stepsservice.dagger.stepsServiceComponent
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 04/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class FakeSystemServiceButton : AppCompatButton {

    @Inject
    lateinit var interactor: StepsServiceInteractor

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        if (!isInEditMode) {
            stepsServiceComponent.inject(this)

            setOnClickListener {
                interactor.recordStep()
            }
        }
    }
}