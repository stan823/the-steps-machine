package com.stanislawbrzezinski.thestepsmachine.stepsservice

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.StepsUploadRequest
import com.stanislawbrzezinski.thestepsmachine.retrofit.StepsRetrofitService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

/**
 * Created by Stanisław Brzeziński on 31/01/2019.
 * Copyrights Stanisław Brzeziński
 */
class StepsServiceRepositoryImpl(
    private val stepsLocalInserter: StepsLocalInserter,
    stepsLocalLoader: StepsLocalLoader,
    systemTimeProvider: SystemTimeProvider,
    stepsRetrofitService: StepsRetrofitService,
    firebaseRemoteConfigManager: FirebaseRemoteConfigManager
) : StepsServiceRepository {

    init {
        val calendar = Calendar.getInstance()
        val montStart = systemTimeProvider.getTimeOfFirstDayOfCurrentMonth(calendar)
        stepsLocalLoader.getStepsLiveDataForPeriod(montStart).observeForever {
            if (it % firebaseRemoteConfigManager.stepsUploadInterval == 0) {
                    try {
                        stepsRetrofitService.uploadSteps(StepsUploadRequest(steps = it))
                    } catch (e: Exception) {
                        // no action required, we ignore lack of network
                    }
            }
        }
    }

    override fun recordStep() {
        GlobalScope.launch {
            stepsLocalInserter.insertStep()
        }
    }
}