package com.stanislawbrzezinski.thestepsmachine.services.errorservice

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
enum class NetworkErrors(val errorCode: Int) {

    TOKEN_NOT_FOUND(2001),
    INVALID_PASSWORD(2002),
    USER_ALREADY_EXISTS(2101),
    RESOURCES_NOT_FOUND(2102),
    DB_CONNECTION_ERROR(2202),
    S3_CONNECTION_ERROR(2201),
    QUERY_ERROR(2203),
    INVALID_INPUT(2301),
    UNKNOWN(1000);

    companion object {
        fun getByErrorCode(errorCode: Int?): NetworkErrors {
            return values().find { it.errorCode == errorCode } ?: UNKNOWN
        }
    }
}