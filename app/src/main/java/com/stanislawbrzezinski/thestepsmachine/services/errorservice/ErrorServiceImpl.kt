package com.stanislawbrzezinski.thestepsmachine.services.errorservice

import com.google.gson.Gson
import retrofit2.HttpException
import java.lang.Exception

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */

class ErrorServiceImpl(private val gson: Gson) : ErrorService {

    private data class ErrorModel(val error: Int? = null)

    private val supportedHttpCodes = listOf(400, 401, 412, 413, 500)

    override fun parseException(exception: HttpException): NetworkErrors {
        return if (supportedHttpCodes.contains(exception.code())) {
            try {

                val error = gson.fromJson(exception.message(), ErrorModel::class.java).error

                NetworkErrors.getByErrorCode(error)
            } catch (e: Exception) {
                NetworkErrors.UNKNOWN
                try {

                    val error = gson.fromJson(exception.response()?.errorBody()?.string(), ErrorModel::class.java).error

                    NetworkErrors.getByErrorCode(error)
                } catch (e: Exception) {
                    NetworkErrors.UNKNOWN
                }
            }
        } else {
            NetworkErrors.UNKNOWN
        }
    }
}