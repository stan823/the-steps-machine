package com.stanislawbrzezinski.thestepsmachine.services.errorservice

import retrofit2.HttpException

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
interface ErrorService {

    fun parseException(exception: HttpException): NetworkErrors
}