package com.stanislawbrzezinski.thestepsmachine.extensions

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */

/**
 * Converts percentage to degrees, ie. 50% = 180deg
 * Input is clamped to 0..100
 */
fun Float.percentateToDegree(): Float {
    var input = when {
        this > 100 -> 100f
        this < 0 -> 0f
        else -> this
    }

    val factor = input / 100f
    return factor * 360
}

/**
 * Converts degrees to percentage, ie. 180deg = 50%
 */
fun Float.degreeToPercentage(): Float {

    val input = when {
        this < 0 -> 0f
        this > 360 -> 360f
        else -> this
    }

    val factor = input / 360f
    return factor * 100f
}