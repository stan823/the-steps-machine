package com.stanislawbrzezinski.thestepsmachine.extensions

import android.app.AlertDialog
import android.content.Context
import androidx.fragment.app.Fragment
import android.content.Context.INPUT_METHOD_SERVICE
import android.view.inputmethod.InputMethodManager
import androidx.core.content.ContextCompat.getSystemService
import com.stanislawbrzezinski.thestepsmachine.R

/**
 * Created by Stanisław Brzeziński on 2019-10-09.
 * Copyrights Stanisław Brzeziński
 */
fun Fragment.closeKeyboard(){
    val context = this.activity
    if (context!=null) {
        try {
            val imm = context.getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager?
            imm?.hideSoftInputFromWindow(context.currentFocus.windowToken, InputMethodManager.HIDE_NOT_ALWAYS);
        }catch (e:Exception){
            e.printStackTrace()
        }
    }
}

fun Fragment.showUnfollowDialog(
        guid: String,
        name: String,
        callback: (guid: String) -> Unit) {
    AlertDialog.Builder(this.activity)
            .setPositiveButton(android.R.string.ok) { d, _ ->
                callback(guid)
                d.dismiss()
            }
            .setNegativeButton(android.R.string.cancel) { d, _ ->
                d.dismiss()
            }
            .setMessage(this.activity?.getString(R.string.alert_unfollow_user, name)?:"")
            .setTitle(R.string.alert_confirm)
            .show()
}