package com.stanislawbrzezinski.thestepsmachine.extensions

import android.view.View

/**
 * Created by Stanisław Brzeziński on 08/05/2019.
 * Copyrights Stanisław Brzeziński
 */
fun View.isNotEditMode(action: () -> Unit) {
    if (!isInEditMode) {
        action()
    }
}

fun View.onClick(action: () -> Unit) {
    this.setOnClickListener {
        action()
    }
}

fun View.setVisible(isVisible: Boolean) {
    if (isVisible) {
        this.visibility = View.VISIBLE
    } else {
        this.visibility = View.GONE
    }
}