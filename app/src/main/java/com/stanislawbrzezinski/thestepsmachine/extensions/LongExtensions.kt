package com.stanislawbrzezinski.thestepsmachine.extensions

import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-06-11.
 * Copyrights Stanisław Brzeziński
 */
fun Long.resetTimestampMonth(): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    calendar.set(Calendar.DAY_OF_MONTH, 1)
    calendar.set(Calendar.HOUR_OF_DAY, 2)
    calendar.set(Calendar.MINUTE, 2)
    calendar.set(Calendar.SECOND, 2)
    val timeInMils = calendar.timeInMillis
    return (timeInMils / 1000_00) * 100
}

fun Long.resetTimestampWeek(): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY)
    calendar.set(Calendar.HOUR_OF_DAY, 2)
    calendar.set(Calendar.MINUTE, 2)
    calendar.set(Calendar.SECOND, 2)
    val timeInMils = calendar.timeInMillis
    return (timeInMils / 1000_00) * 100
}

fun Long.resetTimestampDay(): Long {
    val calendar = Calendar.getInstance()
    calendar.timeInMillis = this
    calendar.set(Calendar.HOUR_OF_DAY, 2)
    calendar.set(Calendar.MINUTE, 2)
    calendar.set(Calendar.SECOND, 2)
    val timeInMils = calendar.timeInMillis
    return (timeInMils / 1000_00) * 100
}