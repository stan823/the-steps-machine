package com.stanislawbrzezinski.thestepsmachine.extensions

import android.text.Editable
import android.text.TextWatcher
import android.widget.EditText

/**
 * Created by Stanisław Brzeziński on 2019-07-19.
 * Copyrights Stanisław Brzeziński
 */
fun EditText.onTextChange(callback: (text: String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            if (s != null) {
                callback(s.toString())
            }
        }
    })
}

fun EditText.onAction(callback: (text: String) -> Unit) {
    this.setOnEditorActionListener { v, actionId, event ->
        callback.invoke(v?.text?.toString() ?: "")
        true
    }
}