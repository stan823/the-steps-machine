package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor

import androidx.annotation.VisibleForTesting
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.updateData
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.toEntity
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.NetworkErrors
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo.FragmentAccountRepo
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountStateLiveDataInterface
import retrofit2.HttpException

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
class FragmentAccountInteractorImpl(
    private val accountsDao: AccountsDao,
    private val firebaseAuthManager: FirebaseAuthManager,
    private val accountLiveData: AccountLiveDataInterface,
    private val navigationLiveData: MainNavigationLiveDataInterface,
    private val repo: FragmentAccountRepo,
    private val stateLiveData: FragmentAccountStateLiveDataInterface,
    private val errorService: ErrorService,
    private val threadsService: ThreadsService
) : FragmentAccountInteractor {

    override fun onLogOut() {
        threadsService.provideThread(this::loggOutRunner).start()
        accountLiveData.postValue(null)
        navigationLiveData.postValue(Pop())
    }

    @VisibleForTesting
    fun loggOutRunner() {
        val account = accountLiveData.getValue()

        if (account?.toEntity() != null) {
            accountsDao.deleteAccount(account.toEntity())
            repo.flushSteps()
        }
        firebaseAuthManager.logOut()
    }

    override fun onBack() {
        navigationLiveData.postValue(Pop())
    }

    override fun onChange(name: String) {
        showLoadingScreen()
        repo.changeName(name,
                successCallback = this::onChangeNameSuccess,
                errorCallback = this::onChangeNameError
        )
    }

    override fun onTextChanged() {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        if (state.errorMessage != null) {
            val newState = state.copy(errorMessage = null, showSuccessMessage = false, isLoading = false)
            stateLiveData.postValue(newState)
        }
    }

    private fun showLoadingScreen() {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        val newState = state.copy(isLoading = true)
        stateLiveData.postValue(newState)
    }

    private fun hideLoadingScreen() {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        val newState = state.copy(isLoading = false)
        stateLiveData.postValue(newState)
    }

    private fun hideSuccessMessage() {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        val newState = state.copy(showSuccessMessage = false)
        stateLiveData.postValue(newState)
    }

    private fun showError(errorMessage: ErrorMessages) {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        val newState = state.copy(errorMessage = errorMessage, isLoading = false)
        stateLiveData.postValue(newState)
    }

    @VisibleForTesting
    fun onChangeNameError(error: Throwable) {
        hideSuccessMessage()

        when (error) {
            is HttpException -> {
                when (errorService.parseException(error)) {
                    NetworkErrors.TOKEN_NOT_FOUND -> showError(ErrorMessages.NOT_AUTHORISED)
                    NetworkErrors.USER_ALREADY_EXISTS -> showError(ErrorMessages.NAME_IN_USE)
                    else -> showError(ErrorMessages.SOMETHING_WENT_WRONG)
                }
            }

            else -> showError(ErrorMessages.SOMETHING_WENT_WRONG)
        }
    }

    @VisibleForTesting
    fun onChangeNameSuccess() {
        val state = stateLiveData.getValue() ?: FragmentAccountState()
        val newState = state.copy(
                showSuccessMessage = true,
                errorMessage = null,
                isLoading = false
        )
        stateLiveData.postValue(newState)
    }

    override fun deleteAccount() {
        showLoadingScreen()
        repo.deleteAccount(
                successCallback = this::deleteAccountSuccessCallback,
                errorCallback = this::deleteAccountErrorCallback)
    }

    @VisibleForTesting
    fun deleteAccountSuccessCallback() {
        stateLiveData.updateData {
            it.copy(isLoading = false)
        }
        navigationLiveData.postValue(Pop())
    }

    @VisibleForTesting
    fun deleteAccountErrorCallback() {
        stateLiveData.updateData {
            it.copy(
                    errorMessage = ErrorMessages.SOMETHING_WENT_WRONG,
                    isLoading = false
            )
        }
    }
}
