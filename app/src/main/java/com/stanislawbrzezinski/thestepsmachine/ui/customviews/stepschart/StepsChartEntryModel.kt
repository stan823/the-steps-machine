package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart

import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-06-06.
 * Copyrights Stanisław Brzeziński
 */
data class StepsChartEntryModel(
    val dateLabel: String,
    val steps: Int
)