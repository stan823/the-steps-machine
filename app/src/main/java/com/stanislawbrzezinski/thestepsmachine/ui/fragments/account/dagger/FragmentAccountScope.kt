package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.dagger

import javax.inject.Scope

/**
 * Created by Stanisław Brzeziński on 2019-09-30.
 * Copyrights Stanisław Brzeziński
 */
@Scope
annotation class FragmentAccountScope