package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData

/**
 * Created by Stanisław Brzeziński on 22/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class TimeButtonPresenterImpl(
    private val timeUnitLiveData: TimeUnitLiveData
) : TimeButtonPresenter {

    override fun observeTimeUnit(observer: (TimeUnit) -> Unit) {
        timeUnitLiveData.observeForever(observer)
    }

    override fun removeTimeUnitObserver(observer: (TimeUnit) -> Unit) {
        timeUnitLiveData.removeObserver(observer)
    }

    override fun getTimeUnit(): TimeUnit {
        return timeUnitLiveData.value ?: TimeUnit.DAY
    }
}