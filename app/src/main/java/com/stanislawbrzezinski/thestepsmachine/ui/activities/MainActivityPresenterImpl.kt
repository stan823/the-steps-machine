package com.stanislawbrzezinski.thestepsmachine.ui.activities

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveData
import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class MainActivityPresenterImpl(
    private val navigationLiveData: MainNavigationLiveDataInterface,
    private val loadingScreenLiveData: LoadingScreenLiveDataInterface
) : MainActivityPresenter {

    override fun observeNavigation(
        lifecycleOwner: LifecycleOwner,
        observer: Observer<NavigationAction>
    ) {
        navigationLiveData.observe(lifecycleOwner, observer)
    }

    override fun observeLoadingScreen(lifecycleOwner: LifecycleOwner, observer: Observer<Boolean>) {
        loadingScreenLiveData.observe(lifecycleOwner, observer)
    }
}