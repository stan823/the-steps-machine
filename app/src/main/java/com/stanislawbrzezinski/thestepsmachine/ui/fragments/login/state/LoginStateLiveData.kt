package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-08-08.
 * Copyrights Stanisław Brzeziński
 */
interface LoginStateLiveDataInterface : LiveDataInterface<LoginState>

class LoginStateLiveData : MutableLiveData<LoginState>(), LoginStateLiveDataInterface