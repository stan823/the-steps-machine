package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLoginRepo {

    fun login(email: String, password: String)

    fun createUser(email: String, password: String)

    suspend fun logOut()
}