package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor

import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.NetworkUnavailableException
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.updateData
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LocaleService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.livedata.*
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.LeaderboardEntryModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.isEmpty
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.retrofit.StepsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardStateLiveDataInterface
import io.reactivex.disposables.Disposable
import retrofit2.HttpException
import java.net.SocketTimeoutException
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLeaderboardInteractorImpl(
        private val accountLiveData: AccountLiveDataInterface,
        private val networkService: NetworkService,
        private val stateLiveData: FragmentLeaderboardStateLiveDataInterface,
        private val stepsLocalLoader: StepsLocalLoader,
        private val systemTimeProvider: SystemTimeProvider,
        private val localeService: LocaleService,
        private val loadingScreenLiveData: LoadingScreenLiveDataInterface,
        private val toastsService: ToastsService
) : FragmentLeaderboardInteractor {

    private var disposable: Disposable? = null
    private var unfollowDisposable: Disposable? = null

    private var currentSteps: Int = 0

    init {
        val calendar = Calendar.getInstance()
        val montStart = systemTimeProvider.getTimeOfFirstDayOfCurrentMonth(calendar)
        stepsLocalLoader.getStepsLiveDataForPeriod(montStart).observeForever {
            currentSteps = it ?: 0
            updateLeaderboardStepsForMyself()
        }


        accountLiveData.observeForever(Observer {
            if (!it.isEmpty()) {
                fetchLeaderboard()
            } else {
                updateLeaderboard(listOf<LeaderboardEntryModel>())
            }
        })
    }

    override fun unfollowUser(guid: String) {
        loadingScreenLiveData.postValue(true)
        unfollowDisposable = networkService.unfollow(guid).subscribe(
                { fetchLeaderboard() },
                {
                    loadingScreenLiveData.postValue(false)
                    toastsService.somethingWentWrong()
                }
        )
    }

    private fun updateLeaderboardStepsForMyself() {
        val entires = stateLiveData.getValue()?.leaderboardEntries?.toMutableList()
        val myOldEntry = entires?.find { it.guid == accountLiveData.getValue()?.guid }
        if (myOldEntry != null) {
            entires.remove(myOldEntry)
            entires.add(myOldEntry.copy(steps = currentSteps))
            entires.sortByDescending { it.steps }
            stateLiveData.updateData {
                it.copy(
                        leaderboardEntries = entires,
                        showLoadingSpinner = false,
                        showPullToRefreshSpinner = false
                )
            }
        }

    }

    override fun fetchLeaderboard() {

        hideMessages()

        disposable = networkService.getConnections
                .subscribe({ leaderboard ->
                    if (loadingScreenLiveData.getValue() == true){
                        loadingScreenLiveData.postValue(false)
                    }
                    updateLeaderboard(leaderboard)
                }, { e ->
                    if (loadingScreenLiveData.getValue() == true){
                        loadingScreenLiveData.postValue(false)
                    }
                    when (e) {
                        is NetworkUnavailableException,
                        is SocketTimeoutException -> stateLiveData.updateData {
                            it.copy(
                                    showNoInternetMessage = true,
                                    showPullToRefreshSpinner = false
                            )
                        }
                        is HttpException -> stateLiveData.updateData {
                            it.copy(
                                    showSomethingWentWrongMessage = true,
                                    showPullToRefreshSpinner = false
                            )
                        }
                    }
                })
    }

    private fun hideMessages() {
        stateLiveData.updateData {
            it.copy(
                    showNoInternetMessage = false,
                    showSomethingWentWrongMessage = false,
                    showConnectionsMessage = false
            )
        }
    }

    private fun updateLeaderboard(learderboard: List<LeaderboardEntryModel>) {
        val tempLeaderboard = learderboard.toMutableList()
        val account = accountLiveData.getValue()
        if (account?.guid != null && account.name != null && !account.isEmpty()) {
            tempLeaderboard.add(LeaderboardEntryModel(account.guid, account.name, currentSteps))
            tempLeaderboard.sortByDescending { it.steps }
        }

        stateLiveData.updateData {
            it.copy(
                    leaderboardEntries = tempLeaderboard,
                    showLoadingSpinner = false,
                    showPullToRefreshSpinner = false
            )
        }
    }

    override fun updateDate() {
        val currTimestamp = systemTimeProvider.getCurrentTimeInMilliseconds()
        val dateFormatter = SimpleDateFormat("MMMM yyyy", localeService.locale)
        val date = dateFormatter.format(Date(currTimestamp))
        stateLiveData.updateData {
            it.copy(currentMonth = date)
        }
    }
}