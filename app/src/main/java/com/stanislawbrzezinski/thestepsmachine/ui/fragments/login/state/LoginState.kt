package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

/**
 * Created by Stanisław Brzeziński on 2019-08-08.
 * Copyrights Stanisław Brzeziński
 */
enum class LoginState {
    SUCCESS,
    USER_NOT_FOUND,
    INVALID_PASSWORD,
    NETWORK_ERROR,
    UNKNOWN_ERROR
}