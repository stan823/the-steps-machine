package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.interactor

import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver.TargetLocalSaver
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData

/**
 * Created by Stanisław Brzeziński on 2019-05-08.
 * Copyrights Stanisław Brzeziński
 */
class FragmentTargetsInteractorImpl(
    private val navigationLiveData: MainNavigationLiveDataInterface,
    private val targetsSaver: TargetLocalSaver,
    private val loader: TargetLocalLoader,
    private val dailyTargetLiveData: CurrentDailyTargetLiveData,
    private val weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
    private val monthlyTargetLiveData: CurrentMonthlyTargetLiveData
) : FragmentTargetsInteractor {

    init {
        loader.loadTargetsLiveData().observeForever {
            dailyTargetLiveData.postValue(it.daily)
            weeklyTargetLiveData.postValue(it.weekly)
            monthlyTargetLiveData.postValue(it.monthly)
        }
    }

    override fun onConfirmClicked() {
        targetsSaver.saveTargets(
                daily = dailyTargetLiveData.value ?: 0,
                weekly = weeklyTargetLiveData.value ?: 0,
                monthly = monthlyTargetLiveData.value ?: 0
        )
        navigationLiveData.postValue(Pop())
    }

    override fun onBackClicked() {
        navigationLiveData.postValue(Pop())
    }
}