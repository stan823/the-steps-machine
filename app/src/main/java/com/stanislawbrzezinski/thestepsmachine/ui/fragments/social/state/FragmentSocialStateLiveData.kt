package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-08-28.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentSocialStateLiveDataInterface : LiveDataInterface<FragmentSocialState>

class FragmentSocialStateLiveData : MutableLiveData<FragmentSocialState>(), FragmentSocialStateLiveDataInterface {
    init {
        value = FragmentSocialState()
    }
}