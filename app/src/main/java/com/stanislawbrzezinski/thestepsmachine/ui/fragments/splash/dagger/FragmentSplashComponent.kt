package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.FragmentSplash
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@FragmentSplashScope
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentSplashModule::class])
interface FragmentSplashComponent {
    fun inject(fragment: FragmentSplash)
}

val fragmentSplashComponent: FragmentSplashComponent by lazy {
    DaggerFragmentSplashComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}