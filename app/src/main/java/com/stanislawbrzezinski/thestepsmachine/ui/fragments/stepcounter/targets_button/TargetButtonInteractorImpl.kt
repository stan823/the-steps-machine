package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button

import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.Push

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetButtonInteractorImpl(
    private val navigationLiveData: MainNavigationLiveDataInterface
) : TargetButtonInteractor {

    override fun onClick() {
        navigationLiveData.postValue(Push(R.id.action_fragmentMain_to_fragmentTargets))
    }
}