package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages

/**
 * Created by Stanisław Brzeziński on 2019-07-31.
 * Copyrights Stanisław Brzeziński
 */
data class FragmentLoginState(
    val isLoading: Boolean = false,
    val isLoginEnabled: Boolean = false,
    val isRegistrationEnabled: Boolean = false,
    val errorMessage: ErrorMessages? = null
)