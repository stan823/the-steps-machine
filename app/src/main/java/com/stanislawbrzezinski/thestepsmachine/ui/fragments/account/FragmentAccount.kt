package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.extensions.*
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.dagger.fragmentAccountComponent
import com.stanislawbrzezinski.thestepsmachine.ui.alerts.showDeleteAccountDialog
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor.FragmentAccountInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.presenter.FragmentAccountPresenter
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.fragment_account.view.backButton
import kotlinx.android.synthetic.main.fragment_account.view.versionHolder
import kotlinx.android.synthetic.main.fragment_login.view.*
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
class FragmentAccount : Fragment() {

    @Inject
    lateinit var interactor: FragmentAccountInteractor

    @Inject
    lateinit var presenter: FragmentAccountPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_account, null, false)
        fragmentAccountComponent.inject(this)

        view?.nameHolder?.onTextChange {
            interactor.onTextChanged()
        }
        view?.logoutButton?.onClick(interactor::onLogOut)
        view?.backButton?.onClick(interactor::onBack)
        view?.nameHolder?.onAction{
            interactor.onChange(it)
            closeKeyboard()
        }
        view?.deleteButton?.onClick {
            activity?.showDeleteAccountDialog(interactor::deleteAccount)
        }
        view?.versionHolder?.text = activity?.getString(
                R.string.version,
                BuildConfig.VERSION_NAME
        ) ?: ""
        presenter.observeName(this, Observer {
            view?.nameHolder?.setText(it)
            view?.nameHolder?.setSelection(it.length)
        })

        presenter.observeState(this, Observer {
            view?.spinner?.setVisible(it.isLoading)
            view?.successHolder?.setVisible(it.showSuccessMessage)
            if (it.errorMessage != null) {
                view?.errorHolder?.setText(it.errorMessage.messageRes)
            } else {
                view?.errorHolder?.text = ""
            }
        })

        return view
    }
}