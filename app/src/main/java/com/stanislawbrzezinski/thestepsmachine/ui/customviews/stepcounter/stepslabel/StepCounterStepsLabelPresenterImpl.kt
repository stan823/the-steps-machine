package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-05-20.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterStepsLabelPresenterImpl(
    private val stepsLoader: StepsLocalLoader,
    private val timeUnitLiveData: TimeUnitLiveData,
    private val systemTimeProvider: SystemTimeProvider
) : StepCounterStepsLabelPresenter {

    @VisibleForTesting
    val stepsLiveData = Transformations.switchMap(timeUnitLiveData) {
        val calendar = Calendar.getInstance()
        val period = when (it) {
            TimeUnit.DAY -> systemTimeProvider.getTimeOfFirstDayOfCurrentDay(calendar)
            TimeUnit.WEEK -> systemTimeProvider.getTimeOfFirstDayOfCurrentWeek(calendar)
            else -> systemTimeProvider.getTimeOfFirstDayOfCurrentMonth(calendar)
        }
        stepsLoader.getStepsLiveDataForPeriod(period)
    }

    @VisibleForTesting
    val formattedStepsMapper = { it: Int -> "$it" }

    private val formattedStepsLiveData = Transformations
            .map(stepsLiveData, formattedStepsMapper)

    override fun observeValue(observer: (String) -> Unit) {
        formattedStepsLiveData.observeForever(observer)
        observer(formattedStepsLiveData.value ?: "0")
    }

    override fun removeValueObserver(observer: (String) -> Unit) {
        formattedStepsLiveData.removeObserver(observer)
    }
}