package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger

import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.interactor.TargetSeekerInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.interactor.TargetSeekerInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.presenter.TargetSeekerPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.presenter.TargetSeekerPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentTargetSeekersModule {

    @Provides
    @FragmentTargetsScope
    fun provideInteractor(
        dailyTargetLiveData: CurrentDailyTargetLiveData,
        weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
        monthlyTargetLiveData: CurrentMonthlyTargetLiveData
    ): TargetSeekerInteractor {
        return TargetSeekerInteractorImpl(
                dailyTargetLiveData,
                weeklyTargetLiveData,
                monthlyTargetLiveData
        )
    }

    @Provides
    @FragmentTargetsScope
    fun providePresenter(
        dailyTargetLiveData: CurrentDailyTargetLiveData,
        weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
        monthlyTargetLiveData: CurrentMonthlyTargetLiveData
    ): TargetSeekerPresenter {
        return TargetSeekerPresenterImpl(
                dailyTargetLiveData,
                weeklyTargetLiveData,
                monthlyTargetLiveData
        )
    }
}