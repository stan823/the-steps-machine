package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.*
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle.StepCounterProgressCirclePresenter
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle.StepCounterProgressCirclePresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel.StepCounterStepsLabelPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel.StepCounterStepsLabelPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.targetlabel.StepCounterTargetLabelPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.targetlabel.StepCounterTargetLabelPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main.FragmentStepCounterInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main.FragmentStepCounterInteractorImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 10/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentStepCounterModule {

    @Provides
    @FragmentProfileScope
    fun provideTargetLiveData(): TargetLiveData {
        return TargetLiveData()
    }

    @Provides
    @FragmentProfileScope
    fun provideLabelPresenter(
        timeUnitLiveData: TimeUnitLiveData,
        stepsLocalLoader: StepsLocalLoader,
        systemTimeProvider: SystemTimeProvider
    ): StepCounterStepsLabelPresenter {
        return StepCounterStepsLabelPresenterImpl(
                stepsLocalLoader,
                timeUnitLiveData,
                systemTimeProvider
        )
    }

    @Provides
    @FragmentProfileScope
    fun provideTargetsLabelPresenter(
        timeUnitLiveData: TimeUnitLiveData,
        targetLocalLoader: TargetLocalLoader
    ): StepCounterTargetLabelPresenter {
        return StepCounterTargetLabelPresenterImpl(
                timeUnitLiveData,
                targetLocalLoader

        )
    }

    @Provides
    @FragmentProfileScope
    fun provideCirclePresenter(
        targetLoader: TargetLocalLoader,
        stepsLoader: StepsLocalLoader,
        systemTimeProvider: SystemTimeProvider,
        timeUnitLiveData: TimeUnitLiveData
    ): StepCounterProgressCirclePresenter {
        return StepCounterProgressCirclePresenterImpl(
                targetLoader,
                stepsLoader,
                systemTimeProvider,
                timeUnitLiveData
        )
    }

    @Provides
    @FragmentProfileScope
    fun provideFragmentInteractor(
        navigationLiveData: MainNavigationLiveDataInterface,
        accountLiveData: AccountLiveDataInterface
    ): FragmentStepCounterInteractor {
        return FragmentStepCounterInteractorImpl(
                accountLiveData, navigationLiveData
        )
    }
}