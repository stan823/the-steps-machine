package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.interactor

/**
 * Created by Stanisław Brzeziński on 2019-08-29.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentSocialInteracor {

    fun onTextChange(text: String)
    fun onSerach(text: String)
    fun onFollowClicked(guid: String?)
}