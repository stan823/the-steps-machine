package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart

import android.content.Context
import android.graphics.Color
import android.util.AttributeSet
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.Observer
import androidx.lifecycle.OnLifecycleEvent
import androidx.room.util.StringUtil
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 2019-06-24.
 * Copyrights Stanisław Brzeziński
 */
class StepsChart : BarChart, LifecycleObserver {

    @Inject
    lateinit var presenter: StepsChartPresenter
    var dataSet: BarDataSet? = null

    private val dataObserver = Observer<List<StepsChartEntryModel>> {

        val entries = it.mapIndexed { index, item ->
            BarEntry(index.toFloat(), item.steps.toFloat(), item.dateLabel.padEnd(11,' '))
        }

        this.dataSet = BarDataSet(entries, "")
        dataSet?.color = context.resources.getColor(R.color.step_machine_accent)
        val data = BarData(dataSet)
        data.barWidth = .9f
        setData(data)
        val labelColor = context.resources.getColor(R.color.step_machine_charcoal)
        data.setValueTextColor(labelColor)
        data.setValueTextSize(12f)
        data.setValueFormatter(object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "${value.toInt()}"
            }
        })
        invalidate()
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyle: Int) : super(context, attrs, defStyle)

    override fun onFinishInflate() {
        val labelColor = context.resources.getColor(R.color.step_machine_charcoal)
        super.onFinishInflate()
        this.xAxis.setDrawGridLines(false)
        this.xAxis.position = XAxis.XAxisPosition.BOTTOM
        this.xAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                val entries = dataSet?.getEntriesForXValue(value)
                if (entries?.size ?: 0 > 0) {
                    val label = (dataSet?.getEntriesForXValue(value)?.get(0)?.data as? String)
                            ?: "no data"
                    return label.padEnd(11,' ')
                } else {
                    return "no entries"
                }
            }
        }
        this.axisLeft.axisMinimum = 0f
        this.axisLeft.setDrawGridLines(false)
        this.axisRight.setDrawGridLines(false)
        this.axisRight.textColor = labelColor
        this.axisLeft.isEnabled = false
        this.axisRight.isEnabled = false
        this.axisLeft.textColor = labelColor
        this.dataSet?.valueTextColor = labelColor
        this.description.isEnabled = false
        this.xAxis.granularity = 1f
        this.legend.isEnabled = false
        this.xAxis.labelRotationAngle = 45f
        this.xAxis.mLabelHeight = 100
        this.xAxis.axisLineWidth = 2f
        this.xAxis.textColor = labelColor
        fragmentStepsCounterComponent.inject(this)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        presenter.observeChartsData(dataObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        presenter.removeChartDataObserver(dataObserver)
    }
}