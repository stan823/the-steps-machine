package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentStepCounterInteractor {
    fun onProfilePressed()
}