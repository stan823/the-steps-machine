package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button.TargetsButton
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons.TimeButton
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.StepCounter
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle.StepCounterProgressCircle
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel.StepCounterStepsLabel
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.targetlabel.StepCounterTargetLabel
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart.StepsChart
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.FragmentStepCounter
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 14/12/2018.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        modules = [
            FragmentStepCounterModule::class,
            TimeButtonsModule::class,
            TargetsButtonModule::class,
            StepChartModule::class
        ],
        dependencies = [
            AppComponent::class
        ]
)
@FragmentProfileScope
interface FragmentProfileComponent {
    fun inject(fragment: FragmentStepCounter)
    fun inject(stepsChart: StepsChart)
    fun inject(presenter: StepCounter)
    fun inject(timeButton: TimeButton)
    fun inject(targetsButton: TargetsButton)
    fun inject(stepCounterLabel: StepCounterStepsLabel)
    fun inject(targetLabel: StepCounterTargetLabel)
    fun inject(circle: StepCounterProgressCircle)
}

val fragmentStepsCounterComponent: FragmentProfileComponent by lazy {
    DaggerFragmentProfileComponent
            .builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}