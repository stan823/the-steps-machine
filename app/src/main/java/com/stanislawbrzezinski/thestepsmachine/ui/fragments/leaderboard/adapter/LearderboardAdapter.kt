package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.LeaderboardEntryModel

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
typealias LeaderboardAdapterUnfollowCallback = (name: String, guid: String) -> Unit

class LearderboardAdapter(
        private val unfollowCallback: LeaderboardAdapterUnfollowCallback
) : RecyclerView.Adapter<LeaderboardViewHolder>() {

    private var _items: List<LeaderboardEntryModel> = arrayListOf()
    private var _myGuid: String = ""

    var myGuid: String
        get() = _myGuid
        set(value) {
            _myGuid = value
            notifyDataSetChanged()
        }

    var items: List<LeaderboardEntryModel>
        get() = _items
        set(value) {
            _items = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LeaderboardViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cell_leaderboard, parent, false)
        return LeaderboardViewHolder(view)
    }

    override fun getItemCount() = _items.size

    override fun onBindViewHolder(holder: LeaderboardViewHolder, position: Int) {
        _items[position]?.let { item ->
            holder.steps = "${item.steps ?: 0}"
            holder.name = "${position + 1}. ${item.name ?: ""}"
            if (item.guid == _myGuid) {
                holder.applyHighlight()
            } else {
                holder.removeHighlight()
            }

            holder.setNameClickListener {
                if (item.name != null && item.guid != null && item.guid != _myGuid) {
                    unfollowCallback(item.name, item.guid)
                }
            }
        }
    }
}