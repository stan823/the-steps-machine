package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.presenter

import androidx.lifecycle.LiveData
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData

/**
 * Created by Stanisław Brzeziński on 2019-05-16.
 * Copyrights Stanisław Brzeziński
 */
class TargetSeekerPresenterImpl(
    private val dailyTargetLiveData: CurrentDailyTargetLiveData,
    private val weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
    private val monthlyTargetLiveData: CurrentMonthlyTargetLiveData
) : TargetSeekerPresenter {

    override fun observeTargets(timeUnit: TimeUnit, observer: (Int) -> Unit) {
        val liveData = getLiveData(timeUnit)
        liveData.observeForever(observer)
        liveData?.value?.let {
            observer(it)
        }
    }

    override fun removeTargetsObserver(timeUnit: TimeUnit, observer: (Int) -> Unit) {
        val liveData = getLiveData(timeUnit)
        liveData.removeObserver(observer)
    }

    private fun getLiveData(timeUnit: TimeUnit): LiveData<Int> {
        return when (timeUnit) {
            TimeUnit.DAY -> dailyTargetLiveData
            TimeUnit.WEEK -> weeklyTargetLiveData
            TimeUnit.MONTH -> monthlyTargetLiveData
        }
    }
}