package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.dagger

import javax.inject.Scope

/**
 * Created by Stanisław Brzeziński on 2019-10-01.
 * Copyrights Stanisław Brzeziński
 */
@Scope
annotation class FragmentLoginScope