package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart

import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-06-06.
 * Copyrights Stanisław Brzeziński
 */
class StepsChartPresenterImpl(
    private val timeUnitLiveData: TimeUnitLiveData,
    stepsLoader: StepsLocalLoader
) : StepsChartPresenter {

    fun pairToChartEntryMapper(input: List<Pair<Long, Int>>): List<StepsChartEntryModel> {

        val result = arrayListOf<Pair<Long, Int>>()
        val dayDiffTimestamp = secondsInterval

        for (i in 0 until input.size - 1) {
            result.add(input[i])
            val currTimestamp = input[i].first
            val nextTimestamp = input[i + 1].first

            val diff = nextTimestamp - currTimestamp
            val diffInDays = diff / dayDiffTimestamp

            for (j in 1 until diffInDays) {
                val newTimestamp = currTimestamp + j * dayDiffTimestamp
                result.add(Pair(newTimestamp, 0))
            }
        }
        if (input.isNotEmpty()) {
            result.add(input.last())
        }
        val formatter = dateFormatter
        return result.map {
            StepsChartEntryModel(
                    dateLabel = formatter.format(Date(it.first * 1000)),
                    steps = it.second
            )
        }
    }

    val chartEntriesLiveData = Transformations.switchMap(timeUnitLiveData) {
        Transformations.map(when (timeUnitLiveData.value) {
            TimeUnit.DAY -> stepsLoader.getStepsByDay()
            TimeUnit.WEEK -> stepsLoader.getStepsByWeek()
            else -> stepsLoader.getStepsByMonth()
        }, this::pairToChartEntryMapper)
    }

    private val secondsInterval: Int
        get() =
            when (timeUnitLiveData.value) {
                TimeUnit.DAY -> 86400 // 60 * 60 * 24
                TimeUnit.WEEK -> 604800 // 60 * 60 * 24 * 7
                else -> 18144000 // 60 * 60 * 24 * 7 * 30
            }

    private val dateFormatter: DateFormat
        get() = when (timeUnitLiveData.value) {
            TimeUnit.DAY -> SimpleDateFormat("EEE dd")
            TimeUnit.WEEK -> DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault())
            else -> SimpleDateFormat("MMM yy")
        }

    override fun observeChartsData(
        observer: Observer<List<StepsChartEntryModel>>
    ) {
        chartEntriesLiveData.observeForever(observer)
    }

    override fun removeChartDataObserver(observer: Observer<List<StepsChartEntryModel>>) {
        chartEntriesLiveData.removeObserver(observer)
    }
}

