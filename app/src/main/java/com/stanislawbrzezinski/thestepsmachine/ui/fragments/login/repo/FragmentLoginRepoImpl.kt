package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo

import androidx.annotation.VisibleForTesting
import com.google.firebase.FirebaseNetworkException
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthInvalidUserException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.stanislawbrzezinski.thestepsmachine.NetworkUnavailableException
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.CreateAccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.toEntity
import com.stanislawbrzezinski.thestepsmachine.retrofit.AccountRetrofitService
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.NetworkErrors
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.LoginState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.LoginStateLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.RegistrationState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.RegistrationStateLiveDataInterface
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException
import java.net.SocketTimeoutException

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLoginRepoImpl(
        private val firebaseManager: FirebaseAuthManager,
        private val registrationLiveData: RegistrationStateLiveDataInterface,
        private val loginLiveData: LoginStateLiveDataInterface,
        private val accountRetrofitService: AccountRetrofitService,
        private val errorService: ErrorService,
        private val accountLiveData: AccountLiveDataInterface,
        private val accountsDao: AccountsDao
) : FragmentLoginRepo {

    private val disposale = CompositeDisposable()

    override fun createUser(email: String, password: String) {
        disposale.add(firebaseManager.registerWithEmail(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                { uid -> onFirebaseRegisterSuccess(email, uid) },
                this::onFirebaseRegisterError
        ))
    }

    @VisibleForTesting
    fun onFirebaseRegisterSuccess(email: String, uid: String) {
        val tempName = email.split("@")[0]
        val accountModel = CreateAccountModel(userId = uid, userName = tempName)
        disposale.add(accountRetrofitService.createAccount(accountModel)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                this::onAccountCreationSuccess,
                this::onAccountCreationError
        ))
    }

   @VisibleForTesting
   fun onFirebaseRegisterError(e: Throwable) {
        if (e is SocketTimeoutException || e is NetworkUnavailableException) {
            registrationLiveData.postValue(RegistrationState.NETWORK_UNAVAILABLE)
            return
        }
        when (e) {
            is FirebaseAuthWeakPasswordException ->
                registrationLiveData.postValue(RegistrationState.PASSWORD_TOO_WEEK)
            is FirebaseAuthUserCollisionException ->
                registrationLiveData.postValue(RegistrationState.USER_ALREADY_EXISTS)
            is FirebaseNetworkException ->
                registrationLiveData.postValue(RegistrationState.NETWORK_UNAVAILABLE)
            else ->
                registrationLiveData.postValue(RegistrationState.UNKNOWN_ERROR)
        }
    }

    private fun onAccountCreationSuccess(account: AccountModel) {
        accountsDao.insertAccount(account.toEntity())
        accountLiveData.postValue(account)
        registrationLiveData.postValue(RegistrationState.SUCCESS)
    }

    private fun onAccountCreationError(e: Throwable) {
        if (e is HttpException) {
            registrationLiveData.postValue(RegistrationState.UNKNOWN_ERROR)
        } else {
            registrationLiveData.postValue(RegistrationState.NETWORK_UNAVAILABLE)
        }
    }

    override fun login(email: String, password: String) {
        disposale.add(firebaseManager.loginWithEmail(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                this::onLoginToFirebaseSuccessful,
                this::onLoginToFirebaseError
        ))
    }

    private fun onLoginToFirebaseSuccessful(uid: String) {
        disposale.add(accountRetrofitService.login(uid)
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribe(
                this::onLoginToAccountSuccessful,
                this::onLoginToAccountFailure
        ))
    }

    private fun onLoginToFirebaseError(e: Throwable) {
        val state = if (e is SocketTimeoutException || e is NetworkUnavailableException) {
            LoginState.NETWORK_ERROR
        } else when (e) {
            is FirebaseAuthInvalidUserException -> LoginState.USER_NOT_FOUND
            is FirebaseAuthInvalidCredentialsException -> LoginState.INVALID_PASSWORD
            is FirebaseNetworkException -> LoginState.NETWORK_ERROR
            else -> LoginState.UNKNOWN_ERROR
        }
        loginLiveData.postValue(state)
    }

    private fun onLoginToAccountSuccessful(account: AccountModel) {
        accountsDao.insertAccount(account.toEntity())
        accountLiveData.postValue(account)
        loginLiveData.postValue(LoginState.SUCCESS)
    }

    private fun onLoginToAccountFailure(e: Throwable) {
        val state = if (e is HttpException) {
            when (errorService.parseException(e)) {
                NetworkErrors.RESOURCES_NOT_FOUND -> LoginState.USER_NOT_FOUND
                else -> LoginState.UNKNOWN_ERROR
            }
        } else {
            LoginState.UNKNOWN_ERROR
        }
        loginLiveData.postValue(state)
    }

    override suspend fun logOut() {
    }
}