package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetsButton : AppCompatImageView {

    @Inject
    lateinit var interactor: TargetButtonInteractor

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        fragmentStepsCounterComponent.inject(this)

        setOnClickListener {
            interactor.onClick()
        }
    }
}