package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.map
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountStateLiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
class FragmentAccountPresenterImpl(
    private val accountLiveData: AccountLiveDataInterface,
    private val stateLiveData: FragmentAccountStateLiveDataInterface
) : FragmentAccountPresenter {

    override fun observeName(livecycleOwner: LifecycleOwner, observer: Observer<String>) {
        accountLiveData.map { it?.name ?: "" }.observe(livecycleOwner, observer)
    }

    override fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentAccountState>) {
        stateLiveData.observe(lifecycleOwner, observer)
    }
}