package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter

import androidx.paging.PageKeyedDataSource
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.updateData
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.ConnectionsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.NetworkErrors
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FragmentSocialStateLiveDataInterface
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.HttpException

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
class ConnectionsSearchDataSource(
    private val searchItem: String,
    private val connectionsRetrofitService: ConnectionsRetrofitService,
    private val stateLiveData: FragmentSocialStateLiveDataInterface,
    private val errorService: ErrorService
) : PageKeyedDataSource<Int, ConnectionSearchResultModel>() {

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, ConnectionSearchResultModel>) {
        makeRequest(0) { items ->
            stateLiveData.updateData { it.copy(showNoResultsMessage = items.isEmpty(), isLoadingData = false) }
            callback.onResult(items.toMutableList(), null, 1)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ConnectionSearchResultModel>) {
        makeRequest(params.key) {
            callback.onResult(it.toMutableList(), params.key + 1)
        }
    }

    private fun makeRequest(pageNumber: Int, callback: (items: List<ConnectionSearchResultModel>) -> Unit) {
        GlobalScope.launch(Dispatchers.IO) {
            if (searchItem.isNotEmpty()) {
                try {
                    showLoading()
                    val items = connectionsRetrofitService.find(name = searchItem, pageNumber = pageNumber)
                    hideLoading()
                    callback(items)
                } catch (e: HttpException) {
                    val err = errorService.parseException(e)
                    val errorMessage = if (err == NetworkErrors.INVALID_INPUT) {
                        ErrorMessages.NAME_TOO_SHORT
                    } else {
                        ErrorMessages.SOMETHING_WENT_WRONG
                    }
                    showError(errorMessage)
                } catch (e: Exception) {
                    showError(ErrorMessages.SOMETHING_WENT_WRONG)
                }
            }
        }
    }

    private fun showLoading() {
        stateLiveData.updateData { it.copy(isLoadingData = true) }
    }

    private fun hideLoading() {
        stateLiveData.updateData { it.copy(isLoadingData = false) }
    }

    private fun showError(errorMessage: ErrorMessages) {
        stateLiveData.updateData {
            it.copy(errorMessage = errorMessage, isLoadingData = false)
        }
    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ConnectionSearchResultModel>) {
    }
}