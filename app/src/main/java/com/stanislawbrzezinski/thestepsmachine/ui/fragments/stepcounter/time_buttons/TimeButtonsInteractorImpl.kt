package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData

/**
 * Created by Stanisław Brzeziński on 22/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class TimeButtonsInteractorImpl(
    private val timeUnitLiveData: TimeUnitLiveData
) : TimeButtonsInteractor {

    override fun onClick(timeUnit: TimeUnit) {
        if (timeUnitLiveData.value != timeUnit) {
            timeUnitLiveData.postValue(timeUnit)
        }
    }
}