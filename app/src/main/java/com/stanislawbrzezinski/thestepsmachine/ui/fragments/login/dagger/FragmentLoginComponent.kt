package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.FragmentLogin
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-07-19.
 * Copyrights Stanisław Brzeziński
 */
@FragmentLoginScope
@Component(
        modules = [FragmentLoginModule::class],
        dependencies = [AppComponent::class]
)
interface FragmentLoginComponent {
    fun inject(fragmentLogin: FragmentLogin)
}

val fragmentLoginComponent: FragmentLoginComponent by lazy {
    DaggerFragmentLoginComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}