package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

/**
 * Created by Stanisław Brzeziński on 2019-09-24.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentMainPresenter {
    fun observePagerEnabled(lifecycleOwner: LifecycleOwner, obsever: Observer<Boolean>)
}