package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LogService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor.FragmentSplashInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor.FragmentSplashInteractorImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentSplashModule {

    @Provides
    @FragmentSplashScope
    fun provideInteractor(
        accountsLiveData: AccountLiveDataInterface,
        navigation: MainNavigationLiveDataInterface,
        db: AppDatabase,
        fireabase: FirebaseRemoteConfigManager,
        threadsService: ThreadsService,
        logService: LogService
    ): FragmentSplashInteractor {
        return FragmentSplashInteractorImpl(
                db.getAccountsDao(),
                navigation,
                accountsLiveData,
                fireabase,
                threadsService,
                logService
        )
    }
}