package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.dagger.fragmentRegistrationSuccessfulComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor.FragmentRegistrationSuccessfulInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter.FragmentRegistrationSuccessfulPresenter
import kotlinx.android.synthetic.main.fragment_registration_successfull.view.*
import javax.inject.Inject

class FragmentRegistrationSuccessful : Fragment() {

    @Inject
    lateinit var presenter: FragmentRegistrationSuccessfulPresenter

    @Inject
    lateinit var interactor: FragmentRegistrationSuccessfulInteractor

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_registration_successfull, container, false)
        fragmentRegistrationSuccessfulComponent.inject(this)

        presenter.observeMessage(this, Observer {
          if (it != null && it.isNotEmpty()) {
              view?.nameHolder?.text = it
          }
        })

        view?.okButton?.onClick(interactor::okClicked)
        return view
    }
}
