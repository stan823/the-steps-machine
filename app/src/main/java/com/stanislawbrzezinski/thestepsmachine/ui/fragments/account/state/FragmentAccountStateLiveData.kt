package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentAccountStateLiveDataInterface : LiveDataInterface<FragmentAccountState>

class FragmentAccountStateLiveData
    : MutableLiveData<FragmentAccountState>(),
        FragmentAccountStateLiveDataInterface {

    init {
        value = FragmentAccountState()
    }
}