package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter

import androidx.paging.DataSource
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.ConnectionsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FragmentSocialStateLiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
class ConnectionsSearchSourceFactory(
    private val connectionsRetrofitService: ConnectionsRetrofitService,
    private val stateLiveData: FragmentSocialStateLiveDataInterface,
    private val errorService: ErrorService
) : DataSource.Factory<Int, ConnectionSearchResultModel>() {

     var searchItem: String = ""

    lateinit var dataSource: ConnectionsSearchDataSource

    override fun create(): DataSource<Int, ConnectionSearchResultModel> {
        dataSource = ConnectionsSearchDataSource(
                searchItem,
                connectionsRetrofitService,
                stateLiveData,
                errorService
        )

        return dataSource
    }
}