package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentRegistrationSuccessfulPresenter {

    fun observeMessage(lifecycleOwner: LifecycleOwner, observer: Observer<String>)
}