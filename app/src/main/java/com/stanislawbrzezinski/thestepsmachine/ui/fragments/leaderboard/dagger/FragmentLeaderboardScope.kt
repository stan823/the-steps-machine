package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.dagger

import javax.inject.Scope

/**
 * Created by Stanisław Brzeziński on 2019-09-29.
 * Copyrights Stanisław Brzeziński
 */
@Scope
annotation class FragmentLeaderboardScope