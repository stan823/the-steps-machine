package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main.FragmentStepCounterInteractor
import kotlinx.android.synthetic.main.fragment_step_counter.view.*

import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class FragmentStepCounter : Fragment() {

    @Inject
    lateinit var interactor: FragmentStepCounterInteractor

    companion object {
        fun getFragment(): Fragment {
            return FragmentStepCounter()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_step_counter, container, false)
        fragmentStepsCounterComponent.inject(this)
        view?.let {
            initViews(view)
        }

        if (BuildConfig.FLAVOR != "prod") {
            view.fakeSystemServiceButton.visibility = View.VISIBLE
        }
        return view
    }

    private fun initViews(view: View) {
        view.apply {
            lifecycle.addObserver(stepCounter)
            lifecycle.addObserver(dayButton)
            lifecycle.addObserver(weekButton)
            lifecycle.addObserver(monthButton)
            lifecycle.addObserver(stepsChart)
            profileButton.onClick(interactor::onProfilePressed)
        }
    }
}