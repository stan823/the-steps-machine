package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatSeekBar
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger.fragmentTargetsComponent
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.extensions.onChange
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.interactor.TargetSeekerInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.presenter.TargetSeekerPresenter
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetSeeker : AppCompatSeekBar, LifecycleObserver {

    @Inject
    lateinit var interactor: TargetSeekerInteractor

    @Inject
    lateinit var presenter: TargetSeekerPresenter

    private var timeUnit = TimeUnit.DAY

    var progressStep = 1
    var progressMin = 1

    private val valueObserver = { value: Int ->
        progress = value
    }

    constructor(context: Context?)
            : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?)
            : super(context, attrs) {
        setUp(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        setUp(attrs)
    }

    private fun setUp(attrs: AttributeSet? = null) {
        val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.TargetSeeker)
        if (styledAttributes != null) {
            val timeUnitIndex = styledAttributes.getInt(
                    R.styleable.TargetSeeker_target_seeker_timeUnit,
                    0
            )

            timeUnit = TimeUnit.getByIndex(timeUnitIndex)
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        max = 1000
        fragmentTargetsComponent.inject(this)
        onChange { progress, fromUser ->
            var newProgress = progress / progressStep
            newProgress *= progressStep
            if (newProgress < progressMin) {
                newProgress = progressMin
            }
            if (fromUser) {
                interactor.updateValue(timeUnit, newProgress)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        presenter.observeTargets(timeUnit, valueObserver)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        presenter.removeTargetsObserver(timeUnit, valueObserver)
    }
}