package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.FragmentMain
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-09-24.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentMainModule::class]
)
@FragmentMainScope
interface FragmentMainComponent {
    fun inject(fragment: FragmentMain)
}

val fragmentMainComponent: FragmentMainComponent by lazy {
    DaggerFragmentMainComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}