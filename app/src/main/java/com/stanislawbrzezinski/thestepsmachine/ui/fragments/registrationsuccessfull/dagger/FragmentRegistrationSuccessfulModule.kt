package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.dagger

import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor.FragmentRegistrationSuccessfulInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor.FragmentRegistrationSuccessfulInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter.FragmentRegistrationSuccessfulPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter.FragmentRegistrationSuccessfulPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentRegistrationSuccessfulModule {

    @Provides
    @FragmentRegistrationSuccessfulScope
    fun providePresenter(accountsLiveData: AccountLiveDataInterface): FragmentRegistrationSuccessfulPresenter {
        return FragmentRegistrationSuccessfulPresenterImpl(accountsLiveData)
    }

    @Provides
    @FragmentRegistrationSuccessfulScope
    fun provideInteractor(navigation: MainNavigationLiveDataInterface): FragmentRegistrationSuccessfulInteractor {
        return FragmentRegistrationSuccessfulInteractorImpl(navigation)
    }
}