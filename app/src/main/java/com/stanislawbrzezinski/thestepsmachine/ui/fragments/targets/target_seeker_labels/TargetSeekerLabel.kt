package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.lifecycle.*
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger.fragmentTargetsComponent
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.extensions.isNotEditMode
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.presenter.TargetSeekerLabelPresenter
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetSeekerLabel : AppCompatTextView, LifecycleOwner, LifecycleObserver {

    @Inject
    lateinit var presenter: TargetSeekerLabelPresenter

    private var timeUnit = TimeUnit.WEEK

    lateinit var lifecycleRegistry: LifecycleRegistry
    override fun getLifecycle() = lifecycleRegistry

    constructor(context: Context?)
            : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?)
            : super(context, attrs) {
        setUp(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        setUp(attrs)
    }

    private fun setUp(attrs: AttributeSet? = null) {
        val styledAttributes = context.obtainStyledAttributes(attrs, R.styleable.TargetSeekerLabel)
        if (styledAttributes != null) {
            val timeUnitIndex = styledAttributes.getInt(
                    R.styleable.TargetSeekerLabel_target_seeker_label_timeUnit,
                    0
            )

            this.timeUnit = TimeUnit.getByIndex(timeUnitIndex)
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        isNotEditMode {
            fragmentTargetsComponent.inject(this)

            lifecycleRegistry = LifecycleRegistry(this)
            lifecycleRegistry.markState(Lifecycle.State.CREATED)

            presenter.observeTarget(this, timeUnit, Observer {
                if (timeUnit == this.timeUnit) {
                    this.text = "$it"
                }
            })
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        isNotEditMode {
            lifecycleRegistry.markState(Lifecycle.State.STARTED)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    fun onDestroy() {
        isNotEditMode {
            lifecycleRegistry.markState(Lifecycle.State.DESTROYED)
        }
    }
}