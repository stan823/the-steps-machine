package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.interactor

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetSeekerInteractorImpl(
    private val dailyTargetLiveData: CurrentDailyTargetLiveData,
    private val weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
    private val monthlyTargetLiveData: CurrentMonthlyTargetLiveData
) : TargetSeekerInteractor {

    override fun updateValue(timeUnit: TimeUnit, value: Int) {

        when (timeUnit) {
            TimeUnit.DAY -> dailyTargetLiveData.postValue(value)
            TimeUnit.WEEK -> weeklyTargetLiveData.postValue(value)
            else -> monthlyTargetLiveData.postValue(value)
        }
    }
}
