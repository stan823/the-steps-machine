package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state

import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages

/**
 * Created by Stanisław Brzeziński on 2019-08-28.
 * Copyrights Stanisław Brzeziński
 */
data class FragmentSocialState(
    val errorMessage: ErrorMessages? = null,
    val isLoadingData: Boolean = false,
    val showNoResultsMessage: Boolean = false
)