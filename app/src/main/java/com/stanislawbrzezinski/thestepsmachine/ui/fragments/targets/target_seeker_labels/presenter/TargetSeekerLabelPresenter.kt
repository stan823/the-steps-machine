package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TargetSeekerLabelPresenter {

    fun observeTarget(
        lifecycleOwner: LifecycleOwner,
        timeUnit: TimeUnit,
        observer: Observer<Int>
    )
}