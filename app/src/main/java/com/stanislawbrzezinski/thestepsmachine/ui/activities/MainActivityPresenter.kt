package com.stanislawbrzezinski.thestepsmachine.ui.activities

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface MainActivityPresenter {

    fun observeNavigation(
        lifecycleOwner: LifecycleOwner,
        observer: Observer<NavigationAction>
    )

    fun observeLoadingScreen(
            lifecycleOwner: LifecycleOwner,
            observer: Observer<Boolean>
    )
}