package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.interactor

import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.Push

/**
 * Created by Stanisław Brzeziński on 2019-09-24.
 * Copyrights Stanisław Brzeziński
 */
class FragmentMainInteractorImpl(
    private val navigationLiveData: MainNavigationLiveDataInterface
) : FragmentMainInteractor {

    override fun onLoggedBottomBarClicked() {
        navigationLiveData.postValue(
                Push(R.id.action_fragmentMain_to_fragmentLogin)
        )
    }
}