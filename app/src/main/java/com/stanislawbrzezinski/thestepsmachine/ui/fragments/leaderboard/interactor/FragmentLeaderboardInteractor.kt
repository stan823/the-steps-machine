package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLeaderboardInteractor {
    fun fetchLeaderboard()
    fun updateDate()
    fun unfollowUser( guid: String)
}