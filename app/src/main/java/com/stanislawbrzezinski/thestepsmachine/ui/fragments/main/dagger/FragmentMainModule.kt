package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.dagger

import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.interactor.FragmentMainInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.interactor.FragmentMainInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.presenter.FragmentMainPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.presenter.FragmentMainPresenterImpl
import dagger.Module
import dagger.Provides

@Module
class FragmentMainModule {

    @Provides
    @FragmentMainScope
    fun providesPresenter(accountLiveData: AccountLiveDataInterface): FragmentMainPresenter {
        return FragmentMainPresenterImpl(accountLiveData)
    }

    @Provides
    @FragmentMainScope
    fun provideInteractor(navigationLiveData: MainNavigationLiveDataInterface): FragmentMainInteractor {
        return FragmentMainInteractorImpl(navigationLiveData)
    }
}