package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter

import androidx.lifecycle.MutableLiveData

/**
 * Created by Stanisław Brzeziński on 14/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class FragmentProfileStepCountAnimateTargetLiveData : MutableLiveData<Boolean>() {
    init {
        value = true
    }
}