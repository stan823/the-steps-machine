package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor

import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
class FragmentRegistrationSuccessfulInteractorImpl(
    private val navigationLiveData: MainNavigationLiveDataInterface
) : FragmentRegistrationSuccessfulInteractor {

    override fun okClicked() {
        navigationLiveData.postValue(Pop())
    }
}