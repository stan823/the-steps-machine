package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentAccountInteractor {
    fun onLogOut()
    fun onTextChanged()
    fun onBack()
    fun onChange(name: String)
    fun deleteAccount()
}