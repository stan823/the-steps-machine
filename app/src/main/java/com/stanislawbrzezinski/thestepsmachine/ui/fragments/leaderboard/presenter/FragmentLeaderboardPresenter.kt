package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardState

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLeaderboardPresenter {

    fun observeState(
        lifecycleOwner: LifecycleOwner,
        observer: Observer<FragmentLeaderboardState>
    )

    fun observeMyGuid(
        lifecycleOwner: LifecycleOwner,
        observer: Observer<String>
    )
}