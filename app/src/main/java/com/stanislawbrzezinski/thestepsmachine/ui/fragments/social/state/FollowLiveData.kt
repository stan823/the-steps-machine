package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-09-16.
 * Copyrights Stanisław Brzeziński
 */

enum class FollowRequestAction {
    SUCCESSFUL,
    ERROR,
    STARTED
}

data class FollowRequestState(
    val guid: String,
    val action: FollowRequestAction
)

interface FollowLiveDataInterface : LiveDataInterface<FollowRequestState>

class FollowLiveData : MutableLiveData<FollowRequestState>(), FollowLiveDataInterface