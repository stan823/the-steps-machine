package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.isEmpty

/**
 * Created by Stanisław Brzeziński on 2019-09-24.
 * Copyrights Stanisław Brzeziński
 */
class FragmentMainPresenterImpl(
    accountLiveData: AccountLiveDataInterface
) : FragmentMainPresenter {

    private val loggedInLiveData = Transformations.map(accountLiveData as LiveData<AccountModel>) {
        !(it?.isEmpty() ?: true)
    }

    override fun observePagerEnabled(lifecycleOwner: LifecycleOwner, obsever: Observer<Boolean>) {
        loggedInLiveData.observe(lifecycleOwner, obsever)
    }
}