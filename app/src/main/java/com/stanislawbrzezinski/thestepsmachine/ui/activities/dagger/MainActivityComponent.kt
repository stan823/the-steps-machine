package com.stanislawbrzezinski.thestepsmachine.ui.activities.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivity
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        dependencies = [AppComponent::class],
        modules = [MainActivityModule::class]
)
@MainActivityScope
interface MainActivityComponent {

    fun inject(activity: MainActivity)
}

val mainActivityComponent: MainActivityComponent by lazy {
    DaggerMainActivityComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}
