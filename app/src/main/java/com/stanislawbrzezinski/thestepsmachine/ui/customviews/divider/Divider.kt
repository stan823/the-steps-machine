package com.stanislawbrzezinski.thestepsmachine.ui.customviews.divider

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import com.stanislawbrzezinski.thestepsmachine.R
import kotlinx.android.synthetic.main.view_divider.view.*

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
class Divider : LinearLayout {

    private var labelText: String? = null

    constructor(context: Context?) : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?)
            : super(context, attrs) {
        setUp(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        setUp(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int)
            : super(context, attrs, defStyleAttr, defStyleRes) {
        setUp(attrs)
    }

    private fun setUp(attrs: AttributeSet? = null) {
        if (attrs != null) {
            val attributes = context.obtainStyledAttributes(attrs, R.styleable.Divider)
            labelText = attributes.getString(R.styleable.Divider_divider_label)
        }

        val inflater = context?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as? LayoutInflater
        inflater?.inflate(R.layout.view_divider, this, true)
    }

    fun setDividerLabel(label: String) {
        this.label.text = label
        invalidate()
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        labelText?.let {
            label.text = labelText
        }
    }
}