package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state

import com.stanislawbrzezinski.thestepsmachine.models.retrofit.LeaderboardEntryModel

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
data class FragmentLeaderboardState(
    val leaderboardEntries: List<LeaderboardEntryModel> = arrayListOf(),
    val showLoadingSpinner: Boolean = false,
    val showPullToRefreshSpinner: Boolean = false,
    val showConnectionsMessage: Boolean = false,
    val showNoInternetMessage: Boolean = false,
    val showSomethingWentWrongMessage: Boolean = false,
    val currentMonth: String = ""
)