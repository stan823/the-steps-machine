package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.main

import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.isEmpty
import com.stanislawbrzezinski.thestepsmachine.navigation.Push

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */
class FragmentStepCounterInteractorImpl(
    private val accountLiveData: AccountLiveDataInterface,
    private val navigationLiveData: MainNavigationLiveDataInterface
) : FragmentStepCounterInteractor {

    override fun onProfilePressed() {
        if (accountLiveData.getValue().isEmpty()) {
            navigationLiveData.postValue(Push(R.id.action_fragmentMain_to_fragmentLogin))
        } else {
            navigationLiveData.postValue(Push(R.id.action_fragmentMain_to_fragmentAccount))
        }
    }
}