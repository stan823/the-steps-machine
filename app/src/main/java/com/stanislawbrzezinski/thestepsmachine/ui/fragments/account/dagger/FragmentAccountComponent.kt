package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.FragmentAccount
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentAccountModule::class]
)
@FragmentAccountScope
interface FragmentAccountComponent {
    fun inject(fragment: FragmentAccount)
}

val fragmentAccountComponent: FragmentAccountComponent by lazy {
    DaggerFragmentAccountComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}