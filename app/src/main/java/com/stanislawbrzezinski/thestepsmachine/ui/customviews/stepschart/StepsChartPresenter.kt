package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart

import androidx.lifecycle.Observer

/**
 * Created by Stanisław Brzeziński on 2019-06-06.
 * Copyrights Stanisław Brzeziński
 */
interface StepsChartPresenter {

    fun observeChartsData(
        observer: Observer<List<StepsChartEntryModel>>
    )

    fun removeChartDataObserver(
        observer: Observer<List<StepsChartEntryModel>>
    )
}