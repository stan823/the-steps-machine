package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo

import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.toEntity
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.room.daos.*
import io.reactivex.disposables.Disposable

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */

class FragmentAccountRepoImpl(
        private val accountsDao: AccountsDao,
        private val networkService: NetworkService,
        private val accountsLiveData: AccountLiveDataInterface,
        private val firebaseAuthManager: FirebaseAuthManager,
        private val stepsLocalInserter: StepsLocalInserter
) : FragmentAccountRepo {

    private var disposable: Disposable? = null

    override fun flushSteps() {
        stepsLocalInserter.clearAll()
    }

    override fun changeName(
            name: String,
            successCallback: () -> Unit,
            errorCallback: (e: Throwable) -> Unit
    ) {

        disposable = networkService.changeName(name).subscribe({
            val account = accountsLiveData.getValue() ?: AccountModel()
            val entity = account.toEntity()
            accountsDao.deleteAccount(entity)
            entity.name = name
            accountsDao.insertAccount(entity)
            accountsLiveData.postValue(account.copy(name = name))
            successCallback()
        }, {
            errorCallback(it)
        })
    }

    override fun deleteAccount(
            successCallback: () -> Unit,
            errorCallback: () -> Unit
    ) {
        val account = accountsLiveData.getValue()
        if (account != null) {
            disposable = networkService.deleteAccount().subscribe({
                firebaseAuthManager.deleteUser().subscribe({
                    accountsDao.deleteAccount(account.toEntity())
                    accountsLiveData.postValue(null)
                    flushSteps()
                    successCallback()
                }, {
                    errorCallback()
                })
            }, {
                errorCallback()
            })
        } else {
            errorCallback()
        }
    }
}