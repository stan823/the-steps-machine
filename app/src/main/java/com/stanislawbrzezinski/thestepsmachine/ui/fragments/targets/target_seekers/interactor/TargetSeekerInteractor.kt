package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.interactor

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TargetSeekerInteractor {

    fun updateValue(timeUnit: TimeUnit, value: Int)
}