package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-07-31.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLoginStateLiveDataInterface : LiveDataInterface<FragmentLoginState>

class FragmentLoginStateLiveData : MutableLiveData<FragmentLoginState>(), FragmentLoginStateLiveDataInterface {
    init {
        value = FragmentLoginState()
    }
}