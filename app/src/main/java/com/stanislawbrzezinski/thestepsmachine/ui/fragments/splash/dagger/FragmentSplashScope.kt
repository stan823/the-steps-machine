package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.dagger

import javax.inject.Scope

/**
 * Created by Stanisław Brzeziński on 2019-10-07.
 * Copyrights Stanisław Brzeziński
 */

@Scope
annotation class FragmentSplashScope