package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.extensions.showUnfollowDialog
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.adapter.LeaderboardAdapterUnfollowCallback
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.dagger.fragmentLeaderboardComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.adapter.LearderboardAdapter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor.FragmentLeaderboardInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.presenter.FragmentLeaderboardPresenter
import kotlinx.android.synthetic.main.fragment_leaderboard.view.*
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLeaderboard : Fragment() {

    @Inject
    lateinit var presenter: FragmentLeaderboardPresenter

    @Inject
    lateinit var interactor: FragmentLeaderboardInteractor

    private val unfollowCallback: LeaderboardAdapterUnfollowCallback = {name, guid ->
        showUnfollowDialog(guid, name) { guid ->
            interactor.unfollowUser(guid)
        }
    }

    private val adapter = LearderboardAdapter(unfollowCallback)

    companion object {
        fun getFragment(): Fragment {
            return FragmentLeaderboard()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_leaderboard, null, false)

        view.leaderboard.adapter = adapter

        fragmentLeaderboardComponent.inject(this)

        presenter.observeState(this, Observer { state ->
            adapter.items = state.leaderboardEntries
            view.swiper.isRefreshing = state.showPullToRefreshSpinner
            view.divider.setDividerLabel(state.currentMonth)
        })

        presenter.observeMyGuid(this, Observer {
            adapter.myGuid = it
        })

        interactor.updateDate()
        view.swiper.setOnRefreshListener(interactor::fetchLeaderboard)

        return view
    }

    override fun onResume() {
        super.onResume()
        if (interactor != null) {
            interactor.updateDate()
        }
    }
}