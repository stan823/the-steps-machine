package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalinserter.StepsLocalInserter
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.room.daos.DaysDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.MonthsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.StepsDao
import com.stanislawbrzezinski.thestepsmachine.room.daos.WeeksDao
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor.FragmentAccountInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.interactor.FragmentAccountInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.presenter.FragmentAccountPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.presenter.FragmentAccountPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo.FragmentAccountRepo
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo.FragmentAccountRepoImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountStateLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountStateLiveDataInterface
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentAccountModule {

    @Provides
    @FragmentAccountScope
    fun provideStateLiveData(): FragmentAccountStateLiveDataInterface {
        return FragmentAccountStateLiveData()
    }

    @Provides
    @FragmentAccountScope
    fun provideRepo(
            appDatabase: AppDatabase,
            accountsLiveData: AccountLiveDataInterface,
            firebaseAuthManager: FirebaseAuthManager,
            networkService: NetworkService,
            stepsLocalInserter: StepsLocalInserter

    ): FragmentAccountRepo {
        return FragmentAccountRepoImpl(
                appDatabase.getAccountsDao(),
                networkService,
                accountsLiveData,
                firebaseAuthManager,
                stepsLocalInserter
        )
    }

    @Provides
    @FragmentAccountScope
    fun provideInteractor(
            appDatabase: AppDatabase,
            firebaseAuthManager: FirebaseAuthManager,
            accountLiveData: AccountLiveDataInterface,
            navigationLiveData: MainNavigationLiveDataInterface,
            repo: FragmentAccountRepo,
            stateLiveData: FragmentAccountStateLiveDataInterface,
            errorService: ErrorService,
            threadsService: ThreadsService
    ): FragmentAccountInteractor {
        return FragmentAccountInteractorImpl(
                appDatabase.getAccountsDao(),
                firebaseAuthManager,
                accountLiveData,
                navigationLiveData,
                repo,
                stateLiveData,
                errorService,
                threadsService
        )
    }

    @Provides
    @FragmentAccountScope
    fun providePresenter(
            accountLiveData: AccountLiveDataInterface,
            stateLiveData: FragmentAccountStateLiveDataInterface
    ): FragmentAccountPresenter {
        return FragmentAccountPresenterImpl(accountLiveData, stateLiveData)
    }
}