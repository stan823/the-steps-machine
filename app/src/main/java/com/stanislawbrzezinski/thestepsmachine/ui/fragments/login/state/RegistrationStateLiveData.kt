package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
interface RegistrationStateLiveDataInterface : LiveDataInterface<RegistrationState>

class RegistrationStateLiveData :
        MutableLiveData<RegistrationState>(),
        RegistrationStateLiveDataInterface {

    override fun observeForever(observer: Observer<in RegistrationState>) {
        super.observeForever(observer)
    }
}