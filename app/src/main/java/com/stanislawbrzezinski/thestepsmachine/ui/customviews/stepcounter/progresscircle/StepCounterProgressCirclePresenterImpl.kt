package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.*
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import java.util.*

/**
 * Created by Stanisław Brzeziński on 2019-06-05.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterProgressCirclePresenterImpl(
    targetLoader: TargetLocalLoader,
    private val stepsLoader: StepsLocalLoader,
    private val systemTimeProvider: SystemTimeProvider,
    timeUnitLiveData: TimeUnitLiveData
) : StepCounterProgressCirclePresenter {

    private val targetStream = targetLoader.loadTargetsLiveData()
    private val percentageLiveData = MediatorLiveData<Float>()

    private val targetsLiveData = Transformations.switchMap(timeUnitLiveData) {
        when (it) {
            TimeUnit.DAY -> Transformations.map(targetStream) { it.daily }
            TimeUnit.WEEK -> Transformations.map(targetStream) { it.weekly }
            else -> Transformations.map(targetStream) { it.monthly }
        }
    }

    private val stepsLiveData = Transformations.switchMap(timeUnitLiveData) {
        val calendar = Calendar.getInstance()
        val period = when (it) {
            TimeUnit.DAY -> systemTimeProvider.getTimeOfFirstDayOfCurrentDay(calendar)
            TimeUnit.WEEK -> systemTimeProvider.getTimeOfFirstDayOfCurrentWeek(calendar)
            else -> systemTimeProvider.getTimeOfFirstDayOfCurrentMonth(calendar)
        }
        stepsLoader.getStepsLiveDataForPeriod(period)
    }

    init {

        percentageLiveData.addSource(targetsLiveData) {
            percentageLiveData.postValue(calculateTargetPercentage(stepsLiveData.value, it))
        }

        percentageLiveData.addSource(stepsLiveData) {
            percentageLiveData.postValue(calculateTargetPercentage(it, targetsLiveData.value))
        }
    }

    @VisibleForTesting
    fun calculateTargetPercentage(steps: Int?, target: Int?): Float {
        val st = steps ?: 0
        val trg = target ?: 0
        val result = if (trg > 0) {
            (st.toFloat() / trg.toFloat()) * 100
        } else {
            0f
        }

        return result.takeIf { it <= 100f } ?: 100f
    }

    override fun observeProgress(lifecycleOwner: LifecycleOwner, observer: Observer<Float>) {
        percentageLiveData.observe(lifecycleOwner, observer)
    }
}