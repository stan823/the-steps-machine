package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.presenter

import android.util.Log
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class TargetSeekerLabelPresenterImpl(
    private val dailyTargetLiveData: CurrentDailyTargetLiveData,
    private val weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
    private val monthlyTargetLiveData: CurrentMonthlyTargetLiveData
) : TargetSeekerLabelPresenter {

    override fun observeTarget(lifecycleOwner: LifecycleOwner, timeUnit: TimeUnit, observer: Observer<Int>) {
        val liveData = when (timeUnit) {
            TimeUnit.DAY -> dailyTargetLiveData
            TimeUnit.WEEK -> weeklyTargetLiveData
            else -> monthlyTargetLiveData
        }

        Log.d("life_data", "registering livedata: $liveData")
        liveData.observe(lifecycleOwner, observer)
        observer.onChanged(liveData.value)
    }
}