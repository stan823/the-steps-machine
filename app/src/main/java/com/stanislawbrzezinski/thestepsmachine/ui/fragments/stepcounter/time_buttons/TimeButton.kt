package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatButton
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 22/02/2019.
 * Copyrights Stanisław Brzeziński
 */
class TimeButton : AppCompatButton, LifecycleObserver {

    private var timeUnit = TimeUnit.DAY

    @Inject
    lateinit var interactor: TimeButtonsInteractor

    @Inject
    lateinit var presenter: TimeButtonPresenter

    private val timeUnitObserver: (TimeUnit) -> Unit = {
        onTimeUnitChagned(it)
    }

    private fun onTimeUnitChagned(it: TimeUnit?) {
        this.isSelected = it == timeUnit
        this.isEnabled = !this.isSelected
    }

    constructor(context: Context?) : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?)
            : super(context, attrs) {
        setUp(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int)
            : super(context, attrs, defStyleAttr) {
        setUp(attrs)
    }

    private fun setUp(attrs: AttributeSet? = null) {

        attrs?.let { attrs ->
            val attributesArray = context.theme.obtainStyledAttributes(attrs,
                    R.styleable.TimeButton,
                    0, 0)

            val timeUnitIndex = attributesArray.getInteger(R.styleable.TimeButton_timeUnit, 0)
            timeUnit = TimeUnit.getByIndex(timeUnitIndex)
        }
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        if (!isInEditMode) {
            fragmentStepsCounterComponent.inject(this)

            setOnClickListener {
                interactor.onClick(timeUnit)
            }
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (!isInEditMode) {
            presenter.observeTimeUnit(timeUnitObserver)
            onTimeUnitChagned(presenter.getTimeUnit())
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        presenter.removeTimeUnitObserver(timeUnitObserver)
    }
}