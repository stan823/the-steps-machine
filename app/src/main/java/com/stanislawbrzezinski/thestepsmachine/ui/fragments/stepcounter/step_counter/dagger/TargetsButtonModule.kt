package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger

import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button.TargetButtonInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button.TargetButtonInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class TargetsButtonModule {

    @FragmentProfileScope
    @Provides
    fun provideInteractor(
        mainNavigationLiveData: MainNavigationLiveDataInterface
    ): TargetButtonInteractor {
        return TargetButtonInteractorImpl(
                mainNavigationLiveData
        )
    }
}