package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.targetlabel

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.isNotEditMode
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 2019-05-20.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterTargetLabel : AppCompatTextView {

    @Inject
    lateinit var presenter: StepCounterTargetLabelPresenter

    private val valueObserver = { value: String ->
        text = value
    }

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    override fun onFinishInflate() {
        super.onFinishInflate()
        isNotEditMode {
            fragmentStepsCounterComponent.inject(this)
        }
    }

    fun onStart() {
        isNotEditMode {
            presenter.observeValue(valueObserver)
        }
    }

    fun onStop() {
        presenter.removeValueObserver(valueObserver)
    }
}