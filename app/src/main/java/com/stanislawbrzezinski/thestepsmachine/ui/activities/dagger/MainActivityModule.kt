package com.stanislawbrzezinski.thestepsmachine.ui.activities.dagger

import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveData
import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivityPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.activities.MainActivityPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */

@Module
class MainActivityModule {

    @Provides
    @MainActivityScope
    fun providePresenter(
        navigationLiveData: MainNavigationLiveDataInterface,
        loadingScreenLiveData: LoadingScreenLiveDataInterface
    ): MainActivityPresenter {
        return MainActivityPresenterImpl(
                navigationLiveData,
                loadingScreenLiveData
        )
    }
}