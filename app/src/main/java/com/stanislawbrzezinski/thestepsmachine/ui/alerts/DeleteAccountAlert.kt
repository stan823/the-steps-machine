package com.stanislawbrzezinski.thestepsmachine.ui.alerts

import android.app.Activity
import androidx.appcompat.app.AlertDialog
import com.stanislawbrzezinski.thestepsmachine.R

/**
 * Created by Stanisław Brzeziński on 2019-08-21.
 * Copyrights Stanisław Brzeziński
 */

fun Activity?.showDeleteAccountDialog(yesCallback: () -> Unit) {
    if (this != null)
        AlertDialog.Builder(this)
                .setPositiveButton(android.R.string.ok) { d, _ ->
                    yesCallback()
                    d.dismiss()
                }
                .setNegativeButton(android.R.string.cancel) { d, _ ->
                    d.dismiss()
                }
                .setMessage(R.string.alert_delete_account)
                .setTitle(R.string.alert_confirm)
                .show()
}
