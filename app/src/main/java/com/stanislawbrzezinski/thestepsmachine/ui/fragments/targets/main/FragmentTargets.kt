package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger.fragmentTargetsComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.interactor.FragmentTargetsInteractor
import kotlinx.android.synthetic.main.fragment_targets.view.*
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 01/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class FragmentTargets : Fragment() {

    @Inject
    lateinit var interactor: FragmentTargetsInteractor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_targets, null, false)

        activity?.resources?.let { res ->
            view.dailySeeker.max = res.getInteger(R.integer.daily_slider_max)
            view.dailySeeker.progressStep = res.getInteger(R.integer.daily_slider_step)
            view.dailySeeker.progressMin = res.getInteger(R.integer.daily_slider_min)

            view.weeklySeeker.max = res.getInteger(R.integer.weekly_slider_max)
            view.weeklySeeker.progressStep = res.getInteger(R.integer.weekly_slider_step)
            view.weeklySeeker.progressMin = res.getInteger(R.integer.weekly_slider_min)

            view.monthlySeeker.max = res.getInteger(R.integer.monthly_slider_max)
            view.monthlySeeker.progressMin = res.getInteger(R.integer.monthly_slider_min)
        }
        fragmentTargetsComponent.inject(this)
        lifecycle.addObserver(view.dailyTargetValue)
        lifecycle.addObserver(view.weeklyTargetValue)
        lifecycle.addObserver(view.monthlyTargetValue)
        lifecycle.addObserver(view.dailySeeker)
        lifecycle.addObserver(view.weeklySeeker)
        lifecycle.addObserver(view.monthlySeeker)

        view.backButton.onClick(interactor::onBackClicked)
        view.confirmButton.onClick(interactor::onConfirmClicked)
        return view
    }
}
