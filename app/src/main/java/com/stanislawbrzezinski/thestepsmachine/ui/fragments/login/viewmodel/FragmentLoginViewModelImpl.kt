package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.navigation.Push
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo.FragmentLoginRepo
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.enums.ToastMessages
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.*
import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtils

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLoginViewModelImpl(
        private val validationUtils: ValidationUtils,
        private val repo: FragmentLoginRepo,
        private val navigationLiveData: MainNavigationLiveDataInterface,
        registrationStateLiveData: RegistrationStateLiveDataInterface,
        loginLiveData: LoginStateLiveDataInterface,
        accountLiveData: AccountLiveDataInterface,
        private val fragmentStateLiveData: FragmentLoginStateLiveDataInterface,
        private val toastsService: ToastsService
) : FragmentLoginViewModel {

    @VisibleForTesting
    var email: String = ""

    @VisibleForTesting
    var password: String = ""

    @VisibleForTesting
    val loginStateObserver = Observer<LoginState> {

        hideLoadingScreen()
        when (it) {
            LoginState.UNKNOWN_ERROR,
            LoginState.NETWORK_ERROR -> toastsService.showMessage(ToastMessages.NETWORK_PROBLEM)
            LoginState.USER_NOT_FOUND -> showError(ErrorMessages.USER_NOT_FOUND)
            LoginState.INVALID_PASSWORD -> showError(ErrorMessages.INVALID_PASSWORD)

            LoginState.SUCCESS -> {
                hideError()
                if (accountLiveData.getValue() != null) {
                    navigationLiveData.postValue(Pop())
                }
            }
        }
    }

    init {

        accountLiveData.observeForever(Observer {
            if (it != null && registrationStateLiveData.getValue() == RegistrationState.SUCCESS) {
                showRegistrationSuccessfullScreen()
            }
        })

        loginLiveData.observeForever(loginStateObserver)
        registrationStateLiveData.observeForever(Observer {
            when (it) {
                RegistrationState.PASSWORD_TOO_WEEK ->
                    showError(ErrorMessages.PASSWORD_TOO_WEAK)
                RegistrationState.USER_ALREADY_EXISTS ->
                    showError(ErrorMessages.USER_ALREADY_EXISTS)

                RegistrationState.SUCCESS -> {
                    hideError()
                    if (accountLiveData.getValue() != null) {
                        showRegistrationSuccessfullScreen()
                    }
                }

                RegistrationState.NETWORK_UNAVAILABLE -> {
                    hideLoadingScreen()
                    toastsService.showMessage(ToastMessages.NETWORK_PROBLEM)
                }
            }
        })
    }

    override fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentLoginState>) {
        fragmentStateLiveData.observe(lifecycleOwner, observer)
    }

    private fun showRegistrationSuccessfullScreen() {
        navigationLiveData.postValue(Push(R.id.action_fragmentLogin_to_fragmentRegistrationSuccessfull))
    }

    override fun onEmailChanged(email: String) {
        this.email = email.trim()
        updateFields()
    }

    override fun onPasswordChanged(password: String) {
        this.password = password.trim()
        updateFields()
    }

    @VisibleForTesting
    fun updateFields() {
        hideError()
        val areEnabled = areCredentialsValid()
        if (areEnabled) {
            enableButtons()
        } else {
            disableButtons()
        }
    }

    override fun register() {
        showLoadingScreen()
        repo.createUser(email, password)
    }

    override fun login() {
        showLoadingScreen()
        repo.login(email, password)
    }

    @VisibleForTesting
    fun areCredentialsValid() = validationUtils.isValidEmail(email) && password.isNotBlank()

    override fun clearState() {
        fragmentStateLiveData.postValue(FragmentLoginState())
    }

    private fun enableButtons() {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(isRegistrationEnabled = true, isLoginEnabled = true)
        )
    }

    private fun disableButtons() {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(isRegistrationEnabled = false, isLoginEnabled = false)
        )
    }

    private fun hideError() {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(errorMessage = null, isLoading = false)
        )
    }

    private fun showError(error: ErrorMessages) {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(errorMessage = error, isLoading = false)
        )
    }

    private fun hideLoadingScreen() {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(isLoading = false)
        )
    }

    private fun showLoadingScreen() {
        val state = fragmentStateLiveData.getValue()
        fragmentStateLiveData.postValue(
                state?.copy(isLoading = true)
        )
    }
}