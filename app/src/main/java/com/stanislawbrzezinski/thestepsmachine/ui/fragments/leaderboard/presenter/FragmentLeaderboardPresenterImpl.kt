package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardStateLiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
class FragmentLeaderboardPresenterImpl(
    private val stateLiveData: FragmentLeaderboardStateLiveDataInterface,
    accountLiveData: AccountLiveDataInterface
) : FragmentLeaderboardPresenter {

    private val myGuidLiveData = Transformations.map(accountLiveData as LiveData<AccountModel>) {
        it?.guid ?: ""
    }

    override fun observeState(
        lifecycleOwner: LifecycleOwner,
        observer: Observer<FragmentLeaderboardState>
    ) {

        stateLiveData.observe(lifecycleOwner, observer)
    }

    override fun observeMyGuid(lifecycleOwner: LifecycleOwner, observer: Observer<String>) {
        myGuidLiveData.observe(lifecycleOwner, observer)
    }
}