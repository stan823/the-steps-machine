package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.google.android.material.tabs.TabLayout
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.dagger.fragmentMainComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.interactor.FragmentMainInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.presenter.FragmentMainPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.viewadapters.MainViewAdapter
import kotlinx.android.synthetic.main.fragment_main.view.*
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 01/05/2019.
 * Copyrights Stanisław Brzeziński
 */
class FragmentMain : Fragment() {

    @Inject
    lateinit var presenter: FragmentMainPresenter

    @Inject
    lateinit var interactor: FragmentMainInteractor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_main, null, false)
        Log.d("splog","created main")
        fragmentMainComponent.inject(this)
        context?.let { context ->
            val adapter = MainViewAdapter(context, childFragmentManager)

            view.viewPager.adapter = adapter
            view.viewPager.offscreenPageLimit = 2
            view.clickCatcher.onClick(interactor::onLoggedBottomBarClicked)

            (view.tabLayout as TabLayout).setupWithViewPager(view.viewPager)

            presenter.observePagerEnabled(this, Observer { loggedIn ->
                if (loggedIn) {
                    view.viewPager.setOnTouchListener { v, event -> false }
                    view.clickCatcher.visibility = View.GONE
                } else {
                    view.viewPager.setOnTouchListener { v, event -> true }
                    view.clickCatcher.visibility = View.VISIBLE
                }
            })
        }

        return view
    }
}