package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.FragmentLoginState

/**
 * Created by Stanisław Brzeziński on 2019-07-22.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLoginViewModel {

    fun onEmailChanged(email: String)
    fun onPasswordChanged(password: String)
    fun register()
    fun login()
    fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentLoginState>)
    fun clearState()
}