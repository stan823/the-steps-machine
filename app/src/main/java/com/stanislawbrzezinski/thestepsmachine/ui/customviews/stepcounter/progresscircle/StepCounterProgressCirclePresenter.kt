package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer

/**
 * Created by Stanisław Brzeziński on 2019-06-05.
 * Copyrights Stanisław Brzeziński
 */
interface StepCounterProgressCirclePresenter {
    fun observeProgress(lifecycleOwner: LifecycleOwner, observer: Observer<Float>)
}