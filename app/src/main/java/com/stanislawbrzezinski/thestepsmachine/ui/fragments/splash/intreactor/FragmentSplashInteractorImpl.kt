package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor

import android.util.Log
import androidx.annotation.VisibleForTesting
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LogService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ThreadsService
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseRemoteConfigManager
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.room.daos.AccountsDao
import com.stanislawbrzezinski.thestepsmachine.room.entities.toAccount

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSplashInteractorImpl(
    private val accountsDao: AccountsDao,
    private val navigation: MainNavigationLiveDataInterface,
    private val accountsLiveData: AccountLiveDataInterface,
    private val firebaseRemoteConfigManager: FirebaseRemoteConfigManager,
    private val threadsService: ThreadsService,
    private val logService: LogService
) : FragmentSplashInteractor {

    override fun loadData() {
        logService.log("splog","loading data")


        firebaseRemoteConfigManager.fetchSettings()
        logService.log("splog","remote fetched")

        val thread = threadsService.provideThread(this::runDbThread)
        logService.log("splog","thread created")
        thread.start()
        logService.log("splog","thread started")

    }

    @VisibleForTesting
    fun runDbThread() {
        logService.log("splog","inside thread started")
        val account = accountsDao.getAccount()

        logService.log("splog","posting account")
        accountsLiveData.postValue(account.toAccount())

        logService.log("splog","pushing navigation")
        navigation.push(R.id.action_fragmentSplash_to_fragmentMain)
    }
}
