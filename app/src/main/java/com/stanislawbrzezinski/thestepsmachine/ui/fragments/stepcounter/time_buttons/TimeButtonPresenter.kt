package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit

/**
 * Created by Stanisław Brzeziński on 22/02/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TimeButtonPresenter {

    fun observeTimeUnit(observer: (TimeUnit) -> Unit)

    fun removeTimeUnitObserver(observer: (TimeUnit) -> Unit)

    fun getTimeUnit(): TimeUnit
}