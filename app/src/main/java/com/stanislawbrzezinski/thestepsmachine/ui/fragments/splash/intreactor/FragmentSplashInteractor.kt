package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentSplashInteractor {

    fun loadData()
}