package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger

import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentTargetsDataModule {

    @Provides
    @FragmentTargetsScope
    fun provideDailyLiveData(): CurrentDailyTargetLiveData {
        return CurrentDailyTargetLiveData()
    }

    @Provides
    @FragmentTargetsScope
    fun provideWeeklyLiveData(): CurrentWeeklyTargetLiveData {
        return CurrentWeeklyTargetLiveData()
    }

    @Provides
    @FragmentTargetsScope
    fun provideMontlyLiveData(): CurrentMonthlyTargetLiveData {
        return CurrentMonthlyTargetLiveData()
    }
}