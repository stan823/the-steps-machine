package com.stanislawbrzezinski.thestepsmachine.ui.fragments.main.interactor

/**
 * Created by Stanisław Brzeziński on 2019-09-24.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentMainInteractor {
    fun onLoggedBottomBarClicked()
}