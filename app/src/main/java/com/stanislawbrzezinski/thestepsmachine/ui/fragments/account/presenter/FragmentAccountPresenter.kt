package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state.FragmentAccountState

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentAccountPresenter {

    fun observeName(livecycleOwner: LifecycleOwner, observer: Observer<String>)

    fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentAccountState>)
}