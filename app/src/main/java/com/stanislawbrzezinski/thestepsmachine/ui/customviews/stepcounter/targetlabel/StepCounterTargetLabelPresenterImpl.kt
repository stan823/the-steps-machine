package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.targetlabel

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData

/**
 * Created by Stanisław Brzeziński on 2019-05-20.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterTargetLabelPresenterImpl(
    private val timeUnitLiveData: TimeUnitLiveData,
    private val targetLocalLoader: TargetLocalLoader
) : StepCounterTargetLabelPresenter {

    private val formattedTargetLiveData = MutableLiveData<String>()

    init {
        formattedTargetLiveData.value = "0"
        targetLocalLoader.loadTargetsLiveData().observeForever { onDataChanged() }
        timeUnitLiveData.observeForever { onDataChanged() }
    }

    private fun onDataChanged() {
        val targetModel = targetLocalLoader.loadTargetsLiveData().value
        val target = when (timeUnitLiveData.value) {
            TimeUnit.DAY -> targetModel?.daily
            TimeUnit.WEEK -> targetModel?.weekly
            else -> targetModel?.monthly
        }
        formattedTargetLiveData.postValue("$target")
    }

    override fun observeValue(observer: (String) -> Unit) {
        formattedTargetLiveData.observeForever(observer)
        observer(formattedTargetLiveData.value ?: "0")
    }

    override fun removeValueObserver(observer: (String) -> Unit) {
        formattedTargetLiveData.removeObserver(observer)
    }
}