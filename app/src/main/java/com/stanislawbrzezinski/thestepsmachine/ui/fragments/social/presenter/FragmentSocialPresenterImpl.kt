package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.*

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSocialPresenterImpl(
        private val stateLiveData: FragmentSocialStateLiveDataInterface,
        private val pagedListLiveData: LiveData<PagedList<ConnectionSearchResultModel>>,
        private val followLiveData: FollowLiveDataInterface
) : FragmentSocialPresenter {

    override fun observeItemState(lifecycleOwner: LifecycleOwner, observer: Observer<FollowRequestState?>) {
        followLiveData.observe(lifecycleOwner, observer)
    }

    override fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentSocialState>) {
        stateLiveData.observe(lifecycleOwner, observer)
    }

    override fun observeItems(lifecycleOwner: LifecycleOwner, observer: Observer<PagedList<ConnectionSearchResultModel>>) {
        pagedListLiveData.observe(lifecycleOwner, observer)
    }
}