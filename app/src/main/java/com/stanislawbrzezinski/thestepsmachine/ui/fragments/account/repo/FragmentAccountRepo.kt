package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.repo

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentAccountRepo {
    fun changeName(
        name: String,
        successCallback: () -> Unit,
        errorCallback: (e: Throwable) -> Unit
    )

    fun flushSteps()

    fun deleteAccount(
        successCallback: () -> Unit,
        errorCallback: () -> Unit
    )
}