package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.interactor

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.stanislawbrzezinski.thestepsmachine.Constants
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.updateData
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionFollowRequest
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.ConnectionsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter.ConnectionsSearchSourceFactory
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.*
import io.reactivex.disposables.CompositeDisposable
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import retrofit2.HttpException

/**
 * Created by Stanisław Brzeziński on 2019-08-29.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSocialInteractorImpl(
        private val dataFactory: ConnectionsSearchSourceFactory,
        private val stateLiveData: FragmentSocialStateLiveDataInterface,
        private val pagedListLiveData: LiveData<PagedList<ConnectionSearchResultModel>>,
        private val networkService: NetworkService,
        private val toastsService: ToastsService,
        private val followLiveData: FollowLiveDataInterface
) : FragmentSocialInteracor {
    val disposable = CompositeDisposable()
    override fun onTextChange(text: String) {
        stateLiveData.updateData {
            it.copy(errorMessage = null, showNoResultsMessage = false)
        }
    }

    override fun onSerach(text: String) {
        if (text.length < Constants.Connections.MIN_SEARCH_WORD_SIZE) {
            setError(ErrorMessages.NAME_TOO_SHORT)
        } else {
            dataFactory.searchItem = text
            pagedListLiveData.value?.dataSource?.invalidate()
        }
    }

    private fun setError(error: ErrorMessages?) {
        stateLiveData.postValue(
                (stateLiveData.getValue() ?: FragmentSocialState()).copy(errorMessage = error)
        )
    }

    override fun onFollowClicked(guid: String?) {
        if (guid != null) {

            val state = FollowRequestState(guid = guid, action = FollowRequestAction.STARTED)
            followLiveData.postValue(state)
            disposable.add(networkService.follow(guid)
                    .subscribe({
                        followLiveData.postValue(state.copy(action = FollowRequestAction.SUCCESSFUL))
                    }, {
                        toastsService.somethingWentWrong()
                        followLiveData.postValue(state.copy(action = FollowRequestAction.ERROR))
                    })
            )

        }
    }
}