package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.stepslabel

/**
 * Created by Stanisław Brzeziński on 2019-05-20.
 * Copyrights Stanisław Brzeziński
 */
interface StepCounterStepsLabelPresenter {

    fun observeValue(observer: (String) -> Unit)
    fun removeValueObserver(observer: (String) -> Unit)
}