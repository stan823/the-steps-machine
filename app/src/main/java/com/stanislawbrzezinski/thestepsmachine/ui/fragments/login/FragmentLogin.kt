package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login

import android.os.Bundle
import android.text.Spannable
import android.text.SpannableString
import android.text.SpannedString
import android.text.TextPaint
import android.text.style.ClickableSpan
import android.text.style.ForegroundColorSpan
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.extensions.closeKeyboard
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.dagger.fragmentLoginComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.extensions.onTextChange
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel.FragmentLoginViewModel
import kotlinx.android.synthetic.main.fragment_login.view.*
import kotlinx.android.synthetic.main.fragment_login.view.versionHolder
import javax.inject.Inject
import android.content.Intent
import android.net.Uri
import android.text.method.LinkMovementMethod
import com.stanislawbrzezinski.thestepsmachine.Constants

/**
 * A simple [Fragment] subclass.
 */
class FragmentLogin : Fragment() {

    companion object {
        fun getInstance(): Fragment {
            return FragmentLogin()
        }
    }

    @Inject
    lateinit var viewModel: FragmentLoginViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_login, null, false)

        fragmentLoginComponent.inject(this)

        viewModel.observeState(this, Observer {
            view.loginButton.isEnabled = it.isLoginEnabled
            view.registerButton.isEnabled = it.isRegistrationEnabled
            view.loader.visibility = if (it.isLoading) View.VISIBLE else View.GONE
            if (it.errorMessage != null) {
                view.error.setText(it.errorMessage.messageRes)
            } else {
                view.error.text = ""
            }
        })

        view?.registerButton?.onClick {
            viewModel.register()
            closeKeyboard()
        }
        view?.loginButton?.onClick {
            viewModel.login()
            closeKeyboard()
        }
        view?.emailField?.onTextChange(viewModel::onEmailChanged)
        view?.passwordField?.onTextChange(viewModel::onPasswordChanged)
        view?.backButton?.onClick {
            Navigation.findNavController(view).popBackStack()
            closeKeyboard()
        }
        view?.versionHolder?.text = activity?.getString(
                R.string.version,
                BuildConfig.VERSION_NAME
        ) ?: ""
        setUpTermsLink(view.termsText)
        return view
    }

    private fun setUpTermsLink(termsText: TextView?) {
        val termsLinkLabel = activity?.getString(R.string.page_login_register_terms_link)
        if (termsLinkLabel != null) {
            val termsLabel = activity?.getString(
                    R.string.page_login_register_terms_text,
                    termsLinkLabel
            )
            if (termsLabel!=null) {
                val spannedString = SpannableString(termsLabel)
                val spanStart = termsLabel.indexOf(termsLinkLabel)
                val spanEnd = spanStart + termsLinkLabel.length
                val color = activity?.resources?.getColor(R.color.step_machine_accent)
                if (color!=null) {
                    spannedString.setSpan(
                            LinkSpan(color, this::onLinkClicked),
                            spanStart,
                            spanEnd,
                            Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                    )
                }

                termsText?.text = spannedString
                termsText?.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    private fun onLinkClicked(){
        val i = Intent(Intent.ACTION_VIEW)
        i.data = Uri.parse(Constants.BaseUrl.TERMS_AND_CONDITIONS_URL)
        activity?.startActivity(i)
    }

    override fun onDestroy() {
        super.onDestroy()
        viewModel.clearState()
    }
}


private class LinkSpan(
        private val color:Int,
        private val callback:()->Unit): ClickableSpan(){

    override fun onClick(widget: View) {
        callback()
    }

    override fun updateDrawState(ds: TextPaint) {
        super.updateDrawState(ds)
        ds.linkColor = color
        ds.isUnderlineText = true
    }
}
