package com.stanislawbrzezinski.thestepsmachine.ui.fragments.account.state

import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages

/**
 * Created by Stanisław Brzeziński on 2019-08-19.
 * Copyrights Stanisław Brzeziński
 */
data class FragmentAccountState(
    val isLoading: Boolean = false,
    val errorMessage: ErrorMessages? = null,
    val showSuccessMessage: Boolean = false
)