package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.presenter

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.Observer
import androidx.paging.PagedList
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FollowRequestState
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FragmentSocialState

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentSocialPresenter {
    fun observeState(lifecycleOwner: LifecycleOwner, observer: Observer<FragmentSocialState>)
    fun observeItems(lifecycleOwner: LifecycleOwner, observer: Observer<PagedList<ConnectionSearchResultModel>>)
    fun observeItemState(lifecycleOwner: LifecycleOwner, observer: Observer<FollowRequestState?>)
}