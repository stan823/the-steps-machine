package com.stanislawbrzezinski.thestepsmachine.ui.activities

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.transition.TransitionManager
import androidx.lifecycle.Observer
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.extensions.setVisible
import com.stanislawbrzezinski.thestepsmachine.ui.activities.dagger.mainActivityComponent
import com.stanislawbrzezinski.thestepsmachine.navigation.NavigationAction
import com.stanislawbrzezinski.thestepsmachine.navigation.Pop
import com.stanislawbrzezinski.thestepsmachine.navigation.Push
import com.stanislawbrzezinski.thestepsmachine.stepsservice.StepsService
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.Exception
import javax.inject.Inject

class MainActivity : AppCompatActivity() {
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var navController: NavController

    @Inject
    lateinit var presenter: MainActivityPresenter

    private val navigationObserver = Observer<NavigationAction> {
        try {
            when (it) {
                is Pop -> navController.popBackStack()
                is Push -> navController.navigate(it.action)
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FirebaseApp.initializeApp(this)
        firebaseAnalytics = FirebaseAnalytics.getInstance(this)

        setContentView(R.layout.activity_main)

        navController = findNavController(this, R.id.nav_host_fragment)

        mainActivityComponent.inject(this)

        presenter.observeNavigation(this, navigationObserver)
        presenter.observeLoadingScreen(this, Observer {
            TransitionManager.beginDelayedTransition(loadingScreen)
            loadingScreen.setVisible(it)
        })

        startService(Intent(this, StepsService::class.java))
    }
}
