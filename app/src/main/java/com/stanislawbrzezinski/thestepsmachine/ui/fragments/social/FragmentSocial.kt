package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.dagger.fragmentSocialComponent
import com.stanislawbrzezinski.thestepsmachine.enums.ErrorMessages
import com.stanislawbrzezinski.thestepsmachine.extensions.closeKeyboard
import com.stanislawbrzezinski.thestepsmachine.extensions.onAction
import com.stanislawbrzezinski.thestepsmachine.extensions.onTextChange
import com.stanislawbrzezinski.thestepsmachine.extensions.setVisible
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter.ConnectionsSearchAdapter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.interactor.FragmentSocialInteracor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.presenter.FragmentSocialPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FollowRequestAction
import kotlinx.android.synthetic.main.fragment_social.view.*
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSocial : Fragment() {

    @Inject
    lateinit var interactor: FragmentSocialInteracor

    @Inject
    lateinit var presenter: FragmentSocialPresenter

    private val adapter = ConnectionsSearchAdapter()

    companion object {
        fun getFragment(): Fragment {
            return FragmentSocial()
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_social, null, false)

        view.resultsHolder.adapter = this.adapter
        view.resultsHolder.layoutManager = LinearLayoutManager(activity)
        fragmentSocialComponent.inject(this)

        presenter.observeItems(this, Observer {
            adapter.submitList(it)
        })

        presenter.observeState(this, Observer {
            view.errorHolder.text = setError(it.errorMessage)
            view.dataLoader.setVisible(it.isLoadingData)
            view.noResultsMessage.setVisible(it.showNoResultsMessage)
        })

        presenter.observeItemState(this, Observer {
            when (it?.action) {
                FollowRequestAction.SUCCESSFUL -> adapter.userAdded(it.guid)
            }
        })

        view.searchField.onTextChange(interactor::onTextChange)
        view.searchField.onAction {
            interactor.onSerach(it)
            closeKeyboard()
        }
        adapter.clickCallback = interactor::onFollowClicked
        return view

    }

    private fun setError(errorMessage: ErrorMessages?): String {
        return if (errorMessage != null) {
            activity?.getString(errorMessage.messageRes) ?: ""
        } else {
            ""
        }
    }
}