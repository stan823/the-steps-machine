package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.FragmentRegistrationSuccessful
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
@FragmentRegistrationSuccessfulScope
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentRegistrationSuccessfulModule::class]
        )
interface FragmentRegistrationSuccessfulComponent {
    fun inject(fragment: FragmentRegistrationSuccessful)
}

val fragmentRegistrationSuccessfulComponent: FragmentRegistrationSuccessfulComponent by lazy {
    DaggerFragmentRegistrationSuccessfulComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}