package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.presenter

import com.stanislawbrzezinski.thestepsmachine.enums.TimeUnit

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TargetSeekerPresenter {

    fun observeTargets(timeUnit: TimeUnit, observer: (Int) -> Unit)
    fun removeTargetsObserver(timeUnit: TimeUnit, observer: (Int) -> Unit)
}