package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter

import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import android.view.LayoutInflater
import android.view.View
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.enums.ConnectionStatus
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import com.stanislawbrzezinski.thestepsmachine.extensions.setVisible
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.status

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
typealias ConnectionsSearchAdapterClickCallback = (guid: String?) -> Unit

class ConnectionsSearchAdapter
    : PagedListAdapter<ConnectionSearchResultModel, ConnectionsSearchViewHolder>(
        ConnectionSearchResultModel.CALLBACK
) {

    var clickCallback: ConnectionsSearchAdapterClickCallback? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ConnectionsSearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.cell_search_result, parent, false)
        return ConnectionsSearchViewHolder(view)
    }

    override fun onBindViewHolder(holder: ConnectionsSearchViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.nameHolder.text = item.name
            holder.addButton.setVisible(item.status != ConnectionStatus.ACCEPTED)
            holder.addedTick.setVisible(item.status == ConnectionStatus.ACCEPTED)
            holder.addButton.isClickable = true
            holder.addProgress.setVisible(false)
            holder.addButton?.onClick {
                holder.addButton.isClickable = false
                holder.addProgress.visibility = View.VISIBLE
                holder.addButton.visibility = View.INVISIBLE
                clickCallback?.invoke(item.guid)
            }
            if (item.hidden) {
                holder.container.visibility = View.GONE
            } else {
                holder.container.visibility = View.VISIBLE
            }
        }
    }

    fun userAdded(guid: String) {
        val item = currentList?.find { it.guid == guid }
        item?._status = ConnectionStatus.ACCEPTED.toString().toLowerCase()
        if (item != null) {
            val index = currentList?.indexOf(item) ?: -1
            notifyItemChanged(index)
        }
    }
}