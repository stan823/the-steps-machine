package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.lifecycle.*
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import kotlinx.android.synthetic.main.view_step_counter.view.*

/**
 * Created by Stanisław Brzeziński on 28/11/2018.
 * Copyrights Stanisław Brzeziński
 *
 */
class StepCounter : FrameLayout, LifecycleOwner, LifecycleObserver {

    private lateinit var lifecycle: LifecycleRegistry

    constructor(context: Context) : super(context) {
        setUp()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        setUp()
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setUp()
    }

    private fun setUp() {
        val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as? LayoutInflater
        inflater?.inflate(R.layout.view_step_counter, this, true)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()
        lifecycle = LifecycleRegistry(this)
        lifecycle.markState(Lifecycle.State.CREATED)
        lifecycle.addObserver(circle)
        if (!isInEditMode) {
            fragmentStepsCounterComponent.inject(this)
            valueHolder.onStart()
            targetHolder.onStart()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        if (!isInEditMode) {
            lifecycle.markState(Lifecycle.State.STARTED)
            valueHolder.onStop()
            targetHolder.onStop()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        if (!isInEditMode) {
            lifecycle.markState(Lifecycle.State.DESTROYED)
        }
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }
}