package com.stanislawbrzezinski.thestepsmachine.ui.viewadapters

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.FragmentLeaderboard
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.FragmentSocial
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.FragmentStepCounter

/**
 * Created by Stanisław Brzeziński on 07/12/2018.
 * Copyrights Stanisław Brzeziński
 */
class MainViewAdapter(
    private val context: Context,
    fm: FragmentManager?
) : FragmentStatePagerAdapter(fm) {

    private var _enabled = false

    var enabled: Boolean
        get() = _enabled
        set(value) {
            _enabled = value
            notifyDataSetChanged()
        }

    companion object {
        const val FRAGMENTS_COUNT = 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        val stringResource = when (position) {
            0 -> R.string.tab_profile
            1 -> R.string.tab_leaderboard
            else -> R.string.tab_social
        }
        return context.getString(stringResource)
    }

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> FragmentStepCounter.getFragment()
            1 -> FragmentLeaderboard.getFragment()
            else -> FragmentSocial.getFragment()
        }
    }

    override fun getCount(): Int {
        return FRAGMENTS_COUNT
    }
}