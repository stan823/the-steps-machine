package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.firebase.FirebaseAuthManager
import com.stanislawbrzezinski.thestepsmachine.livedata.*
import com.stanislawbrzezinski.thestepsmachine.retrofit.AccountRetrofitService
import com.stanislawbrzezinski.thestepsmachine.room.AppDatabase
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo.FragmentLoginRepo
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.repo.FragmentLoginRepoImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state.*
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel.FragmentLoginViewModel
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.viewmodel.FragmentLoginViewModelImpl
import com.stanislawbrzezinski.thestepsmachine.utils.ValidationUtils
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-07-19.
 * Copyrights Stanisław Brzeziński
 */

@Module
class FragmentLoginModule {

    @FragmentLoginScope
    @Provides
    fun provideStateLiveData(): FragmentLoginStateLiveDataInterface {
        return FragmentLoginStateLiveData()
    }

    @FragmentLoginScope
    @Provides
    fun registrationLiveData(): RegistrationStateLiveDataInterface {
        return RegistrationStateLiveData()
    }

    @FragmentLoginScope
    @Provides
    fun loginLiveData(): LoginStateLiveDataInterface {
        return LoginStateLiveData()
    }

    @FragmentLoginScope
    @Provides
    fun providesRepo(
        firebaseAuthManager: FirebaseAuthManager,
        registrationLiveData: RegistrationStateLiveDataInterface,
        loginLiveData: LoginStateLiveDataInterface,
        accountRetrofitService: AccountRetrofitService,
        errorService: ErrorService,
        accountLiveData: AccountLiveDataInterface,
        appDatabase: AppDatabase
    ): FragmentLoginRepo {
        return FragmentLoginRepoImpl(
                firebaseAuthManager,
                registrationLiveData,
                loginLiveData,
                accountRetrofitService,
                errorService,
                accountLiveData,
                appDatabase.getAccountsDao()
        )
    }

    @FragmentLoginScope
    @Provides
    fun provideViewModel(
        validationUtils: ValidationUtils,
        repo: FragmentLoginRepo,
        registrationLiveData: RegistrationStateLiveDataInterface,
        accountLiveData: AccountLiveDataInterface,
        navigationLiveData: MainNavigationLiveDataInterface,
        stateLiveData: FragmentLoginStateLiveDataInterface,
        loginStateLiveData: LoginStateLiveDataInterface,
        toastsService: ToastsService
    ): FragmentLoginViewModel {
        return FragmentLoginViewModelImpl(
                validationUtils,
                repo,
                navigationLiveData,
                registrationLiveData,
                loginStateLiveData,
                accountLiveData,
                stateLiveData,
                toastsService
        )
    }
}