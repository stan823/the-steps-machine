package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.extensions.onClick
import kotlinx.android.synthetic.main.cell_leaderboard.view.*

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
class LeaderboardViewHolder(itemView: View)
    : RecyclerView.ViewHolder(itemView) {

    val normalColor = itemView.context.resources.getColor(R.color.step_machine_charcoal)
    val highlightColor = itemView.context.resources.getColor(R.color.step_machine_accent)

    var steps: String
        get() = itemView.stepsHolder.text.toString()
        set(value) {
            itemView.stepsHolder.text = value
        }

    var name: String
        get() = itemView.nameHolder.text.toString()
        set(value) {
            itemView.nameHolder.text = value
        }

    fun setNameClickListener(listener: ()->Unit){
        itemView.nameHolder.onClick(listener)
    }

    fun applyHighlight() {
        itemView.stepsHolder.setTextColor(highlightColor)
        itemView.nameHolder.setTextColor(highlightColor)
    }

    fun removeHighlight() {
        itemView.stepsHolder.setTextColor(normalColor)
        itemView.nameHolder.setTextColor(normalColor)
    }
}