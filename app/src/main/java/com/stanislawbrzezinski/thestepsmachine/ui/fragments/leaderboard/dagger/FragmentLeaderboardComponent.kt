package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.FragmentLeaderboard
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
@FragmentLeaderboardScope
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentLeaderboardModule::class]
)
interface FragmentLeaderboardComponent {
    fun inject(fragment: FragmentLeaderboard)
}

val fragmentLeaderboardComponent: FragmentLeaderboardComponent by lazy {
    DaggerFragmentLeaderboardComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}