package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger

import com.stanislawbrzezinski.thestepsmachine.datasources.targetlocalloader.TargetLocalLoader
import com.stanislawbrzezinski.thestepsmachine.datasources.targetslocalsaver.TargetLocalSaver
import com.stanislawbrzezinski.thestepsmachine.livedata.MainNavigationLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.interactor.FragmentTargetsInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.interactor.FragmentTargetsInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-05-08.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentTargetsMainModule {

    @Provides
    @FragmentTargetsScope
    fun provideInteractor(
        navigationLiveData: MainNavigationLiveDataInterface,
        targetSaver: TargetLocalSaver,
        dailyTargetLiveData: CurrentDailyTargetLiveData,
        weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
        monthlyTargetLiveData: CurrentMonthlyTargetLiveData,
        targetLocalLoader: TargetLocalLoader
    ): FragmentTargetsInteractor {
        return FragmentTargetsInteractorImpl(
                navigationLiveData,
                targetSaver,
                targetLocalLoader,
                dailyTargetLiveData,
                weeklyTargetLiveData,
                monthlyTargetLiveData
        )
    }
}