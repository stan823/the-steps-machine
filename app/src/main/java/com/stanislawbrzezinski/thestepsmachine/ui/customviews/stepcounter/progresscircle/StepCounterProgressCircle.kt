package com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepcounter.progresscircle

import android.animation.ObjectAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Paint
import android.util.AttributeSet
import android.view.View
import android.view.animation.DecelerateInterpolator
import androidx.core.content.ContextCompat
import androidx.lifecycle.*
import com.stanislawbrzezinski.thestepsmachine.R
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger.fragmentStepsCounterComponent
import com.stanislawbrzezinski.thestepsmachine.extensions.degreeToPercentage
import com.stanislawbrzezinski.thestepsmachine.extensions.percentateToDegree
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 28/11/2018.
 * Copyrights Stanisław Brzeziński
 */
class StepCounterProgressCircle : View, LifecycleOwner, LifecycleObserver {

    private lateinit var lifecycleRegistry: LifecycleRegistry

    @Inject
    lateinit var presenter: StepCounterProgressCirclePresenter

    companion object {
        const val ANIMATION_DURATION = 1000L
        const val PERCENTAGE_COMPLETED = "percentageCompleted"
        const val GREEN_CIRCLE_ALPHA = "greenCircleAlpha"
    }

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)
    private var circleSize: Float = 0f
    private var halfCircleSize: Float = 0f
    private var colorBckg: Int = 0
    private var colorCircleProgress: Int = 0
    private var colorCircleDisabled: Int = 0
    private var colorGreenCircle: Int = 0
    private var circleAngleDegrees = 0f
    private var greenCircleAlpha: Int = 0
    private lateinit var angleAmimator: ObjectAnimator
    private lateinit var greenAmimator: ObjectAnimator

    private val progressObserver = Observer<Float> {
        val duration = (ANIMATION_DURATION * (it / 100)).toLong()
        animatedProgressBy(it, duration)
        greenAmimator.cancel()
        if (it == 100f) {
            animateColorToGreen(duration)
        } else {
            animateColorFromGreen(duration)
        }
    }

    var percentageCompleted: Float
        get() {
            return circleAngleDegrees.degreeToPercentage()
        }
        set(value) {
            circleAngleDegrees = value.percentateToDegree()
            invalidate()
        }

    constructor(context: Context?) : super(context) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
        setUp()
    }

    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes) {
        setUp()
    }

    private fun setUp() {
        circleSize = context.resources.getDimension(R.dimen.step_counter_size)
        halfCircleSize = circleSize / 2
        colorCircleProgress = ContextCompat.getColor(context, R.color.step_machine_accent)
        colorBckg = ContextCompat.getColor(context, R.color.step_machine_background)
        colorCircleDisabled = ContextCompat.getColor(context, R.color.step_machine_pearl)
        colorGreenCircle = context.resources.getColor(R.color.step_machine_positive)

        greenAmimator = ObjectAnimator.ofInt(this, GREEN_CIRCLE_ALPHA, 0)

        angleAmimator = ObjectAnimator.ofFloat(this, PERCENTAGE_COMPLETED, 0f)
        angleAmimator.interpolator = DecelerateInterpolator()
        lifecycleRegistry = LifecycleRegistry(this)
        lifecycleRegistry.markState(Lifecycle.State.CREATED)

        fragmentStepsCounterComponent.inject(this)
        presenter.observeProgress(this, progressObserver)
    }

    private fun animateColorToGreen(duration: Long) {
        greenAmimator.cancel()
        greenAmimator.setIntValues(greenCircleAlpha, 255)
        greenAmimator.duration = duration
        greenAmimator.start()
    }

    fun animatedProgressBy(percentage: Float, duration: Long) {

        angleAmimator.cancel()
        angleAmimator.setFloatValues(percentageCompleted, percentage)
        angleAmimator.duration = duration
        angleAmimator.start()
    }

    private fun animateColorFromGreen(duration: Long) {
        greenAmimator.cancel()
        greenAmimator.setIntValues(greenCircleAlpha, 0)
        greenAmimator.duration = duration
        greenAmimator.start()
    }

    override fun draw(canvas: Canvas?) {
        super.draw(canvas)

        paint.color = colorCircleDisabled
        canvas?.drawCircle(halfCircleSize, halfCircleSize, halfCircleSize - 1, paint)

        canvas?.drawCircle(halfCircleSize, halfCircleSize, halfCircleSize - 1, paint)

        paint.color = colorBckg
        canvas?.drawCircle(halfCircleSize, halfCircleSize, halfCircleSize - 2, paint)

        paint.color = colorCircleProgress
        canvas?.drawArc(0f, 0f, circleSize, circleSize, -90f, circleAngleDegrees, true, paint)

        paint.color = colorGreenCircle
        paint.alpha = greenCircleAlpha
        canvas?.drawArc(0f, 0f, circleSize, circleSize, -90f, circleAngleDegrees, true, paint)

        paint.alpha = 100
        paint.color = colorBckg
        canvas?.drawCircle(halfCircleSize, halfCircleSize, halfCircleSize - 4, paint)
    }

    private fun setGreenCircleAlpha(value: Int) {
        this.greenCircleAlpha = value
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycleRegistry
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onStart() {
        lifecycleRegistry.markState(Lifecycle.State.STARTED)
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun onStop() {
        lifecycleRegistry.markState(Lifecycle.State.DESTROYED)
    }
}