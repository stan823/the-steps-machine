package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.FragmentTargets
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.TargetSeekerLabel
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.TargetSeeker
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        dependencies = [AppComponent::class],
        modules = [
            FragmentTargetSeekersModule::class,
            FragmentTargetsDataModule::class,
            FragmentTargetsSeekerLabelsModule::class,
            FragmentTargetsMainModule::class
        ]
)
@FragmentTargetsScope
interface FragmentTargetsComponent {

    fun inject(fragment: FragmentTargets)
    fun inject(label: TargetSeekerLabel)
    fun inject(seeker: TargetSeeker)
}

val fragmentTargetsComponent: FragmentTargetsComponent by lazy {
    DaggerFragmentTargetsComponent.builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}