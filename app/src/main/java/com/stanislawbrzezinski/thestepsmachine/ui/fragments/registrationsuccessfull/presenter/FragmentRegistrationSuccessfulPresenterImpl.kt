package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.presenter

import androidx.annotation.VisibleForTesting
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.Transformations
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
class FragmentRegistrationSuccessfulPresenterImpl(
    accountLiveData: AccountLiveDataInterface
) : FragmentRegistrationSuccessfulPresenter {

    @VisibleForTesting
    val mapper: (AccountModel) -> String = { it.name ?: "" }
    private val messageLiveData = Transformations.map(accountLiveData as LiveData<AccountModel>, mapper)

    override fun observeMessage(lifecycleOwner: LifecycleOwner, observer: Observer<String>) {
        messageLiveData.observe(lifecycleOwner, observer)
    }
}