package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data

import androidx.lifecycle.MutableLiveData

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */

class CurrentDailyTargetLiveData : MutableLiveData<Int>() {

    init {
        value = 0
    }
}

class CurrentWeeklyTargetLiveData : MutableLiveData<Int>() {
    init {
        value = 0
    }
}

class CurrentMonthlyTargetLiveData : MutableLiveData<Int>() {
    init {
        value = 0
    }
}