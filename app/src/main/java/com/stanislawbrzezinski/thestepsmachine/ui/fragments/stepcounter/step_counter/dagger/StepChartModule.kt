package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger

import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart.StepsChartPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.customviews.stepschart.StepsChartPresenterImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-06-24.
 * Copyrights Stanisław Brzeziński
 */

@Module
class StepChartModule {

    @Provides
    @FragmentProfileScope
    fun providePresenter(
        timeUnitLiveData: TimeUnitLiveData,
        stepsLoader: StepsLocalLoader
    ): StepsChartPresenter {
        return StepsChartPresenterImpl(timeUnitLiveData, stepsLoader)
    }
}