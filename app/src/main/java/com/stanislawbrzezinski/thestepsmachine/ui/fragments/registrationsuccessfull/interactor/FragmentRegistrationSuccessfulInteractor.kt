package com.stanislawbrzezinski.thestepsmachine.ui.fragments.registrationsuccessfull.interactor

/**
 * Created by Stanisław Brzeziński on 2019-08-01.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentRegistrationSuccessfulInteractor {
    fun okClicked()
}