package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.dagger

import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.presenter.TargetSeekerLabelPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seeker_labels.presenter.TargetSeekerLabelPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentDailyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentMonthlyTargetLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.target_seekers.live_data.CurrentWeeklyTargetLiveData
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 07/05/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentTargetsSeekerLabelsModule {

    @Provides
    @FragmentTargetsScope
    fun providePresenter(
        dailyTargetLiveData: CurrentDailyTargetLiveData,
        weeklyTargetLiveData: CurrentWeeklyTargetLiveData,
        monthlyTargetLiveData: CurrentMonthlyTargetLiveData
    ): TargetSeekerLabelPresenter {
        return TargetSeekerLabelPresenterImpl(
                dailyTargetLiveData,
                weeklyTargetLiveData,
                monthlyTargetLiveData
        )
    }
}