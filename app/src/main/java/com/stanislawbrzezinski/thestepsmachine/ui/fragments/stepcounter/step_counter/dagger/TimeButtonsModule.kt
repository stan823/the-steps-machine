package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.step_counter.dagger

import com.stanislawbrzezinski.thestepsmachine.livedata.TimeUnitLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons.TimeButtonPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons.TimeButtonPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons.TimeButtonsInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.time_buttons.TimeButtonsInteractorImpl
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 22/02/2019.
 * Copyrights Stanisław Brzeziński
 */
@Module
class TimeButtonsModule {

    @Provides
    @FragmentProfileScope
    fun provideInteractor(timeUnitLiveData: TimeUnitLiveData): TimeButtonsInteractor {
        return TimeButtonsInteractorImpl(timeUnitLiveData)
    }

    @Provides
    @FragmentProfileScope
    fun providePresenter(timeUnitLiveData: TimeUnitLiveData): TimeButtonPresenter {
        return TimeButtonPresenterImpl(timeUnitLiveData)
    }
}