package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.dagger

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.stanislawbrzezinski.thestepsmachine.Constants
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.ConnectionSearchResultModel
import com.stanislawbrzezinski.thestepsmachine.retrofit.ConnectionsRetrofitService
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.services.errorservice.ErrorService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter.ConnectionsSearchSourceFactory
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.interactor.FragmentSocialInteracor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.interactor.FragmentSocialInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.presenter.FragmentSocialPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.presenter.FragmentSocialPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FollowLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FollowLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FragmentSocialStateLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.state.FragmentSocialStateLiveDataInterface
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
@Module
class FragmentSocialAdapterModule {

    @Provides
    @FragmentSocialScope
    fun provideStateLiveData(): FragmentSocialStateLiveDataInterface {
        return FragmentSocialStateLiveData()
    }

    @Provides
    @FragmentSocialScope
    fun provideSourceFactory(
        retrofitService: ConnectionsRetrofitService,
        stateLiveData: FragmentSocialStateLiveDataInterface,
        errorService: ErrorService
    ): ConnectionsSearchSourceFactory {
        return ConnectionsSearchSourceFactory(
                retrofitService,
                stateLiveData,
                errorService
        )
    }

    @Provides
    @FragmentSocialScope
    fun providePagedListLiveData(
        sourceFactory: ConnectionsSearchSourceFactory
    ): LiveData<PagedList<ConnectionSearchResultModel>> {
        val config = PagedList.Config.Builder()
                .setEnablePlaceholders(true)
                .setPrefetchDistance(10)
                .setPageSize(Constants.Connections.SEARCH_PAGE_SIZE)
                .build()
        return LivePagedListBuilder(
                sourceFactory,
                config
        ).build()
    }

    @Provides
    @FragmentSocialScope
    fun providePresenter(
        stateLiveData: FragmentSocialStateLiveDataInterface,
        pagedListLiveData: LiveData<PagedList<ConnectionSearchResultModel>>,
        followLiveData: FollowLiveDataInterface
    ): FragmentSocialPresenter {
        return FragmentSocialPresenterImpl(
                stateLiveData,
                pagedListLiveData,
                followLiveData
        )
    }

    @Provides
    @FragmentSocialScope
    fun followLiveData(): FollowLiveDataInterface {
        return FollowLiveData()
    }

    @Provides
    @FragmentSocialScope
    fun provideInteractor(
        dataFactory: ConnectionsSearchSourceFactory,
        stateLiveData: FragmentSocialStateLiveDataInterface,
        pagedListLiveData: LiveData<PagedList<ConnectionSearchResultModel>>,
        networkService: NetworkService,
        toastsService: ToastsService,
        followLiveData: FollowLiveDataInterface
    ): FragmentSocialInteracor {
        return FragmentSocialInteractorImpl(
                dataFactory,
                stateLiveData,
                pagedListLiveData,
                networkService,
                toastsService,
                followLiveData
        )
    }
}