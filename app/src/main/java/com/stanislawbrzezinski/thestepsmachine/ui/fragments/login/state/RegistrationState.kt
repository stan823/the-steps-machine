package com.stanislawbrzezinski.thestepsmachine.ui.fragments.login.state

/**
 * Created by Stanisław Brzeziński on 2019-07-25.
 * Copyrights Stanisław Brzeziński
 */
enum class RegistrationState {

    SUCCESS,
    UNKNOWN_ERROR,
    PASSWORD_TOO_WEEK,
    NETWORK_UNAVAILABLE,
    USER_ALREADY_EXISTS;
}
