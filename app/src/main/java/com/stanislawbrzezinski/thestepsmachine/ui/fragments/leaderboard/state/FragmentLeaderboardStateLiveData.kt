package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state

import androidx.lifecycle.MutableLiveData
import com.stanislawbrzezinski.thestepsmachine.commoninterfaces.LiveDataInterface

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentLeaderboardStateLiveDataInterface : LiveDataInterface<FragmentLeaderboardState>

class FragmentLeaderboardStateLiveData :
        MutableLiveData<FragmentLeaderboardState>(),
        FragmentLeaderboardStateLiveDataInterface {

    init {
        value = FragmentLeaderboardState()
    }
}