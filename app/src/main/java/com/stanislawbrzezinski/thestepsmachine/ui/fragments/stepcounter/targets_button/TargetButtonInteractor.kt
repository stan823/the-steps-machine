package com.stanislawbrzezinski.thestepsmachine.ui.fragments.stepcounter.targets_button

/**
 * Created by Stanisław Brzeziński on 06/05/2019.
 * Copyrights Stanisław Brzeziński
 */
interface TargetButtonInteractor {
    fun onClick()
}