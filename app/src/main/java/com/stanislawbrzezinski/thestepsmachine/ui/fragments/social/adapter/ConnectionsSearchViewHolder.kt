package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.adapter

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_search_result.view.*

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
class ConnectionsSearchViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val nameHolder = itemView.nameHolder
    val addButton = itemView.addButton
    val addProgress = itemView.addProgress
    val container = itemView.container
    val addedTick = itemView.addedTick
}