package com.stanislawbrzezinski.thestepsmachine.ui.fragments.targets.main.interactor

/**
 * Created by Stanisław Brzeziński on 2019-05-08.
 * Copyrights Stanisław Brzeziński
 */
interface FragmentTargetsInteractor {
    fun onConfirmClicked()
    fun onBackClicked()
}