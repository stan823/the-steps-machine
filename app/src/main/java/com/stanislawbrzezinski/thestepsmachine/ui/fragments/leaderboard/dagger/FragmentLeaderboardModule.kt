package com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.dagger

import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.LocaleService
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.SystemTimeProvider
import com.stanislawbrzezinski.thestepsmachine.dagger.systemservice.ToastsService
import com.stanislawbrzezinski.thestepsmachine.datasources.stepslocalreader.StepsLocalLoader
import com.stanislawbrzezinski.thestepsmachine.livedata.AccountLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.LoadingScreenLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.livedata.ToastsLiveDataInterface
import com.stanislawbrzezinski.thestepsmachine.retrofit.NetworkService
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor.FragmentLeaderboardInteractor
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.interactor.FragmentLeaderboardInteractorImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.presenter.FragmentLeaderboardPresenter
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.presenter.FragmentLeaderboardPresenterImpl
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardStateLiveData
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.leaderboard.state.FragmentLeaderboardStateLiveDataInterface
import dagger.Module
import dagger.Provides

/**
 * Created by Stanisław Brzeziński on 2019-09-19.
 * Copyrights Stanisław Brzeziński
 */

@Module
class FragmentLeaderboardModule {

    @Provides
    @FragmentLeaderboardScope
    fun provideLiveData(): FragmentLeaderboardStateLiveDataInterface {
        return FragmentLeaderboardStateLiveData()
    }

    @Provides
    @FragmentLeaderboardScope
    fun providePresenter(
        stateLiveData: FragmentLeaderboardStateLiveDataInterface,
        accountLiveData: AccountLiveDataInterface
    ): FragmentLeaderboardPresenter {
        return FragmentLeaderboardPresenterImpl(stateLiveData, accountLiveData)
    }

    @Provides
    @FragmentLeaderboardScope
    fun provideInteractor(
        accountLiveData: AccountLiveDataInterface,
        networkService: NetworkService,
        stateLiveData: FragmentLeaderboardStateLiveDataInterface,
        systemTimeProvider: SystemTimeProvider,
        localeService: LocaleService,
        stepsLocalLoader: StepsLocalLoader,
        loadingScreenLiveData: LoadingScreenLiveDataInterface,
        toastsService: ToastsService
    ): FragmentLeaderboardInteractor {
        return FragmentLeaderboardInteractorImpl(
                accountLiveData,
                networkService,
                stateLiveData,
                stepsLocalLoader,
                systemTimeProvider,
                localeService,
                loadingScreenLiveData,
                toastsService
        )
    }
}