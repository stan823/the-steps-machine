package com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.dagger

import com.stanislawbrzezinski.thestepsmachine.TheStepsMachineApp
import com.stanislawbrzezinski.thestepsmachine.dagger.app.AppComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.social.FragmentSocial
import dagger.Component

/**
 * Created by Stanisław Brzeziński on 2019-08-27.
 * Copyrights Stanisław Brzeziński
 */
@Component(
        dependencies = [AppComponent::class],
        modules = [FragmentSocialAdapterModule::class]
)
@FragmentSocialScope
interface FragmentSocialComponent {

    fun inject(fragment: FragmentSocial)
}

val fragmentSocialComponent: FragmentSocialComponent by lazy {
    DaggerFragmentSocialComponent
            .builder()
            .appComponent(TheStepsMachineApp.appComponent)
            .build()
}