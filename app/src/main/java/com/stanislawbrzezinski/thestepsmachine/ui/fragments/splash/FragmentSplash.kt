package com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.stanislawbrzezinski.thestepsmachine.BuildConfig
import com.stanislawbrzezinski.thestepsmachine.R
import kotlinx.android.synthetic.main.fragment_splash.view.versionHolder
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.dagger.fragmentSplashComponent
import com.stanislawbrzezinski.thestepsmachine.ui.fragments.splash.intreactor.FragmentSplashInteractor
import javax.inject.Inject

/**
 * Created by Stanisław Brzeziński on 2019-08-13.
 * Copyrights Stanisław Brzeziński
 */
class FragmentSplash : Fragment() {

    @Inject
    lateinit var interactor: FragmentSplashInteractor

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_splash, null, false)
        Log.d("splog","created splash")
        return view
    }

    override fun onResume() {
        super.onResume()
        Log.d("splog","resuming splash")
        fragmentSplashComponent.inject(this)
        interactor.loadData()
        view?.versionHolder?.text = activity?.getString(
                R.string.version,
                BuildConfig.VERSION_NAME
        ) ?: ""
    }
}