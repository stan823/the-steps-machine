package com.stanislawbrzezinski.thestepsmachine.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.NAME
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_ID
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_DAILY
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_WEEKLY
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_MONTHLY
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_CREATED

/**
 * Created by Stanisław Brzeziński on 2019-05-09.
 * Copyrights Stanisław Brzeziński
 */
@Entity(tableName = NAME)
data class TargetsEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = COL_ID)
    val id: Long = 0,

    @ColumnInfo(name = COL_DAILY)
    val daily: Int,

    @ColumnInfo(name = COL_WEEKLY)
    val weekly: Int,

    @ColumnInfo(name = COL_MONTHLY)
    val monthly: Int,

    @ColumnInfo(name = COL_CREATED)
    val created: Long

)