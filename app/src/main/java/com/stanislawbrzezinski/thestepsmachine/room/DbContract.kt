package com.stanislawbrzezinski.thestepsmachine.room

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
object DbContract {

    const val ID = "col_id"
    object TimedSteps {

        object Month {
            const val NAME = "month_steps"
        }

        object Week {
            const val NAME = "week_steps"
        }

        object Days {
            const val NAME = "day_steps"
        }

        const val TIMESTAMP = "col_timestamp"
        const val COUNT = "col_count"
    }

    object Account {
        const val TAB_ACCOUNT = "account"
        const val COL_TOKEN = "token"
        const val COL_GUID = "guid"
        const val COL_NAME = "name"
    }

    object Steps {
        const val NAME = "steps"
        const val COL_ID = ID
        const val COL_CREATED = "col_created"
    }

    object Targets {
        const val NAME = "targets"
        const val COL_ID = ID
        const val COL_DAILY = "daily"
        const val COL_WEEKLY = "weekly"
        const val COL_MONTHLY = "monthly"
        const val COL_CREATED = "created"
    }
}