package com.stanislawbrzezinski.thestepsmachine.room.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.stanislawbrzezinski.thestepsmachine.room.entities.TargetsEntity
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.NAME
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Targets.COL_CREATED

/**
 * Created by Stanisław Brzeziński on 2019-05-09.
 * Copyrights Stanisław Brzeziński
 */
@Dao
interface TargetsDao {

    @Query("SELECT * FROM $NAME ORDER BY $COL_CREATED DESC LIMIT 1")
    fun getTargetsLiveData(): LiveData<TargetsEntity>

    @Insert
    fun insert(vararg targets: TargetsEntity)
}