package com.stanislawbrzezinski.thestepsmachine.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.TimedSteps as Contract

/**
 * Created by Stanisław Brzeziński on 2019-06-10.
 * Copyrights Stanisław Brzeziński
 */

@Entity(tableName = Contract.Month.NAME)
data class MonthsEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = Contract.TIMESTAMP)
    var timestampOfFirstOfMonth: Long = 0L,

    @ColumnInfo(name = Contract.COUNT)
    var count: Int = 0
)
