package com.stanislawbrzezinski.thestepsmachine.room.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.stanislawbrzezinski.thestepsmachine.room.entities.StepEntity
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Steps.COL_CREATED
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Steps.NAME

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Dao
interface StepsDao {

    @Insert
    fun insert(step: StepEntity)

    @Query("DELETE FROM $NAME WHERE 1")
    fun deleteAll()

    @Query("SELECT count(*) FROM $NAME WHERE $COL_CREATED > :fromMilliseconds")
    fun getStepsLiveData(fromMilliseconds: Long): LiveData<Int>
}