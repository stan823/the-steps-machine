package com.stanislawbrzezinski.thestepsmachine.room.daos

import androidx.annotation.VisibleForTesting
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Account.TAB_ACCOUNT
import com.stanislawbrzezinski.thestepsmachine.room.entities.AccountEntity

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@Dao
interface AccountsDao {

    @VisibleForTesting
    @Insert
    fun insertAccountSync(account: AccountEntity?)

    @Insert
    fun insertAccount(account: AccountEntity)

    @Query("SELECT * FROM $TAB_ACCOUNT LIMIT 1")
    fun getAccount(): AccountEntity?

    @Delete
    fun deleteAccount(account: AccountEntity)
}