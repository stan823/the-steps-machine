package com.stanislawbrzezinski.thestepsmachine.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Steps as Contract

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */
@Entity(tableName = Contract.NAME)
data class StepEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = Contract.COL_ID)
    val id: Long = 0L,

    @ColumnInfo(name = Contract.COL_CREATED)
    val created: Long = System.currentTimeMillis()
)