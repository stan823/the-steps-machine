package com.stanislawbrzezinski.thestepsmachine.room

import android.content.Context
import androidx.room.Database
import androidx.room.RoomDatabase
import com.stanislawbrzezinski.thestepsmachine.room.daos.*
import com.stanislawbrzezinski.thestepsmachine.room.entities.*

/**
 * Created by Stanisław Brzeziński on 24/01/2019.
 * Copyrights Stanisław Brzeziński
 */

private lateinit var instance: AppDatabase

@Database(version = 5,
        entities = [
            StepEntity::class,
            TargetsEntity::class,
            DaysEntity::class,
            WeeksEntity::class,
            MonthsEntity::class,
            AccountEntity::class
        ])
abstract class AppDatabase : RoomDatabase() {

    companion object {
        fun getInstance(context: Context): AppDatabase {

            if (!::instance.isInitialized) {
                instance = createInstance(context)
            }
            return instance
        }

        private fun createInstance(context: Context): AppDatabase {
            val builder = AppDatabaseProvider().createDatabase(context)

            return builder.build()
        }
    }

    abstract fun getStepsDao(): StepsDao
    abstract fun getTargetsDao(): TargetsDao
    abstract fun getDaysDao(): DaysDao
    abstract fun getWeeksDao(): WeeksDao
    abstract fun getMonthsDao(): MonthsDao
    abstract fun getAccountsDao(): AccountsDao
}