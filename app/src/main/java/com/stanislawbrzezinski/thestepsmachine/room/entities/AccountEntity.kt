package com.stanislawbrzezinski.thestepsmachine.room.entities

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.stanislawbrzezinski.thestepsmachine.models.retrofit.AccountModel
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Account.COL_GUID
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Account.COL_TOKEN
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Account.COL_NAME
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.Account.TAB_ACCOUNT

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
@Entity(tableName = TAB_ACCOUNT)
data class AccountEntity(

    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name = COL_TOKEN)
    var token: String = "",

    @ColumnInfo(name = COL_GUID)
    var guid: String = "",

    @ColumnInfo(name = COL_NAME)
    var name: String = ""
)

fun AccountEntity?.toAccount(): AccountModel? {
        return if (this != null) {
                AccountModel(name = this.name, guid = this.guid, token = this.token)
        } else {
                null
        }
}