package com.stanislawbrzezinski.thestepsmachine.room.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.stanislawbrzezinski.thestepsmachine.room.entities.DaysEntity
import com.stanislawbrzezinski.thestepsmachine.room.DbContract.TimedSteps as Contract
import com.stanislawbrzezinski.thestepsmachine.room.entities.MonthsEntity
import com.stanislawbrzezinski.thestepsmachine.room.entities.WeeksEntity
import androidx.room.OnConflictStrategy

/**
 * Created by Stanisław Brzeziński on 2019-06-11.
 * Copyrights Stanisław Brzeziński
 */

@Dao
interface DaysDao {

    @Query("DELETE FROM ${Contract.Days.NAME} WHERE 1")
    fun deleteAll()

    @Query("SELECT * FROM ${Contract.Days.NAME} ORDER BY ${Contract.TIMESTAMP} ASC")
    fun getStepsByDay(): LiveData<List<DaysEntity>>

    @Query("SELECT * FROM ${Contract.Days.NAME} ORDER BY ${Contract.TIMESTAMP} ASC")
    fun getStepsValue(): List<DaysEntity>

    @Query("UPDATE ${Contract.Days.NAME} SET ${Contract.COUNT} = ${Contract.COUNT} + 1 WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun addStepToDay(timestamp: Long)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(daysEntity: DaysEntity): Long

    @Query("SELECT COUNT(*) FROM ${Contract.Days.NAME} WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun insert(timestamp: Long): Long
}

@Dao
interface WeeksDao {

    @Query("DELETE FROM ${Contract.Week.NAME} WHERE 1")
    fun deleteAll()

    @Query("SELECT * FROM ${Contract.Week.NAME} ORDER BY ${Contract.TIMESTAMP} ASC")
    fun getStepsByWeek(): LiveData<List<WeeksEntity>>

    @Query("UPDATE ${Contract.Week.NAME} SET ${Contract.COUNT} = ${Contract.COUNT} + 1 WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun addStepToWeek(timestamp: Long)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(weeksEntity: WeeksEntity): Long

    @Query("SELECT COUNT(*) FROM ${Contract.Week.NAME} WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun insert(timestamp: Long): Long
}

@Dao
interface MonthsDao {

    @Query("DELETE FROM ${Contract.Month.NAME} WHERE 1")
    fun deleteAll()

    @Query("SELECT * FROM ${Contract.Month.NAME} ORDER BY ${Contract.TIMESTAMP} ASC")
    fun getStepsByMonth(): LiveData<List<MonthsEntity>>

    @Query("UPDATE ${Contract.Month.NAME} SET ${Contract.COUNT} = ${Contract.COUNT} + 1 WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun addStepToMonth(timestamp: Long)

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(monthsEntity: MonthsEntity): Long

    @Query("SELECT COUNT(*) FROM ${Contract.Month.NAME} WHERE ${Contract.TIMESTAMP} = :timestamp")
    fun insert(timestamp: Long): Long
}