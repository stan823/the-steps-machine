package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import android.content.Context
import androidx.core.content.ContextCompat.getSystemService
import android.net.ConnectivityManager

/**
 * Created by Stanisław Brzeziński on 2019-08-05.
 * Copyrights Stanisław Brzeziński
 */
class NetworkInfoServiceImpl(private val context: Context) : NetworkInfoService {

    override fun isConnected(): Boolean {
        val cm = getSystemService(context, ConnectivityManager::class.java)
        return cm?.activeNetworkInfo != null
    }
}