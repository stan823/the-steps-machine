package com.stanislawbrzezinski.thestepsmachine

/**
 * Created by Stanisław Brzeziński on 2019-07-19.
 * Copyrights Stanisław Brzeziński
 */
object Endpoints {
    val baseUrl: String
        get() = Constants.BaseUrl.BASE_URL_STAGING
}