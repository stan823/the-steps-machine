package com.stanislawbrzezinski.thestepsmachine.room

import android.content.Context
import androidx.room.Room

/**
 * Created by Stanisław Brzeziński on 2019-08-14.
 * Copyrights Stanisław Brzeziński
 */
class AppDatabaseProvider {

    fun createDatabase(context: Context) = Room.databaseBuilder(
            context,
            AppDatabase::class.java,
            "the-steps-machine-db"
    ).fallbackToDestructiveMigration()
}
