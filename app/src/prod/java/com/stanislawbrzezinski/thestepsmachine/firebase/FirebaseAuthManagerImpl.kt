package com.stanislawbrzezinski.thestepsmachine.firebase

import com.google.firebase.auth.FirebaseAuth
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzezińskis
 */

class FirebaseAuthManagerImpl : FirebaseAuthManager {

    private val auth: FirebaseAuth by lazy {
        FirebaseAuth.getInstance()
    }

    override fun registerWithEmail(email: String, password: String): Single<String> {
        return Single.create { emitter ->
            auth.createUserWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        val uid = it.user?.uid ?: ""
                        emitter.onSuccess(uid)
                    }
                    .addOnFailureListener {
                        emitter.onError(Exception())
                    }
        }
    }

    override fun logOut() {
        auth.signOut()
    }

    override fun loginWithEmail(email: String, password: String): Single<String> {
        return Single.create { emitter ->
            auth.signInWithEmailAndPassword(email, password)
                    .addOnSuccessListener {
                        emitter.onSuccess(it.user.uid)
                    }
                    .addOnFailureListener {
                        emitter.onError(it)
                    }
        }
    }

    override fun deleteUser(): Single<Boolean> {
        return Single.create<Boolean> { emitter ->
            auth.currentUser?.delete()
                    ?.addOnSuccessListener { emitter.onSuccess(true) }
                    ?.addOnFailureListener { emitter.onError(it) }
        }.subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
    }
}



