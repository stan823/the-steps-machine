package com.stanislawbrzezinski.thestepsmachine.firebase

import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import io.reactivex.Single
import java.lang.Exception

/**
 * Created by Stanisław Brzeziński on 2019-07-04.
 * Copyrights Stanisław Brzeziński
 */

class FirebaseAuthManagerImpl : FirebaseAuthManager {

    companion object {
        const val EMAIL_IN_USE = "email_in@use.com"
        const val VALID_ID = "VALID_ID"
        const val NAME_IN_USE_NAME = "nameinuse"
        const val FREE_NAME = "freename"

        const val LOGIN_EMAIL_NOT_IN_DB = "LOGIN_EMAIL_NOT_IN_DB@EMAIL.COM"
        const val LOGIN_VALID_PASSWORD = "LOGIN_VALID_PASSWORD"
        const val LOGIN_UID_NOT_IN_DB = "LOGIN_UID_NOT_IN_DB"
    }



    override fun registerWithEmail(email: String, password: String): Single<String> {

        if (password.length < 6) {
            val exception = FirebaseAuthWeakPasswordException("abc", "abc", "abc")

            return Single.error(exception)
        }

        if (email == EMAIL_IN_USE) {
            val exception = FirebaseAuthUserCollisionException("abc", "abc")
            return Single.error(exception)
        }

        return Single.just(VALID_ID)
    }

    override  fun logOut() {
    }

    override fun loginWithEmail(email: String, password: String): Single<String> {
        return Single.just(when {
            email == LOGIN_EMAIL_NOT_IN_DB && password == LOGIN_VALID_PASSWORD -> LOGIN_UID_NOT_IN_DB
            else -> LOGIN_UID_NOT_IN_DB
        })
    }

    override fun deleteUser(): Single<Boolean> {
        return Single.just(true)
    }
}
