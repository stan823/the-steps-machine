package com.stanislawbrzezinski.thestepsmachine.dagger.systemservice

import android.content.Context

/**
 * Created by Stanisław Brzeziński on 2019-08-05.
 * Copyrights Stanisław Brzeziński
 */
class NetworkInfoServiceImpl(context: Context) : NetworkInfoService {

    companion object {
        var isConnected = true
    }

    override fun isConnected(): Boolean {
        return NetworkInfoServiceImpl.isConnected
    }
}